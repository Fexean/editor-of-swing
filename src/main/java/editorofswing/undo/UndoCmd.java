
package editorofswing.undo;

public interface UndoCmd {
    void undo();
    void redo();
    boolean shouldMerge();
}
