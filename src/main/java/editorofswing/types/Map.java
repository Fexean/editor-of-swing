
package editorofswing.types;
import editorofswing.util.Rom;
import editorofswing.util.RomConstants;
import java.util.ArrayList;
import java.io.File;
import java.io.ObjectOutputStream;
import java.io.*;



public class Map implements java.io.Serializable{
    private static final long serialVersionUID = 1L;
    
    //level size in pixels
    public int w, h;
    
    //Level size in 256x256 pixel chunks
    public int wChunks, hChunks;
    
    //Pixel coordinates for the sides of the camera rectangle
    public int cam_top, cam_left, cam_right, cam_bot;
    
    public int song;
    public int scrollFunc;
    public int crystalCoconutId;
    public int medalAddress;
    
    public ArrayList<ClimbCollision> climbCol;
    public ArrayList<Entity> entities;
    
    public Tileset tileset1, tileset2;
    public Tilemap layer1, layer2, layer3;
    public CollisionMap wallcol;
    public Palette pal, sprpal;
    
    public Rom.Version version;
    
    public Map(Rom rom, int index, int language){
	int borderPtr = rom.read32(RomConstants.getListValue(RomConstants.Table.LEVEL_BORDER, index));
	cam_top = rom.read16(borderPtr);
	cam_bot = rom.read16(borderPtr+2);
	cam_left = rom.read16(borderPtr+4);
	cam_right = rom.read16(borderPtr+6);
	
	song = rom.read32(RomConstants.getListValue(RomConstants.Table.LEVEL_SONG, index));
	scrollFunc = rom.read32(RomConstants.getListValue(RomConstants.Table.LEVEL_SCROLLFUNC, index));
	crystalCoconutId = rom.read32(RomConstants.getListValue(RomConstants.Table.CRYSTAL_COCONUT_ID, index));
	medalAddress = rom.read32(RomConstants.getListValue(RomConstants.Table.LEVEL_MEDAL_ADDR, index));
	
	h = 160 + cam_bot;
	w = 240 + cam_right;
	
	wChunks = 1 + (w - 1)/256;
	hChunks = 1 + (h - 1)/256;
	
	w = wChunks*256;
	h= hChunks*256;
	//System.out.println(wChunks + " "+ hChunks);
	loadPalette(rom, index);
	loadBackground(rom, index, language);
	loadClimbColllisions(rom, index, Math.max(wChunks, 4)*hChunks);
	loadEntities(rom, index);
	
	//Add extra horizontal row to wall collisions to allow falling under the map
	wallcol = new CollisionMap(rom, rom.read32(RomConstants.getListValue(RomConstants.Table.WALL_COLLISION, index)), wChunks*32, hChunks*32 + 32);
	version = rom.getVersion();
    }
    
    
    private void loadPalette(Rom rom, int lvlIndex){
	int globalpal = RomConstants.getListValue(RomConstants.Table.LEVEL_GLOBALPAL, lvlIndex); //this should maybe be in a separate function that does not use an index
	pal = Palette.loadPalFromRom(rom, globalpal, 32);
	int bgPal = rom.read32(RomConstants.getListValue(RomConstants.Table.LEVEL_BG_PAL, lvlIndex));
	pal.concatenate(Palette.loadPalFromRom(rom, bgPal, 14 * 16));
	
	int sprPal = rom.read32(RomConstants.getListValue(RomConstants.Table.LEVEL_SPR_PAL, lvlIndex));
	sprpal = Palette.loadPalFromRom(rom, sprPal, 256);
    }
    
    private void loadBackground(Rom rom, int lvlIndex, int language){
	int bgstruct = RomConstants.getLevelBgStruct(rom, lvlIndex, language);
	int tiles1 = rom.read32(bgstruct);
	int tiles2 = rom.read32(bgstruct+4);
	tileset1 = new Tileset(rom, tiles1);
	tileset2 = new Tileset(rom, tiles2);
	
	int tilemap1 = rom.read32(bgstruct+8);
	int tilemap2 = rom.read32(bgstruct+12);
	int tilemap3 = rom.read32(bgstruct+16);
	layer1 = new Tilemap(rom, tilemap1, wChunks*32, hChunks*32);
	layer2 = new Tilemap(rom, tilemap2, wChunks*32, hChunks*32);
	layer3 = new Tilemap(rom, tilemap3, wChunks*32, hChunks*32);
    }
    
    private void loadEntities(Rom rom, int lvlIndex){
	entities = new ArrayList();
	int entityPtr = rom.read32(RomConstants.getListValue(RomConstants.Table.ENTITIES, lvlIndex));
	while(rom.read32(entityPtr) >= 0){  //*entityptr < 0x80000000
	    entities.add(new Entity(rom, entityPtr));
	    entityPtr += 16;
	}
    }
    
    private void loadClimbColllisions(Rom rom, int index, int chunkcount){
	climbCol = new ArrayList();
	int climbColPtr = rom.read32(RomConstants.getListValue(RomConstants.Table.CLIMB_COLLISION, index));
	int climbColCount = getClimbCollisionCount(rom, index, chunkcount);
	for(int i = 0;i<climbColCount;i++){
	    ClimbCollision col = new ClimbCollision(rom, climbColPtr);
	    climbCol.add(col);
	    climbColPtr += 8;
	}
    }
    
    //The game uses a separate collision map for mapping each tile to a ClimbCollision index
    //to get the ClimbCollision count, iterate through this map and find the largest index
    private int getClimbCollisionCount(Rom rom, int index, int chunkCount){
	int climbmapPtr = rom.read32(RomConstants.getListValue(RomConstants.Table.CLIMB_MAP, index));
	if(climbmapPtr == 0){return 0;}
	int max = 0;
	for(int i = 0;i<chunkCount;i++){
	    int chunkptr = rom.read32(climbmapPtr + i*4);
	    if(!Rom.isRomPointer(chunkptr)){break;} //Happens in fire neckys stage, because the tilemap is 2x2 chunks but climbmap is smaller
	    for (byte b : rom.read(chunkptr, 0x400)){
		if (Byte.toUnsignedInt(b) > max){max = Byte.toUnsignedInt(b);}
	    }
	}
	return max;
    }
    
    public void resize(int width, int height){
	if(width <= 0 || height <= 0){return;}
	
	wChunks = width;
	hChunks = height;
	w = width*256;
	h = height*256;
	
	layer1.resize(width*32, height*32);
	layer2.resize(width*32, height*32);
	layer3.resize(width*32, height*32);
	wallcol.resize(width*32, height*32 + 32);
	
	cam_right = Math.min(cam_right, w - 240);
	cam_bot = Math.min(cam_bot, h - 160);
    }
    
    
    public void saveToFile(File file) throws Exception{
	FileOutputStream fileOut = new FileOutputStream(file);
	ObjectOutputStream out = new ObjectOutputStream(fileOut);
	out.writeObject(this);
	out.close();
	fileOut.close();
    }
    
    public static Map loadFromFile(File file) throws Exception{
	Map map;
	FileInputStream fileIn = new FileInputStream(file);
	ObjectInputStream strm = new ObjectInputStream(fileIn);
	map = (Map)strm.readObject();
	strm.close();
	fileIn.close();
	return map;
    }
}
