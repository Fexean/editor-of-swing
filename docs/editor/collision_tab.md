# Collisions-tab
![Collisions-tab Screenshot](https://gitlab.com/Fexean/editor-of-swing/-/raw/master/docs/editor/screenshots/collision_tab.png "Collisions-tab Screenshot")  

This tab lets you edit the collisions in the level.  
On the right side you can select the type of collision, which directions it applies to and the opacity at which the collisions are drawn on top of the map.  
On the left side you see the map's collisions which you can edit.  
The collisions are drawn with lines indicating their direction values. A collision only affects the player if they approach it from its direction, otherwise the player just passes through it.  


## Collision types
There are 16 types of collisions but as far as I can tell, they are all duplicates of the following types:  

|name|description|
|----|------------|
|LEVELBORDER|Solid to the player but lets thrown rocks pass
|SPIKE|Hurts the player
|BOUNCY|Solid, bounces player further than other solids
|SOLID_NO_BOUNCE|Solid, bounces player less than other solids
|SOLID| Solid wall, breaks thrown rocks


## Controls
Left click: place collision on the map  
Right click: copy a collision from the map  
Middle click: flood fill  
X: When held, the cursor's Y-coordinate is locked to its current value  
C: When held, the cursor's X-coordinate is locked to its current value  
Ctrl+Z: Undo  
Ctrl+Y: Redo  