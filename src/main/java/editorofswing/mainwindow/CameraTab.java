
package editorofswing.mainwindow;

import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.canvas.*;
import javafx.scene.paint.Color;
import javafx.scene.Scene;
import javafx.geometry.Insets;
import javafx.scene.control.Tab;
import editorofswing.types.*;
import javafx.scene.Cursor;
import editorofswing.util.MapEditor;

import editorofswing.util.RectUtil;

public class CameraTab extends Tab{
    BorderPane root;
    Canvas canvas;
    Pane canvasPane;
    

    MapEditor editor;
    
    TextField tField, bField, lField, rField;
    
    int dragSide;
    Scene scene;
    
    public CameraTab(){
	
	root = new BorderPane();
	canvas = new Canvas();
	
	canvasPane = new Pane();
	ScrollPane canvasScroll = new ScrollPane();
	canvasScroll.setContent(canvasPane);
	root.setCenter(canvasScroll);
	
	tField = new TextField();
	bField = new TextField();
	lField = new TextField();
	rField = new TextField();
	
	VBox toolRoot = new VBox();
	toolRoot.setSpacing(10);
	toolRoot.setPadding(new Insets(0, 10, 10, 10)); 
	toolRoot.getChildren().add(new Label("Camera boundaries:"));
	toolRoot.getChildren().add(new Label("Top:"));
	toolRoot.getChildren().add(tField);
	toolRoot.getChildren().add(new Label("Bottom:"));
	toolRoot.getChildren().add(bField);
	toolRoot.getChildren().add(new Label("Left:"));
	toolRoot.getChildren().add(lField);
	toolRoot.getChildren().add(new Label("Right:"));
	toolRoot.getChildren().add(rField);
	root.setRight(toolRoot);
	
	setContent(root);
	setText("Camera");
	
	tField.focusedProperty().addListener(e->{updateFromFields();});
	bField.focusedProperty().addListener(e->{updateFromFields();});
	lField.focusedProperty().addListener(e->{updateFromFields();});
	rField.focusedProperty().addListener(e->{updateFromFields();});
	
	canvas.setOnMousePressed(e->{
	    dragSide = getDragSide((int)e.getX(), (int)e.getY());
	});
	
	canvas.setOnMouseReleased(e->{
	    scene.setCursor(Cursor.DEFAULT);
	    dragSide = -1;
	});
	
	canvas.setOnMouseDragged(e->{
	    setSidePosition(dragSide, (int)e.getX(), (int)e.getY());
	    updateFields();
	    redrawCameraRect();
	});
	
	canvas.setOnMouseMoved(e->{
	    scene.setCursor(RectUtil.rectSideToCursor(getDragSide((int)e.getX(), (int)e.getY())));
	});
	
	canvas.setOnMouseExited(e -> {
	    if(dragSide == -1){
		scene.setCursor(Cursor.DEFAULT);
	    }
	});

    }
    
    private void updateFromFields(){
	editor.setCameraBorders(editorofswing.util.StringUtil.strToInt(tField.getText()),
	    editorofswing.util.StringUtil.strToInt(bField.getText()),
	    editorofswing.util.StringUtil.strToInt(lField.getText()),
	    editorofswing.util.StringUtil.strToInt(rField.getText())
	);
	fixMapCameraToBoundaries();
	redrawCameraRect();
    }
    
    private void updateFields(){
	Map map = editor.getMap();
	tField.setText(""+map.cam_top);
	bField.setText(""+map.cam_bot);
	lField.setText(""+map.cam_left);
	rField.setText(""+map.cam_right);
    }
    
    
    public void redrawCameraRect(){
	Map map = editor.getMap();
	GraphicsContext ctx = canvas.getGraphicsContext2D();
	ctx.setGlobalAlpha(1);
	ctx.setStroke(Color.RED);
	ctx.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
	ctx.strokeRect(map.cam_left, map.cam_top, map.cam_right-map.cam_left + 240, map.cam_bot-map.cam_top+160);
    }
    
    
    
    public void setup(MapEditor editor_, Canvas mapcanvas, Scene scene){
	editor = editor_;
	this.scene = scene;
	
	canvas.setWidth(mapcanvas.getWidth());
	canvas.setHeight(mapcanvas.getHeight());
	
	canvasPane.getChildren().clear();
	canvasPane.getChildren().add(mapcanvas);
	canvasPane.getChildren().add(canvas);
	canvas.toFront();
	updateFields();
	redrawCameraRect();
	
    }
    
    private void fixMapCameraToBoundaries(){
	Map map = editor.getMap();
	map.cam_top = Math.max(0, map.cam_top);
	map.cam_left = Math.max(0, map.cam_left);
	map.cam_right = Math.min(map.w - 240, Math.max(0, map.cam_right));
	map.cam_bot = Math.min(map.h - 160, Math.max(0, map.cam_bot));
    }
    
    
    private int getDragSide(int x, int y){
	Map map = editor.getMap();
	java.awt.Rectangle rect = new java.awt.Rectangle(map.cam_left, map.cam_top, map.cam_right-map.cam_left + 240, map.cam_bot-map.cam_top + 160);
	return RectUtil.getRectSide(rect, x, y, 0.05);
    }
    
    private void setSidePosition(int side, int x, int y){
	Map map = editor.getMap();
	switch(side){
	    case 1:
		map.cam_left = x;
		map.cam_top = y;
		break;
	    case 2:
		map.cam_top = y;
		break;
	    case 3:
		map.cam_right = x - 240;
		map.cam_top = y;
		break;
	    case 4:
		map.cam_left = x;
		break;
	    case 6:
		map.cam_right = x - 240;
		break;
	    case 7:
		map.cam_left = x;
		map.cam_bot= y - 160;
		break;
	    case 8:
		map.cam_bot= y - 160;
		break;
	    case 9:
		map.cam_right = x - 240;
		map.cam_bot = y - 160;
		break;
	}
	editor.setModified();
	fixMapCameraToBoundaries();
    }
    
    
}
