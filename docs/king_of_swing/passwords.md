# Passwords
The game has a secret password screen which can be accesed by pressing Up+L+B+A on the title screen.
There are 3 valid passwords (65942922, 55860327, 35805225) which unlock otherwise hidden jungle jam events.

The passwords are stored as 32-bit numbers (65942922 => 0x65942922) starting at 0x89F8CF8.
The 3 passwords are followed by a list of 7 32-bit string ids. The first 3 are for the strings that are shown when the passwords are entered, the next two are set to 0 and unused and the last 2 are for wrong password and already entered password.
The flags for entered passwords are stored at the end of [save data](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/king_of_swing/save.md) and they can be found starting at 0x203F410 in ram.

## Custom Passwords
New passwords can be added fairly easily. Simply increment the byte at 0x89F91A8 to get the game to check for more than 3 passwords and repoint the password and string id list and add new entries to them.  
