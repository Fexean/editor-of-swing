/*
This is the program used to create entity templates for the US rom from the EU entity templates.
It goes throught every entity on every map and takes the EU entity's template, slaps the US entity's callback on it and prints it out.

This can be used to generate entity templates for other versions too once(if) they are supported by the editor.
*/
package editorofswing;

import editorofswing.types.*;
import editorofswing.util.*;
import java.util.HashMap;

public class EntityTemplateDumperMain {
    public static void main(String[] args){
	Rom rom1, rom2;
	try{
	    rom1 = new Rom("kingofswing_eu.gba");
	    rom2 = new Rom("kingofswing_us.gba");
	}catch(Exception e){
	    System.out.println("Failed to open rom: " + e.getMessage());
	    return;
	}

	HashMap<String, EntityTemplate> euTemplates = ConfigReader.readEntityTemplates("EU_VERSION/entities.cfg");
	HashMap<Integer, EntityTemplate> usTemplates = new HashMap();
	
	HashMap<String, Integer> levelNames = ConfigReader.readHexSymbols("levelnames.cfg");
	for(int level : levelNames.values()){
	    Map euMap = getMapFromRom(rom1, level);
	    Map usMap = getMapFromRom(rom2, level);
	    
	    for(int i = 0;i<euMap.entities.size();i++){
		Entity euEnt = euMap.entities.get(i);
		Entity usEnt = usMap.entities.get(i);
		
		if(euEnt.spr_field80 != usEnt.spr_field80 || euEnt.x != usEnt.x || euEnt.y != usEnt.y){
		    System.out.println("Entity mismatch: " + level + " " + i);
		}else{
		    if(!usTemplates.containsKey(usEnt.callback)){
			//add matching us template
			for(EntityTemplate t : euTemplates.values()){
			    if(t.callback == euEnt.callback){
				usTemplates.put(usEnt.callback, createUsTemplate(t, usEnt.callback));
				break;
			    }
			}
		    }
		}
	    }

	}
	
	//print us templates
	for(EntityTemplate  t : usTemplates.values()){
	    System.out.println(t.toString());
	}
    }
    
    
    private static EntityTemplate createUsTemplate(EntityTemplate t, int callback){
	try{
	    EntityTemplate usTemp = new EntityTemplate(t.toString());
	    usTemp.callback = callback;
	    return usTemp;
	}catch(Exception e){
	    return null;
	}
    }
    
    private static Map getMapFromRom(Rom rom, int index){
	RomConstants.initOffsetsForRom(rom);
	return new Map(rom, index, 0);
    }
}
