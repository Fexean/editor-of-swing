# Worldmap  
This file contains information about the worldmap (level selection) screen.

## State function
The [state 0x9 function](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/king_of_swing/mainloop.md) loads the background and palettes, after which it calls a world-specific function to setup the rest of the data.  


## Background
The backgrounds are in the following format:  

|length (bytes)|name|
|--------|-------|
|4 | tileset_size
|tileset_size*0x20 | tileset
|0x800 | 32x32 tilemap

The backgrounds are loaded by the function ow_loadWorldBg (0x8872450) and the list of pointers to the different backgrounds is at 0x8872534.  
The tiles are loaded at 0x6000000 and the tilemap at 0x600F800.  


## Palettes
Each world has a 256 color sprite palette and a 256 color bg palette.  
The list of pointers to the palettes are at 0x8870FE4: 2 pointers for each world, first for bg palette & second for sprite palette. Note that world 1 is repeated twice at the start and also at the end of the list.  
The first 16 colors of sprite palette are always overwritten with DK's palette.  
  

## Level icons
Each level icon is an object with the callback handling the animation. The icons use the affine sprite tileset and sprite templates.  


## Level IDs
At address 0x3003F14 there's a pointer to the level ids of the currently selected world.  
The data is in the following format:  

|type|description|
|--------|-------|
|u8 | levelID
|u8 | level map count (excluding bonuses)

The arrows that take the player to another world are also in this list. Their levelID is set to 0x10*worldid and their level map count is set to 0x10.  
The list is terminated with 0xFFFF.  


## Level names
At address 0x3003F24 there's a pointer to the level name list. This list contains 2 bytes for each level in the currently open world. These bytes are used to index the list of level names to display the level name of the currently selected level.  
The level name is drawn by the function at 0x841CD2C (name id is passed in R0). The level names are stored as an array of uncompressed 4bpp tiles and a tilemap is generated for them when they are loaded. At 0x0842A798 there's a list of palette numbers for each level name to be used on the tilemap.


## Level positions
At address 0x3003F08 there's a pointer to a list of positions for each level in the current world. The positions are stored like this:

|type|description|
|--------|-------|
|u16 | x
|u16 | y

These positions are only used when the player enters the world/exits a level, and for determining DK's direction as he moves between levels. These values don't control DK's position when moving between levels.  


## Player priority list (?)
The pointer at 0x3003F0C points to a list of bytes, one for each level in the world. When a world is loaded, the value corresponding to the level the player will start on is stored into the player object's field 0x53 (priority?).  
This value is 0x14 for most(all?) levels.  


## Arrow destination positions 
At 0x3003F10 there's a pointer to the data that controls where DK moves when an arrow is selected.  
There's 16 bytes of data, 8 for each arrow, structured like:  

|type|name|description|
|------|-------|-------------|
|u16 | x | x-coordinate of the position the player will head to when arrow is selected
|u16 | y | y-coordinate of the position the player will head to when arrow is selected
|u16 | dist | the larger this is, the slower DK will move
|u16 | delay | when entering the world, this amount of time is waited before DK starts moving

Pointers for each world:  

|World|Pointer|
|--------|-------|
|1| 0x89CBBB4
|2| 0x89D1374
|3| 0x89D2740
|4| 0x89D3AE4
|5| 0x89E2BD8



## Movement
At 0x3003F1C there's a pointer to a 0xFF-terminated byte array that lists the indexes of the levels that reside on the "top row" in the worldmap, this list is used to determine which way DK moves when l/r are pressed.  

At 0x3003F18 there's a pointer that controls where the player can move by pressing L/R. It points to a list of pointers, one for each "world state" or set of unlocked levels. Each of these pointers then point to a list of 3 pointers:

|Offset|What|
|--------|-------|
|0| Index table
|4| L-button moves
|8| R-button moves

When the player presses L or R on the world map, the game reads a byte from the index table at index `2*selected_level + is_r`, where is_r is 1 if R was pressed and 0 if L was pressed. This value incremented by one is then used as index for the L-button moves or R-button moves tables and the byte read from there is the destination level index. If the destination index is 0xFF (end of table?), the game will instead use the first byte in L/R-button moves table.

Similarly there's a pointer for possible dpad moves at 0x3003F20, which points to a list of pointers, one for each world state. When the player presses a direction on the dpad, the game gets the pointer for the current world state and indexes it with `4*selected_level + dir_value`, the byte read from this index is the destination level index, if the byte is 0xFF, the player does not move. "dir_value" is the pressed dpad direction:

|Direction|Value|
|--------|-------|
|Down| 3
|Up| 2
|Left | 1
|Right | 0

When moving, the game reads the pointer at 0x3003F30, and indexes it with `8*selected_level+destination` to get a byte that selects the path that the player should follow. The bottom 7 bits of the byte select the path and the top bit inverts the path's direction.


## Layout data / Paths

Address 0x3003F28 contains a pointer to the path data. It's a list of pointers, one for each path in the world. The structure for a path is:

|Offset|Description|
|--------|-------|
|0| u8 length
|1| u8 unused (related to unused2)
|2| u8 unused2 (indexes level completion array at 0x3003EB4, and changes the paths palette to the same as always? =does nothing. Possibly originally meant to color completed paths differently)
|3| u8 unknown/unused

followed by "length" amount of nodes, stuctured like:

|Offset|Description|
|--------|-------|
|0 | u8 x
|1 | u8 y
|2 | u8 completion_id (used to determine level dot color based on if level is completed)
|3 | u8 unknown/unused
|4 | u16 graphic_id (only certain values are allowed, see table below)
|6 | u16 unknown/unused

List of allowed graphic_id values:

|graphic_id|Description|
|--------|-------|
|0x198| Level dot
|0x12E| Small path dot
|0x314| Arrow left
|0x318| Arrow right
|0x31C| Arrow up
|0x310| Arrow down

Address 0x3003F2C contains a pointer to data for each world state that selects which paths are drawn for a given state? (TODO: document structure)
