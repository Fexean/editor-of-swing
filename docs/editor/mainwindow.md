# Editor Of Swing Main Window
The tool's main window contains a menubar, a list of levels and several tabs for editing levels.  
To get started with map editing, you will need to open a map, either from a map file or from the level list which becomes available after opening a rom.

## Editor Tabs
[Map](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/editor/map_tab.md)  
[Collisions](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/editor/collision_tab.md)  
[Climbing](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/editor/climb_tab.md)  
[Entities](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/editor/entity_tab.md)  
[Camera](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/editor/camera_tab.md)  
[Other](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/editor/other_tab.md)  



## File Menu

### Open ROM...
Opens a file dialog that lets you select a rom file.  

### Save map to ROM (Overwrite)
Inserts the currently opened map to the rom, replacing the level that was last selected.  
The map data is written over the original data and data is only repointed if necessary.

### Save map to ROM (Repointed)
Inserts the currently opened map to the rom, replacing the level that was last selected.
The map data is entirely written to free space in the rom.

### Save map as...
Saves the currently open map to a file.

### Import map...
Replaces the currently open map with one read from a file.

## Undo/Redo
Undo & Redo only apply to changes in the tilemap and collisionmap.

## Map insertion: Overwrite VS Repointed
The overwrite mode usually uses less rom space since it reuses the space taken by the original level so it might seem like the better option.  
*However* in some of the levels the game reuses the same chunkmap chunks(usually collisions) in multiple places. In these situations the Overwrite mode will either save the chunks incorrectly or overwrite data used by other levels.  
Because of this I recommend that you use the repointed mode when saving a map for the first time and overwrite for further edits.





## Options Menu

### Auto-repoint
When enabled, all repointing is done by automatically finding a long enough sequence of 0xFF bytes to replace.  
When disabled and repointing is needed, a window opens up and asks the user to insert the repoint address manually.  

### Ask before discarding changes
When enabled, the editor will ask the user to discard changes whenever they open a new map and they haven't saved their changes to the previous map.

### Draw level backgrounds
When enabled, the editor will render the background layers of levels.  
When disabled, the background is filled with black and only the top background layer is drawn.

### Draw entity sprites
When enabled the editor will try to draw sprites for entities instead of blue rectangles.
The sprite IDs to draw are stored in the entity template files. Entities without sprite IDs will still be drawn as blue rectangles. Currently there are sprite IDs only for the european rom.

### ROM language
This selects which language is used when levels are loaded from the rom or saved to the rom. Only available with european roms.


## Tools Menu
From this menu you can access built-in tools other than the map editor. This menu is only available after a rom has been loaded.  

### List of tools:
[Time attack editor](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/editor/time_attack_editor.md)  



## Level List
This contains a list of the game's levels. If you have a rom open, you can open any of the levels by clicking on its name.  
The names are stored in resources/levelnames.cfg  
TODO: Allow the user to load a custom levelname list  