

package editorofswing.types;
import editorofswing.util.Rom;


public class CollisionMap implements java.io.Serializable, ChunkMap{
    private static final long serialVersionUID = 1L;
    
    private byte[] collision;
    public int width;
    public int height;
    
    public CollisionMap(int width, int height){
        this.width = width;
        this.height = height;
        collision = new byte[width*height];
    }
    
    public CollisionMap(Rom rom, int ptr, int w, int h){
	width = w;
        height = h;
        collision = new byte[w*h];
	
	//Chunk of a position is found like this, (x & y are pixel coords):
	//*(u32*)((climb_map + (y&0xFFFFFF00) / 16) + x/256 * 4)
	//With tile coords: *(u32*)((climb_map + (8*ty&0xFFFFFF00) / 16) + tx/32 * 4)
	for(int ty = 0;ty<h; ty+= 32){
	    for(int tx = 0;tx<w; tx+= 32){
		int chunkptr = rom.read32((ptr + (8*ty&0xFFFFFF00) / 16) + tx/32 * 4);
		
		//copy data from the chunk
		for(int y = 0;y<32;y++){
		    //TODO: Replace with rom.read() and arraycopy
		    for(int x = 0;x<32;x++){	
			collision[ty * w + tx + (y*w + x)] = rom.read8(chunkptr + (y*32 + x));
		    }
		}
		
	    }
	}
    }
    
    public byte getCollision(int x, int y){
        if(x>=width || y >= height){
            (new Exception("[CollisionMap::getCollision] Out of bounds: "+x+ ", "+y)).printStackTrace();
            return 0;
        }
        return collision[y*width + x];
    }
    
    public void setCollision(int x, int y, byte value){
        if(x>=width || y >= height){
            (new Exception("[CollisionMap::setCollision] Out of bounds: "+x+", "+y)).printStackTrace();
            return;
        }
        collision[y*width + x] = value;
    }
    
    public void insert(CollisionMap col, int x, int y){
	for(int i = y;i<y+col.height;i++){
	    for(int j = x;j<x+col.width;j++){
		if(i < this.height && i-y<col.height && j<this.width && j-x<col.width){
		    this.setCollision(j,i, col.getCollision(j-x,i-y));
		}
	    }
	}
    }
    
    
    public byte[] getChunk(int tx, int ty){
	tx&=~0x1F;
	ty&=~0x1F;
	byte[] chunk = new byte[32*32];
	for(int i = 0;i<32;i++){
	    for(int j = 0;j<32;j++){
		byte data = getCollision(tx+j, ty+i);
		chunk[32*i+j] = data;
	    }
	}
	return chunk;
    }
    
    
    
    @Override
    public void writeChunkToRom(Rom rom, int cx, int cy, int addr){
	if(cx*32 >= width || cy*32 >=height){
	    return;
	}
	for(int j = 0;j<32;j++){
	    for(int i = 0;i<32;i++){
		rom.write8(addr + i + j*32, getCollision(cx*32+i, cy*32+j));
	    }
	}
    }
    
    
    
    public void print(){
        System.out.println("Printing Collision: (width:"+width+" height:"+height);
        for(int i = 0;i<height;i++){
            for(int j = 0;j<width;j++){
                System.out.print(String.format("%02X ",getCollision(j,i)));
            }
            System.out.println("");
        }
    }
    
    @Override
    public boolean isChunkEmpty(int cx, int cy){
	if(cx*32 >= width || cy*32 >=height){
	    return true;
	}
	for(int j = 0;j<32;j++){
	    for(int i = 0;i<32;i++){
		if(getCollision(cx*32+i, cy*32+j) != 0){
		    return false;
		}
	    }
	}
	return true;
    }
    
    @Override
    public boolean isChunkEmpty(int i){
	int cx = i % getWChunks();
	int cy = i / getHChunks();
	return isChunkEmpty(cx, cy);
    }
    
    @Override
    public int getEmptyChunkCount(){
	int cw = width/32;
	int ch = height/32;
	int count = 0;
	for(int i = 0;i<cw;i++){
	    for(int j = 0;j<ch;j++){
		if(isChunkEmpty(i, j)){
		    count++;
		}
	    }
	}
	return count;
    }
    
    @Override
    public int getWChunks(){
	return width / 32;
    }
    
    @Override
    public int getHChunks(){
	return height / 32;
    }
    
    @Override
    public int getChunkSize(){
	return 32*32;
    }
    
    public void resize(int w, int h){
	byte newmap[] = new byte[w*h];
	for(int y = 0;y<Math.min(h, height);y++){
	    System.arraycopy(collision, y*width, newmap, y*w, Math.min(width, w));
	}
	collision = newmap;
	width = w;
	height = h;
    }
}
