
package editorofswing;

import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;
import editorofswing.util.*;
import java.awt.Rectangle;
import java.util.ArrayList;

public class SpriteTileAllocTest {
    
    private Rom rom;
    
    @Before
    public void loadRom(){
        try{
            rom = new Rom("baserom.gba");
            RomConstants.initOffsetsForRom(rom);
        }catch(Exception e){
             fail("unable to load baserom.gba for tests " + e.getMessage());
        }
    }
    
    @Test
    public void testFailingTileAlloc() {
		SpriteInserter ins = new SpriteInserter(rom);
		assertEquals(-1, ins.findFreeTileNum(RomConstants.SpriteType.REGULAR, 2));
    }

    @Test
    public void testAllocAfterFree() {
		SpriteInserter ins = new SpriteInserter(rom);
                ins.freeSpriteTiles(RomConstants.SpriteType.REGULAR, 0x69);
		assertNotEquals(-1, ins.findFreeTileNum(RomConstants.SpriteType.REGULAR, 20));
    }
    
    @Test
    public void testAllocAfterFree2() {
		SpriteInserter ins = new SpriteInserter(rom);
                ins.freeSpriteTiles(RomConstants.SpriteType.REGULAR, 0x69);
                int free = ins.findFreeTileNum(RomConstants.SpriteType.REGULAR, 20);
		assertNotEquals(-1, free);
                rom.fill(free*0x20 + RomConstants.getSpriteTilePtr(RomConstants.SpriteType.REGULAR), 0x20*20, (byte)1);
                assertEquals(-1, ins.findFreeTileNum(RomConstants.SpriteType.REGULAR, 20));
    }
    
    @Test
    public void testDoubleFree() {
		SpriteInserter ins = new SpriteInserter(rom);
                ins.freeSpriteTiles(RomConstants.SpriteType.REGULAR, 0x69);
                int free = ins.findFreeTileNum(RomConstants.SpriteType.REGULAR, 20);
		assertNotEquals(-1, free);
                rom.fill(free*0x20 + RomConstants.getSpriteTilePtr(RomConstants.SpriteType.REGULAR), 0x20*20, (byte)1);

                ins.freeSpriteTiles(RomConstants.SpriteType.REGULAR, 0x69);
                assertEquals(-1, ins.findFreeTileNum(RomConstants.SpriteType.REGULAR, 20));
    }


}




