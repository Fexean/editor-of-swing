# Map-tab

In this tab you can edit the BG1 tilemap, which is where the pegboards and level layout is drawn on.
If you want to edit the other background layers, you can use the background exporter/importer tool in the other tab instead.

![Tiles tab](https://gitlab.com/Fexean/editor-of-swing/-/raw/master/docs/editor/screenshots/maptab_tiles.png "map tab screenshot")  

On the right side of the window you can select whether you want to use tiles or blocks.
In the tiles tab you can select any tile from the level's tileset, a palette and vertical/horizontal flipping, and then place that tile on the map by left clicking.

Building a level tile by tile would take forever which is why the editor also has blocks. Blocks are rectangular areas consisting of tiles *and collisions*.
In the blocks tab you can see a blockset. Similar to the tiles you can select a block by clicking on it and you can place it on the map by left clicking.
The editor has 8 built-in blocksets each of which is meant for a different level tileset. These blocksets mostly just contain different sized pegboards.

![Block tab](https://gitlab.com/Fexean/editor-of-swing/-/raw/master/docs/editor/screenshots/maptab_blocks.png "Block tab screenshot")  

You can also create your own blocks. Hold down right click and drag your mouse to select an area on the map and when you let go of right click the area gets copied as a temporary block. You can then place it on the map or add this block to the blockset by clicking the "Add selected to blocks" button.
Changes made to the blocksets are not permanent and will be lost when a different blockset is loaded or the program is closed, which is why the block tab also has buttons for saving the current blockset to a file and loading a blockset from a file.


## Controls

Left click: place tile or block  
Right click: copy tile  
Right click (dragged): copy block  
X: When held, the cursor's Y-coordinate is locked to its current value  
C: When held, the cursor's X-coordinate is locked to its current value  
Ctrl + Z: Undo  
Ctrl + Y: Redo  