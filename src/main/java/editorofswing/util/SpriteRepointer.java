
package editorofswing.util;


public class SpriteRepointer {
    
    Rom rom;
    public SpriteRepointer(Rom rom){
        this.rom = rom;
    }
    
    public void setSpriteTilePointers(int regular, int affine, int player){
        rom.write32(RomConstants.spritePtrLiteralPool+8, regular | 0x8000000);
        rom.write32(RomConstants.spritePtrLiteralPool+4, affine | 0x8000000);
        rom.write32(RomConstants.spritePtrLiteralPool, player | 0x8000000);
        RomConstants.initOffsetsForRom(rom); //refresh constants
    }
    
    /***
     * tilecount arrays are in order: affine, regular, player
     * @param location
     * @param oldTileCounts
     * @param newTileCounts 
     */
    public void repointSpriteTiles(int freespace, int[] oldTileCounts, int[] newTileCounts){
        final int spaceNeeded = 0x20 * (newTileCounts[0] + newTileCounts[1] + newTileCounts[2]);
        rom.fill(freespace, spaceNeeded, (byte)0);
        int affineTiles = freespace;
        int regularTiles = freespace + newTileCounts[0]*0x20;
        int playerTiles = freespace + (newTileCounts[0]+newTileCounts[1])*0x20;
        RomMisc.copy(rom, RomConstants.affineSprTiles, affineTiles, oldTileCounts[0]*0x20);
        RomMisc.copy(rom, RomConstants.regularSprTiles, regularTiles, oldTileCounts[1]*0x20);
        RomMisc.copy(rom, RomConstants.playerSprTiles, playerTiles, oldTileCounts[2]*0x20);
        setSpriteTilePointers(regularTiles, affineTiles, playerTiles);
    }
    
}
