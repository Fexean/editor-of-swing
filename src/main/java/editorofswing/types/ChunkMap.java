
package editorofswing.types;

import editorofswing.util.Rom;


public interface ChunkMap {
    public int getHChunks();
    public int getWChunks();
    public boolean isChunkEmpty(int x, int y);
    public boolean isChunkEmpty(int i);
    public int getEmptyChunkCount();
    public int getChunkSize();
    
    public void writeChunkToRom(Rom rom, int cx, int cy, int addr);
    
    //for debugging
    public static void printChunkPointers(Rom rom, int addr, int width, int height){
	for(int ty = 0;ty<height; ty+= 32){
	    for(int tx = 0;tx<width; tx+= 32){
		int chunkptr = rom.read32((addr + (8*ty&0xFFFFFF00) / 16) + tx/32 * 4);
		System.out.print(String.format("%04X ", chunkptr));
	    }
	    System.out.println("");
	}
    }    
}
