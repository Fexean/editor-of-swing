package editorofswing.util;

import javafx.util.Pair;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.File;

import editorofswing.types.*;
import java.util.ArrayList;

public class BackgroundImporter {
    
    public static void importBackground1(File file, Map map, boolean clearBg1) throws Exception{
	if(clearBg1){
	    map.tileset1 = new Tileset(1);
	    map.layer1 = new Tilemap(map.wChunks*32, map.hChunks*32);
	    return;
	}
	BufferedImage img = ImageIO.read(file);
	if(img.getWidth() != map.w || img.getHeight() != map.h){
	    throw new IllegalArgumentException("Image must be the same size as the map.");
	}
	Pair<Tileset, Tilemap> result = createImageTilesetTilemap(img, new ArrayList<Tile>());
	if(result.getKey().length() <= 512){
	    map.tileset1 = result.getKey();
	    map.layer1 = result.getValue();
	}else{
	    throw new IllegalArgumentException("Primary tileset can only have 512 tiles, this image has " + result.getKey().length());
	}
    }
    
    public static void importSecondaryBackgrounds(File bg2file, File bg3file, Map map, boolean clearBg2, boolean clearBg3) throws Exception{
	Pair<Tileset, Tilemap> bg3result, bg2result;
	
	//create bg3 tiles & tilemap
	if(!clearBg3){
	    BufferedImage img = ImageIO.read(bg3file);
	    if(img.getWidth() != map.w || img.getHeight() != map.h){
		throw new IllegalArgumentException("(BG3) Image must be the same size as the map.");
	    }
	    ArrayList<Tile> tiles = new ArrayList<Tile>();
	    tiles.add(new Tile()); //add empty transparent tile as the first tile to allow clearing backgrounds by just filling tilemap with 0s
	    bg3result = createImageTilesetTilemap(img, tiles);
	}else{
	    bg3result = new Pair<Tileset, Tilemap>(new Tileset(1), new Tilemap(map.wChunks*32, map.hChunks*32));
	    bg3result.getValue().fill((short)0);
	}
	
	//create bg2 tiles & tilemap
	if(!clearBg2){
	    BufferedImage img = ImageIO.read(bg2file);
	    if(img.getWidth() != map.w || img.getHeight() != map.h){
		throw new IllegalArgumentException("(BG2) Image must be the same size as the map.");
	    }
	    bg2result = createImageTilesetTilemap(img, bg3result.getKey().getTiles());
	}else{
	    bg2result = new Pair<Tileset, Tilemap>(bg3result.getKey(), new Tilemap(map.wChunks*32, map.hChunks*32));
	    bg2result.getValue().fill((short)0);
	}
	
	if(bg2result.getKey().length() <= 760){
	    map.layer2 = bg2result.getValue();
	    map.layer3 = bg3result.getValue();
	    map.tileset2 = bg2result.getKey();
	}else{
	    throw new IllegalArgumentException("Secondary tileset can only have 760 tiles, these images have " + (bg2result.getKey().length()));
	}
    }
    
    private static Pair<Tileset, Tilemap> createImageTilesetTilemap(BufferedImage img, ArrayList<Tile> tiles) throws Exception{
	if(img.getType() != BufferedImage.TYPE_BYTE_INDEXED){
	    throw new IllegalArgumentException("Image is not indexed!");
	}
	final int w = img.getWidth() / 8;
	Tilemap tmap = new Tilemap(w, img.getHeight() / 8);
	
	//for each tile in image
	for(int i = 0;i<img.getHeight()*w/8;i++){
	    int x = (i%w)*8;
	    int y = (i/w)*8;
	    Tile t = new Tile(img, x, y, true);
	    byte palnum = getOptimalPalNum(img, x, y);
	    
	    //check if tile is in tileset
	    int idx = tiles.indexOf(t);
	    if(idx != -1){
		tmap.setTile(x/8, y/8, (short)idx, palnum, false, false);
		continue;
	    }
	    
	    //check with hflip
	    idx = tiles.indexOf(Tile.copyFlipped(t, true, false));
	    if(idx != -1){
		tmap.setTile(x/8, y/8, (short)idx, palnum, true, false);
		continue;
	    }
	    
	    //check with vflip
	    idx = tiles.indexOf(Tile.copyFlipped(t, false, true));
	    if(idx != -1){
		tmap.setTile(x/8, y/8, (short)idx, palnum, false, true);
		continue;
	    }
	    
	    //check with hflip + vflip
	    idx = tiles.indexOf(Tile.copyFlipped(t, true, true));
	    if(idx != -1){
		tmap.setTile(x/8, y/8, (short)idx, palnum, true, true);
	    }else{
		//tile not in tileset, add tile
		tmap.setTile(x/8, y/8, (short)tiles.size(), palnum, false, false);
		tiles.add(t);
	    }
	    
	}
	return new Pair(new Tileset(tiles), tmap);
    }
    
    
    /*
	Returns the most used palette number in a tile withing img, ignoring transparent pixels
    */
    private static byte getOptimalPalNum(BufferedImage img, int x, int y){
	final int colorIndex[] = new int[1];
	final int colorPalCounts[] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	for(int j = 0;j<8;j++){
	    for(int i = 0;i<8;i++){
		img.getRaster().getPixel(i+x, j+y, colorIndex);
		
		//ignore transparent pixels
		if(colorIndex[0] % 16 != 0){
		    final int palnum = colorIndex[0] / 16;
		    colorPalCounts[palnum]++;
		}
	    }
	}
	
	//find most common palette, default to 0
	int max_idx = 0;
	int max_val = colorPalCounts[0];
	for(int i = 1;i<16;i++){
	    if(colorPalCounts[i] > max_val){
		max_val = colorPalCounts[i];
		max_idx = i;
	    }
	}
	return (byte)max_idx;
    }
}
