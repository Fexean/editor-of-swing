
package editorofswing.cli;

import editorofswing.util.Rom;
import editorofswing.util.RomConstants;


public class CliUtil {

    public static void exit(String msg){
	System.out.println(msg);
	System.exit(0);
    }
    
    public static Rom tryOpenRom(String romname){
	Rom rom = null;
	try{
	    rom = new Rom(romname);
	    if(!rom.isVersionSupported()){
		exit("Unsupported rom version.");
	    }
	    RomConstants.initOffsetsForRom(rom);
	}catch(Exception e){
	    exit("Failed to open rom");
	}
	return rom;
    }
    
    public static int parseKeyValueInt(String arg){
        if(!arg.contains("=")){
            CliUtil.exit("Expected integer kay-value pair, got: " + arg);
        }
        
        String keyname = arg.substring(0, arg.indexOf("="));
        try {
            return Integer.parseInt(arg.substring(1 + arg.indexOf("=")), 16);
        } catch (Exception e) {
            CliUtil.exit(keyname+" must be an integer:" + arg);
        }
        return -1;
    }
    
    public static void trySaveRom(Rom rom){
	try{
	    rom.save();
	}catch(Exception e){
	    exit("Failed to save rom");
	}
    }
    
    public static void printHelp(String helpStrings[]){
	for(String s : helpStrings){
	    System.out.println(s);
	}
    }
}
