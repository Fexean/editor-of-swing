package editorofswing.types;
import java.awt.Color;
import editorofswing.util.Rom;

import java.awt.image.BufferedImage;

public class Tile implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
    
	public byte[] pixels;

        
        
        public Tile(){
            pixels = new byte[64];
        }
        
        
        public short getColorIndex(byte x, byte y){
            if(y*8+x >= pixels.length){
                new Exception("[Tile::getColorIndex] out of bound").printStackTrace();
		return 0;
            }
            return (short)(pixels[x+y*8] & 0xFF);
        }
        
        public void setColorIndex(byte value, byte x, byte y){
            if(y*8+x >= pixels.length){
                new Exception("[Tile::setColorIndex] out of bound").printStackTrace();
		return;
            }
            pixels[y*8+x] = value;
        }
        
        public void storeToRom4bpp(Rom rom, int address){
            rom.write(getRaw(), address);
        }
	
	public byte[] getRaw(){
            byte[] raw = new byte[32];
            for(int i = 0;i<raw.length;i++){
                raw[i] = (byte)(pixels[2*i]&0xF);
                raw[i] |= (byte)((pixels[2*i+1]&0xF)<<4);
            }
	    return raw;
	}
        
        
        public void printTile(){
            System.out.println(pixels.length + " Tiles: ");
            for(int i = 0;i<pixels.length;i++){
                System.out.print(String.format("0x%02X",pixels[i])+" ");
            }
            System.out.println("");
        }
        
        
        public static Tile loadTile4bpp(Rom rom, int address){
            byte[] raw = rom.read(address, 32);
            return loadTile4bpp(raw, 0);
        }
        
        public static Tile loadTile4bpp(byte[] raw, int offset){
            Tile tmp = new Tile();
            int dst = 0;
            for(int i = 0;i<32;i++){
                tmp.pixels[dst] = (byte)(raw[i+offset]&0xF);
                tmp.pixels[dst+1] = (byte)((raw[i+offset]&0xF0)>>4);
                dst+=2;
            }
            return tmp;
        }
        
        public Tile(Tile t){
	    pixels = new byte[64];
	    System.arraycopy(t.pixels, 0, this.pixels, 0, 64);
	}
	
	
	
	public Tile(BufferedImage img, int x, int y, boolean _4bpp){
	    if(img.getType() != BufferedImage.TYPE_BYTE_INDEXED){
		throw new IllegalArgumentException("File is not a indexed image!");
	    }
	    int colorIndex[] = new int[1];
	    pixels = new byte[64];
	    for(int i = y;i<y+8;i++){
		for(int j = x;j<x+8;j++){
		    img.getRaster().getPixel(j, i, colorIndex);
		    if(_4bpp){
			colorIndex[0] %= 16;
		    }
		    pixels[8*(i-y)+j-x] = (byte)colorIndex[0];
		}
	    }
	}
	
	
	
	public boolean isClear(){
	    for(int i = 0;i<64;i++){
		if(pixels[i] % 16 != 0){
		    return false;
		}
	    }
	    return true;
	}
	
	
	public void drawTileToBufferedImage(Palette pal, BufferedImage img, int x, int y){
	    drawTileToBufferedImage(pal, img, x, y, 0, false, false);
	}
	
	public void drawTileToBufferedImage(Palette pal, BufferedImage img, int destX, int destY, int palNumber, boolean hflip, boolean vflip){
	    int colorindex[] = new int[1];
	    for(int i = 0;i<8;i++){
		for(int j = 0;j<8;j++){
		    int xOfs = hflip ? (7-i) : i;
		    int yOfs = vflip ? (7-j) : j;
		    short index = getColorIndex((byte)(xOfs), (byte)(yOfs));
		    colorindex[0] = index + 16*palNumber;
		    img.getRaster().setPixel(destX+i, destY+j, colorindex);
		}
	    }
	}
	
	
	@Override
	public boolean equals(Object other){
	    if(other instanceof Tile){
		for(int i = 0;i<64;i++){
		    if(((Tile) other).pixels[i] != pixels[i]){
			return false;
		    }
		}
		return true;
	    }
	    return false;
	}
	
	public static Tile copyFlipped(Tile t, boolean hFlip, boolean vFlip){
	    Tile flipped = new Tile();
	    for(byte i = 0;i<8;i++){
		for(byte j = 0;j<8;j++){
		    int x = hFlip ? (7-j) : j;
		    int y = vFlip ? (7-i) : i;
		    flipped.setColorIndex((byte)t.getColorIndex((byte)x, (byte)y), j, i);
		}
	    }
	    return flipped;
	}
	    
}
