# DK: King of Swing Documentation
*Note: All information & memory addresses are for the European version unless stated otherwise*  
[About](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/king_of_swing/about.md)  
[Object](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/king_of_swing/object.md)  
[Level data](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/king_of_swing/leveldata.md)  
[Entity](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/king_of_swing/entity.md)
(
[Pegs](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/king_of_swing/entities/entities_pegs.MD), [Enemies](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/king_of_swing/entities/entities_enemies.MD), [Items](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/king_of_swing/entities/entities_items.MD), [Levers](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/king_of_swing/entities/entities_levers.MD), [Visual effects](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/king_of_swing/entities/entities_fx.MD), [Other](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/king_of_swing/entities/entities_other.MD), [Unused](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/king_of_swing/entities/entities_unused.md)
)  
[Sprite engine](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/king_of_swing/sprite.md)  
[Main loop](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/king_of_swing/mainloop.md)  
[Save data](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/king_of_swing/save.md)  
[World map](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/king_of_swing/worldmap.md)  
[Time attack mode](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/king_of_swing/time_attack.md)  
[Audio](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/king_of_swing/audio.md)  
[Text](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/king_of_swing/text.md)  
[Backgrounds](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/king_of_swing/backgrounds.md)  
[Playable characters](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/king_of_swing/characters.md)  
[Password screen](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/king_of_swing/passwords.md)  
[Symbol map](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/king_of_swing/symbols.md)  


# EditorOfSwing Documentation
[Main window](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/editor/mainwindow.md)
(
[Map](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/editor/map_tab.md)
[Collisions](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/editor/collision_tab.md)
[Climbing](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/editor/climb_tab.md)
[Entities](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/editor/entity_tab.md)
[Camera](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/editor/camera_tab.md)
[Other](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/editor/other_tab.md)
)  
[Time attack editor](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/editor/time_attack_editor.md)  
[Command line interface](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/editor/cli_insert.md)  
