package editorofswing.util;
import java.awt.image.BufferedImage;
import editorofswing.types.*;
import editorofswing.types.sprites.AffineSprite;
import editorofswing.types.sprites.RegularSprite;
import java.awt.Rectangle;
import java.util.ArrayList;
import javafx.util.Pair;
import editorofswing.types.sprites.SubSprite;

//TODO: don't store repeating tiles
public class SpriteInserter {
    
    private Rom rom;
    
    public SpriteInserter(Rom rom){
        this.rom = rom;
    }
    
    private Pair<Integer, Integer> getSpriteSizeShape(int w, int h){
        int size, shape;
        if(w == 8 && h == 8) return new Pair<>(0, 0);
        if(w == 16 && h == 8) return new Pair<>(0, 1);
        if(w == 8 && h == 16) return new Pair<>(0, 2);
        if(w == 16 && h == 16) return new Pair<>(1, 0);
        if(w == 32 && h == 8) return new Pair<>(1, 1);
        if(w == 8 && h == 32) return new Pair<>(1, 2);
        if(w == 32 && h == 32) return new Pair<>(2, 0);
        if(w == 32 && h == 16) return new Pair<>(2, 1);
        if(w == 16 && h == 32) return new Pair<>(2, 2);
        if(w == 64 && h == 64) return new Pair<>(3, 0);
        if(w == 64 && h == 32) return new Pair<>(3, 1);
        if(w == 32 && h == 64) return new Pair<>(3, 2);
         return new Pair<>(-1, -1);
    }
    
    private boolean isValidSpriteSize(int w, int h){
        return getSpriteSizeShape(w,h).getValue() != -1;
    }
    
    private int writeAffineSprite(int templateAddr, BufferedImage img, int tileNum, int tilesetBaseAddr) throws Exception{
        int w = img.getWidth();
        int h = img.getHeight();
        Pair<Integer, Integer> sizeShape = getSpriteSizeShape(w,h);
        rom.write8(templateAddr, (byte)(h / 2)); //yOffset
        rom.write8(templateAddr+1, (byte)(sizeShape.getValue() << 6)); //atttr0_top8
        rom.write8(templateAddr+2, (byte)(w / 2)); //xOffset
        rom.write8(templateAddr+3, (byte)(sizeShape.getKey() << 6)); //attr1_top8
        
        //calculate preAlloc & postAlloc values
        Tileset t = new Tileset(img);
        boolean foundNonEmpty = false;
        int postAlloc = 0;
        int preAlloc = 0;
        for(int i = 0;i<w*h/64;i++){
            if(t.getTile(i).isClear()){
                postAlloc++;
                if(!foundNonEmpty)
                    preAlloc++;
            }else{
                foundNonEmpty = true;
                postAlloc = 0;
            }
        }
        
        //store tiles and tilemap
        int tilecount = (w*h/64) - postAlloc - preAlloc;
        for(int i = 0;i<tilecount;i++){
            rom.write16(templateAddr+6+2*i, (short)(i+tileNum)); //tilemap
            t.getTile(i + preAlloc).storeToRom4bpp(rom, tilesetBaseAddr + (i+tileNum) * 0x20);
        }
        
        rom.write8(templateAddr+5, (byte)(tilecount)); //tilecount
        rom.write8(templateAddr+4, (byte)(preAlloc)); //prealloc
        rom.write16(templateAddr+6+2*tilecount, (byte)(postAlloc)); //postalloc
        return tilecount - preAlloc - postAlloc;
    }
    
    public int insertImgAsAffineSprite(int graphicId, BufferedImage img, int freespace, int tileNum) throws Exception{
        if(!isValidSpriteSize(img.getWidth(), img.getHeight())){
            throw new Exception(String.format("Invalid affine sprite size: (%dx%d)", img.getWidth(), img.getHeight()));
        }
        int tilesUsed = writeAffineSprite(freespace, img, tileNum, RomConstants.affineSprTiles);
        rom.write32(RomConstants.affineSprTemplates + graphicId*4, 0x8000000|freespace);
        return tileNum + tilesUsed;
    }
    
    public int insertImgAsPlayerSprite(int graphicId, BufferedImage img, int freespace, int tileNum) throws Exception{
        if(!isValidSpriteSize(img.getWidth(), img.getHeight())){
            throw new Exception(String.format("Invalid player sprite size: (%dx%d)", img.getWidth(), img.getHeight()));
        }
        int tilesUsed = writeAffineSprite(freespace, img, tileNum, RomConstants.playerSprTiles);
        rom.write32(RomConstants.playerSprTemplates + graphicId*4, 0x8000000|freespace);
        return tileNum + tilesUsed;
    }
    
    
    public int insertImgAsRegularSprite(int graphicId, BufferedImage img, int freespace, int tileNum) throws Exception{
        if(img.getWidth() % 8 != 0 || img.getHeight() % 8 != 0){
            throw new Exception(String.format("Invalid regular sprite size: (%dx%d)", img.getWidth(), img.getHeight()));
        }
        ArrayList<Rectangle> subspriteRects = RectUtil.splitRectsIntoSprites(img.getWidth(), img.getHeight());
        rom.write32(RomConstants.regularSprTemplates + graphicId*4, 0x8000000|freespace);
        rom.write16(freespace, (short)subspriteRects.size());
        
        int subPtr = freespace + 2;
        for(Rectangle sub : subspriteRects){
            SubSprite subspr = new SubSprite(sub.width, sub.height);
            subspr.yOffset = (byte)(sub.y-img.getHeight()/2);
            subspr.xOffset = (byte)(sub.x-img.getWidth()/2);
            subspr.flipXOffset = (byte)(-sub.width/2);
            tileNum = insertRegularSpriteRectTiles(img, sub, subspr, tileNum);
            
            subspr.writeToRom(rom, subPtr);
            subPtr += 8+2*(sub.width * sub.height / 64);
        }
        return tileNum;
    }
    
    /***
     * Inserts the tiles needed by a subsprite into the global tileset and builds tilemap for subsprite.
     * @param rect - area of image used by subsprite 
     * @param img - sprite image 
     * @param sub - subsprite (contains the tilemap)
     * @param tileNum
     * @return tileNum after insertions
     * @throws Exception 
     */
    private int insertRegularSpriteRectTiles(BufferedImage img, Rectangle rect, SubSprite sub, int tileNum) throws Exception{
        Tileset tiles = new Tileset(img.getSubimage(rect.x, rect.y, rect.width, rect.height));
        for(int i = 0;i < sub.subspriteTileCount;i++){
            Tile t = tiles.getTile(i);
            if(t.isClear()){
                sub.tmap[i] = 0;
            }else{
                sub.tmap[i] = (short)(tileNum);
                t.storeToRom4bpp(rom, 0x20*tileNum + RomConstants.regularSprTiles);
                tileNum += 1;
            } 
        }
        return tileNum;
    }
    
    
    public int findFreeTileNum(RomConstants.SpriteType type, int tileCount){
        int tileTable = RomConstants.getSpriteTilePtr(type);
        int addr = RomMisc.findFreeSpace(rom, tileTable + 0x20, 0x20*tileCount, (byte)0);
        int tile = (addr  - (tileTable & 0x1FFFFFF)) / 0x20;
        if(addr == -1 || tile > 0xffff || tile < 0)
            return -1;
        return tile;
    }
    
    private void freeTile(RomConstants.SpriteType type, short tileId2){
        int tileId = Short.toUnsignedInt(tileId2);
        rom.fill(RomConstants.getSpriteTilePtr(type) + tileId*0x20, 0x20, (byte)0);
    }
    
    public int findFreeTileNum(RomConstants.SpriteType type, BufferedImage img){
        int tileId = findFreeTileNum(type, img.getHeight()*img.getWidth() / 64); //tileCount estimate, could be lowered
        return tileId;
    }
    
    
    private void freeAffineSpriteTiles(int graphicId){
        AffineSprite spr = AffineSprite.fromGraphicId(rom, graphicId);
        for(int i = 0;i<spr.tilecount;i++){
            freeTile(RomConstants.SpriteType.AFFINE, spr.tmap[i]);
	}
        spr.preAlloc += spr.tilecount+spr.postAlloc;
        spr.postAlloc = 0;
        spr.tilecount = 0;
        spr.writeToRomAffine(rom, graphicId);
    }
    
    private void freePlayerSpriteTiles(int graphicId){
        AffineSprite spr = AffineSprite.fromPlayerGraphicId(rom, graphicId);
        for(int i = 0;i<spr.tilecount;i++){
            freeTile(RomConstants.SpriteType.AFFINE, spr.tmap[i]);
	}
        spr.preAlloc += spr.tilecount+spr.postAlloc;
        spr.postAlloc = 0;
        spr.tilecount = 0;
        spr.writeToRomPlayer(rom, graphicId);
    }
    
    
    private void freeRegularSpriteTiles(int graphicId){
        try{
            int template = rom.read32(RomConstants.getListValue(RomConstants.Table.REGULAR_SPR_TEMPLATE, graphicId));
            RegularSprite spr = new RegularSprite(rom, template);
            spr.subsprites.forEach(sub -> {
                for(int i = 0;i<sub.tmap.length;i++){
                    if(sub.tmap[i] != 0)
                        freeTile(RomConstants.SpriteType.REGULAR, sub.tmap[i]);
                }
            });
            spr.subsprites.clear();
            spr.writeToRom(rom, template);
        }catch(Exception e){
            System.out.println("freeRegularSpriteTiles failed: "+e.getMessage());
        }
    }
    public void freeSpriteTiles(RomConstants.SpriteType type, int graphicId){
        switch(type){
            case AFFINE: 
                freeAffineSpriteTiles(graphicId);
            break;
            case REGULAR: 
                freeRegularSpriteTiles(graphicId);
            break;   
            case PLAYER:
                freePlayerSpriteTiles(graphicId);
            break;
        }
    }
    
}
