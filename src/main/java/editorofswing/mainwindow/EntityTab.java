
package editorofswing.mainwindow;


import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.canvas.*;

import javafx.geometry.Insets;

import java.util.ArrayList;

import javafx.scene.control.Tab;


import editorofswing.util.ConfigReader;


import java.util.HashMap;

import editorofswing.types.*;

import editorofswing.util.MapEditor;
import editorofswing.util.Rom;
import editorofswing.rendering.EntityRenderer;
import editorofswing.ui.util.CategoryChoiceBox;

public class EntityTab extends Tab{
    
    BorderPane root;
    Canvas canvas;
    Pane canvasPane;
    
    EntityRenderer renderer;
    MapEditor editor;
    
    Entity selectedEnt;
    TextField xField, yField, callbackField, flagField, tableidField;
    CategoryChoiceBox templateBox = new CategoryChoiceBox();
    
    VBox toolRoot, entRoot;
    editorofswing.ui.util.Spinbox entitySelector;
    Label entityCntLabel = new Label();
    Label entityPalSlotLabel = new Label();
    
    HashMap<String, EntityTemplate> entTemplates;
    ArrayList<String> entityNames;
	 
    boolean disableTemplateBoxCallback = false; //this is to prevent templateBox listener running when an entity is selected
    
    public EntityTab(){
	xField = new TextField();
	yField = new TextField();
	callbackField = new TextField();
	flagField = new TextField();
	tableidField = new TextField();
	
	canvasPane = new Pane();
	root = new BorderPane();
	canvas = new Canvas();
	this.renderer = new EntityRenderer(canvas);
	
	ScrollPane canvasScroll = new ScrollPane();
	canvasScroll.setContent(canvasPane);
	root.setCenter(canvasScroll);
	
	ScrollPane toolScroll = new ScrollPane();
	
	
	entRoot = new VBox();
	toolRoot = new VBox();
	toolScroll.setContent(toolRoot);
	
	Button addButton = new Button("New entity...");
	toolRoot.setSpacing(10);
	toolRoot.setPadding(new Insets(0, 10, 10, 10)); 
	
	
	entitySelector = new editorofswing.ui.util.Spinbox(0, 0, 0);
	toolRoot.getChildren().add(new Label("Selected Entity:"));
	toolRoot.getChildren().add(entitySelector);
	toolRoot.getChildren().add(entityCntLabel);
	toolRoot.getChildren().add(addButton);
	
	entRoot.setSpacing(10);
	entRoot.setPadding(new Insets(0, 10, 10, 10)); 
	
	entRoot.getChildren().add(new Label("Templates:"));
	entRoot.getChildren().add(templateBox);
	entRoot.getChildren().add(new Label("X:"));
	entRoot.getChildren().add(xField);
	entRoot.getChildren().add(new Label("Y:"));
	entRoot.getChildren().add(yField);
	entRoot.getChildren().add(new Label("Entity table:"));
	entRoot.getChildren().add(tableidField);
	entRoot.getChildren().add(new Label("Callback:"));
	entRoot.getChildren().add(callbackField);
	entRoot.getChildren().add(new Label("Parameter:"));
	entRoot.getChildren().add(flagField);
	entRoot.getChildren().add(entityPalSlotLabel);
	
	Button deleteButton = new Button("Delete Entity");
	entRoot.getChildren().add(deleteButton);
	entRoot.setVisible(false);
	toolRoot.getChildren().add(entRoot);
	
	setText("Entities");
	root.setRight(toolScroll);
	setContent(root);
	
	canvas.setOnMousePressed(e->{onCanvasClicked(e.getX(), e.getY(), e.getButton());});
	canvas.setOnMouseDragged(e->{onCanvasDragged(e.getX(), e.getY(), e.getButton());});
	canvas.setOnMouseReleased(e->{dragging = false;});
	
	xField.focusedProperty().addListener(e->{updateEntityFromFields();});
	yField.focusedProperty().addListener(e->{updateEntityFromFields();});
	callbackField.focusedProperty().addListener(e->{updateEntityFromFields();updateTemplateBox();});
	flagField.focusedProperty().addListener(e->{updateEntityFromFields();});
	
	deleteButton.setOnAction(e->{onDeleteButtonClicked();});
	addButton.setOnAction(e->{onAddButtonClicked();});
	
	entitySelector.setOnValueChange((observable, oldValue, newValue) -> {
	    int val = entitySelector.getValue();
	    if(val < editor.getMap().entities.size()){
		selectedEnt = editor.getMap().entities.get(val);
		updateEntityFields();
		redraw();
	    }
	});
	
	
	templateBox.setOnSelection(e->{
	    if(!disableTemplateBoxCallback && entTemplates.containsKey(templateBox.getSelected())){
		final EntityTemplate tmp = entTemplates.get(templateBox.getSelected());
		callbackField.setText(String.format("0x%06X", tmp.callback));
		if(tmp.objectTable != -1){
		    tableidField.setText(String.format("0x%06X", tmp.objectTable));
		}
		updateEntityFromFields();
	    }
	});
	
    }
    
    
    public void loadEntityTemplates(editorofswing.util.Rom.Version version){
	final String filename = version + "_VERSION/entities.cfg";
	entityNames = ConfigReader.getKeys(filename);
	entTemplates = ConfigReader.readEntityTemplates(filename);
	templateBox.clear();
	templateBox.addCategories("Pegs", "Enemies", "Items", "Levers", "Visual", "Misc", "Other", "Special Pegs");
	for(String s : entityNames){
	    templateBox.addOption(s, entTemplates.get(s).category);
	}
	renderer.setEntityTemplates(entTemplates);
    }
    
    
    private void onDeleteButtonClicked(){
	editor.getMap().entities.remove(selectedEnt);
	entitySelector.setValue(0);
	entRoot.setVisible(false);
	selectedEnt = null;
	editor.setModified();
	redraw();
	updateEntityCount();
    }
    
    private void onAddButtonClicked(){
	Entity ent = new Entity(0, 0, 0, 0, 0);
	editor.getMap().entities.add(ent);
	selectedEnt = ent;
	redraw();
	updateEntityFields();
	updateEntityCount();
	entitySelector.setValue(editor.getMap().entities.indexOf(ent));
	editor.setModified();
    }
    
    private void onCanvasDragged(double x, double y, javafx.scene.input.MouseButton button){	
	if(dragging && selectedEnt != null){
	    selectedEnt.x = (int)x;
	    selectedEnt.y = (int)y;
	    updateEntityFields();
	    redraw();
		
	    prevEntX = selectedEnt.x;
	    prevEntY = selectedEnt.y;
	    
	}
    }
    
    
    private void updateEntityFields(){
	xField.setText(String.format("0x%04X", selectedEnt.x));
	yField.setText(String.format("0x%04X", +selectedEnt.y));
	callbackField.setText(String.format("0x%08X", selectedEnt.callback));
	flagField.setText(String.format("0x%08X", selectedEnt.spr_field80));
	tableidField.setText(String.format("0x%08X", selectedEnt.spriteTableId));
	
	updateTemplateBox();
	if(!entTemplates.containsKey(templateBox.getSelected()) || entTemplates.get(templateBox.getSelected()).paletteSlot == -1){
	    entityPalSlotLabel.setText("Palette Slot: ???");
	}else{
	    entityPalSlotLabel.setText(String.format("Palette Slot: %d", entTemplates.get(templateBox.getSelected()).paletteSlot));
	}
	
	entRoot.setVisible(true);
    }
    
    private void updateTemplateBox(){
	if(selectedEnt == null){return;}
	templateBox.clearSelection();
	for(String template : entityNames){
	    if(selectedEnt.callback == entTemplates.get(template).callback){
		disableTemplateBoxCallback = true;
		templateBox.select(template);
		disableTemplateBoxCallback = false;
		return;
	    }
	}
    }
    
    private void updateEntityFromFields(){
	if(selectedEnt == null){return;}
	selectedEnt.x = editorofswing.util.StringUtil.strToInt(xField.getText());
	selectedEnt.y = editorofswing.util.StringUtil.strToInt(yField.getText());
	
	selectedEnt.callback = editorofswing.util.StringUtil.strToInt(callbackField.getText());
	selectedEnt.spr_field80 = editorofswing.util.StringUtil.strToInt(flagField.getText());
	selectedEnt.spriteTableId = editorofswing.util.StringUtil.strToInt(tableidField.getText());
	
	editor.setModified();
	redraw();
    }
    
    private void onCanvasClicked(double x, double y, javafx.scene.input.MouseButton button){
	if(x < 0 || y < 0 || x >= canvas.getWidth() || y >= canvas.getHeight()){
	    return;
	}
	updateEntityFromFields();
	Entity ent = editor.getEntityAt((int)x, (int)y);
	if(ent != null){    
	    switch(button){
		case PRIMARY:
		    selectedEnt = ent;
		    break;
		    
		case SECONDARY:
		    Entity clone = new Entity(ent.spriteTableId, ent.callback, ent.x, ent.y, ent.spr_field80);
		    editor.getMap().entities.add(clone);
		    selectedEnt = clone;
		    updateEntityCount();
		    editor.setModified();
		    break;
		    
		case MIDDLE:
		    selectedEnt = ent;
		    onDeleteButtonClicked();
		    return;
		    
		default:
		    return;
	    }
	    
	    updateEntityFields();
	    redraw();
	    entitySelector.setValue(editor.getMap().entities.indexOf(selectedEnt));
	    dragging = true;
	}
	
    }
    
    boolean dragging = false;
    int prevEntX, prevEntY;
    

    public Canvas getCanvas(){
	return canvas;
    }
    
    public void resizeCanvas(double w, double h){
	canvas.setWidth(w);
	canvas.setHeight(h);
    }
    
    public void setup(MapEditor editor, Canvas mapcanvas){
	renderer.setPalette(editor.getMap().sprpal);
	canvas.setWidth(mapcanvas.getWidth());
	canvas.setHeight(mapcanvas.getHeight());
	this.editor = editor;
	canvasPane.getChildren().clear();
	canvasPane.getChildren().add(canvas);
	canvasPane.getChildren().add(mapcanvas);
	canvas.toFront();
	
	entRoot.setVisible(false);
	selectedEnt = null;
	
	redraw();
	updateEntityCount();
	
	entitySelector.setValue(0);
	entRoot.setVisible(false);
	selectedEnt = null;
    }
    
    public void redraw(){
	canvas.getGraphicsContext2D().clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
	renderer.drawEntities(editor.getMap(), selectedEnt);
    }
    
    public void updateEntityCount(){
	int newmax = editor.getMap().entities.size()-1;
	if(entitySelector.getValue() > newmax){
	    entitySelector.setValue(0);
	}
	entityCntLabel.setText("Entities: " + (newmax + 1));
	entitySelector.setLimits(0, newmax);
    }
    
    public void setRendererRom(Rom rom){
	renderer.setRom(rom);
	renderer.setSpriteDrawing(rom != null);
    }
    
    public void clearRendererCache(){
        renderer.clearCache();
    }
    
}
