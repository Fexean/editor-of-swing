# Text
Most of the text in King of swing is embedded in graphics - usually sprites, but the game also has the ability to print strings onto a background using a variable-width font.

## Strings
At 0x886F8D8 there are pointers for each language. Each pointer points to a list of all the game's string using that language.  
Each string starts with a byte indicating the string's x-coordinate, followed by the string encoded in a slightly modified ASCII encoding. Newlines (0xA) are also followed by the x-coordinate of the next line.  
The strings are null-terminated.  
**TODO**: Document special characters.


## Fonts
The game has separate fonts for each language, pointers to them are located at 0x8850404.  
Each of the fonts is a list of 256 pointers, one for each character.  

### Character format
Each character starts with a byte describing the width of the character. The height is always 12 for every character.  
The width is followed by pixel data for the character. Each pixel is represented by a single byte containing the color index between 0 and 15. The pixels are organized in columns rather than rows (so the first 12 bytes are for the first column, and the next 12 bytes are for the second).  
Each character is 1+width*12 bytes long.  


## Printing code
To print a string, first the game calls the function prepareString(0x886F880) with the string's id in R0. This function stores the string's pointer to 0x300019C, the string's x-position to 0x300000C and the string's y-position to 0x300000D (always set to 8).  
Then the function putchar(0x8850318) is called once/frame to draw the string's characters on the screen one by one. This function doesn't take any arguments and instead uses the variables set by prepareString.  