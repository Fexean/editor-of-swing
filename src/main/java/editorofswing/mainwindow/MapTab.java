
package editorofswing.mainwindow;

import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.canvas.*;
import javafx.scene.paint.Color;
import java.io.File;
import java.util.ArrayList;

import javafx.scene.control.Tab;
import editorofswing.util.ConfigReader;
import javafx.stage.Stage;

import editorofswing.types.*;
import editorofswing.rendering.MapRenderer;
import editorofswing.ui.dialog.FileDialog;
import editorofswing.util.MapEditor;



public class MapTab extends Tab {
    MapEditor editor;
    MapRenderer renderer;
    Label bottomtext;
    
    Pane canvasPane;
    Canvas canvas, tileset_canvas, block_canvas, mapcanvas;
    MapRenderer tileset_renderer;
    MapRenderer block_renderer, map_renderer;
    Stage stage;
    
    int selectedTile = 0;
    int selectedPal = 0;
    boolean tileHflip, tileVFlip;
    ComboBox paletteBox;
    RadioButton vFlipButton, hFlipButton;
    
    TabPane toolpane;
    
    ArrayList<MapBlock> blockset;
    MapBlock selectionBuf;
    
    boolean horizontalLock = false;
    boolean verticalLock = false;
    int lockX, lockY;
    
    boolean dragging;
    boolean copyDragging;
    int screenCursorX, screenCursorY, screenCursorW, screenCursorH;
    int dragStartX, dragStartY;
    ComboBox blockset_selection;
    Button addSelectedBlockButton, deleteBlockButton, loadBlocksButton, saveBlocksButton;
    
    public MapTab(){
	tileset_canvas = new Canvas();
	block_canvas = new Canvas();
	canvas = new Canvas();
	canvasPane = new Pane();
	
	tileset_renderer = new MapRenderer(tileset_canvas);
	block_renderer = new MapRenderer(block_canvas);
	
	renderer = new MapRenderer(canvas);
	
	BorderPane root = new BorderPane();
	ScrollPane canvasScroll = new ScrollPane();
	canvasScroll.setContent(canvasPane);
	root.setCenter(canvasScroll);
	
	
	toolpane = new TabPane();
	toolpane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);
	Tab tilesetTab = new Tab("Tiles");
	
	VBox tilesetPane = new VBox();
	tilesetPane.getChildren().add(new Label("Tiles:"));
	tilesetPane.getChildren().add(tileset_canvas);
	tilesetPane.getChildren().add(new Label("Palette:"));
	
	final String palNames[] = {"Palette 0","Palette 1","Palette 2","Palette 3",
		"Palette 4","Palette 5","Palette 6","Palette 7",
		"Palette 8","Palette 9","Palette A","Palette B",
		"Palette C","Palette D","Palette E","Palette F"};
	paletteBox = new ComboBox();
	paletteBox.getItems().addAll(palNames);
	paletteBox.getSelectionModel().select(0);
	tilesetPane.getChildren().add(paletteBox);
	
	hFlipButton = new RadioButton("HFlip");
	vFlipButton = new RadioButton("VFlip");
	tilesetPane.getChildren().add(hFlipButton);
	tilesetPane.getChildren().add(vFlipButton);
	tilesetTab.setContent(tilesetPane);
	
	toolpane.getTabs().add(tilesetTab);
	
	Tab blocktab = new Tab("Blocks");
	VBox blockroot = new VBox();
	blockroot.getChildren().add(new ScrollPane(block_canvas));
	blockset_selection = new ComboBox();
	blockset_selection.getItems().addAll(0, 1, 2, 3, 4, 5, 6, 7, 8);
	blockroot.getChildren().add(new Label("Blocksets:"));
	blockroot.getChildren().add(blockset_selection);
	
	addSelectedBlockButton = new Button("Add selected to blocks");
	addSelectedBlockButton.setDisable(true);
	deleteBlockButton = new Button("Delete block");
	saveBlocksButton = new Button("Save Blockset");
	loadBlocksButton = new Button("Load Blockset");
	
	
	blockroot.getChildren().add(addSelectedBlockButton);
	blockroot.getChildren().add(deleteBlockButton);
	blockroot.getChildren().add(saveBlocksButton);
	blockroot.getChildren().add(loadBlocksButton);
	
	blocktab.setContent(new ScrollPane(blockroot));
	toolpane.getTabs().add(blocktab);
	root.setRight(toolpane);
	
	bottomtext = new Label();
	root.setBottom(bottomtext);
	
	setText("Map");
	setContent(root);
	
	tileset_canvas.setOnMouseClicked(e->{onTilesetClicked(e.getX(), e.getY(), e.getButton());});
	block_canvas.setOnMouseClicked(e->{onBlocksetClicked(e.getX(), e.getY());});
	
	canvas.setOnMousePressed(e->{
	    onMapClicked(e.getX(), e.getY(), e.getButton());
	    if(e.getButton() == javafx.scene.input.MouseButton.SECONDARY){
		copyDragging = true;
	    }
	    dragStartX = (int)(e.getX()/8)*8;
	    dragStartY = (int)(e.getY()/8)*8;
	    dragging = true;
	});
	
	canvas.setOnMouseDragged(e->{onMapClicked(e.getX(), e.getY(), e.getButton());});
	canvas.setOnMouseMoved(e->{onMapMouseMove(e.getX(), e.getY());});
	canvas.setOnMouseReleased(e->{
	    if(e.getButton() == javafx.scene.input.MouseButton.SECONDARY){
		copySelection();
	    }
	    copyDragging = false;
	    dragging = false;
	});
	
	paletteBox.getSelectionModel().selectedIndexProperty().addListener(e -> {
            selectedPal = paletteBox.getSelectionModel().getSelectedIndex(); drawTileset();
	});
	
	hFlipButton.setOnAction(e->{tileHflip = hFlipButton.selectedProperty().get(); drawTileset();});
	vFlipButton.setOnAction(e->{tileVFlip = vFlipButton.selectedProperty().get(); drawTileset();});
	addSelectedBlockButton.setOnAction(e->{ addSelectionToBlockset(); });
	deleteBlockButton.setOnAction(e -> deleteBlock());
	saveBlocksButton.setOnAction(e -> saveBlocksetFile());
	loadBlocksButton.setOnAction(e -> loadBlocksetFile());
	
	blockset_selection.setOnAction(e->{loadBlockset(blockset_selection.getSelectionModel().getSelectedIndex());});
	blockset_selection.getSelectionModel().select(0);
	loadBlockset(0);
    }
    
    public Canvas getCanvas(){
	return canvas;
    }
    
    public void resizeCanvas(double w, double h){
	canvas.setWidth(w);
	canvas.setHeight(h);
    }
    
    
    public void drawTileset(){
	Tileset tiles = editor.getMap().tileset1;
	Palette bgpal = editor.getMap().pal;
	int tilecount = tiles.length();
	tileset_canvas.setWidth(16 * 8);
	tileset_canvas.setHeight(tilecount/16 * 8 + 8);
	tileset_renderer.drawTileset(tiles, bgpal, (byte)selectedPal, tileHflip, tileVFlip);
    }
    
    public void drawBlockset(){
	if(editor == null){return;}
	block_renderer.clearCanvas();
	block_canvas.setHeight(32 * (blockset.size() / 4 + 1));
	block_canvas.setWidth(32 * 4);
	block_renderer.drawBlockset(blockset, editor.getMap());
    }
    
    
    private void addSelectionToBlockset(){
	if(selectionBuf != null){    
	    blockset.add(selectionBuf);
	    drawBlockset();
	}
    }
    
    private void deleteBlock(){
	for(MapBlock b : blockset){
	    if(b == selectionBuf){
		blockset.remove(b);
		drawBlockset();
		return;
	    }
	}
    }
    
    private void loadBlocksetFile(){
	File f = FileDialog.openFile(stage, "Open blockset", "Blockset file", "*.blk");
	if(f != null && f.canRead()){
	    try{
		blockset = ConfigReader.readBlockset(f);
		drawBlockset();
	    }catch(Exception e){
		new Alert(Alert.AlertType.ERROR, "Failed to load blocks: " + e.getMessage()).showAndWait();
	    }
	}
    }
    
    private void saveBlocksetFile(){
	File f = FileDialog.saveAs(stage, "Save blockset", "Blockset file", "*.blk", "blocks.blk");
	if(f != null){
	    try{
		MapBlock.saveToFile(blockset, f);
	    }catch(Exception e){
		new Alert(Alert.AlertType.ERROR, "Failed to save blocks: " + e.getMessage()).showAndWait();
	    }
	}
    }
    
    private void placeBlock(int tx, int ty, MapBlock block){
	Map map = editor.getMap();
	if(tx < 0 || ty < 0 || tx >= map.wChunks*32 || ty >= map.hChunks*32){return;}	    
	editor.setBlockAt(tx, ty, block, dragging);
	map_renderer.drawMap(map, tx*8, ty*8, tx*8+block.width*8, ty*8+8+block.height*8);
    }
    
    
    
    
    private void onMapClicked(double x, double y, javafx.scene.input.MouseButton button){
	int pos[] = getTilePosAt((int)x, (int)y);
	int tx = pos[0];
	int ty = pos[1];
	Map map = editor.getMap();
	if(tx < 0 || ty < 0 || x >= map.wChunks*256 || y >= map.hChunks*256){
	    return;
	}
	
	switch(button){
	    case PRIMARY: //Left click: place block or tile
		if(selectionBuf != null){
		    placeBlock(tx, ty, selectionBuf);		
		}else{
		    editor.setTileAt(tx, ty, (short)selectedTile, (byte)selectedPal, tileHflip, tileVFlip, dragging);
		    map_renderer.drawMap(map, tx*8, ty*8, tx*8+8, ty*8+8);
		}
		break;
	    
	    case SECONDARY: //Right click: copy tile
		copyTileAt(tx, ty);
		break;
		
	    case MIDDLE: //Middle click: Flood fill
		editor.floodFillTile(tx, ty, Tilemap.getRawValue((short)selectedTile, (byte)selectedPal, tileHflip, tileVFlip));
		map_renderer.drawMap(editor.getMap());
		break;
	}
	
	onMapMouseMove(x, y); //This doesn't otherwise get called when dragging
    }
    
    public void onTilesetClicked(double x, double y, javafx.scene.input.MouseButton button){
	int tx = (int)x/8;
	int ty = (int)y/8;
	int tilenum = ty*16 + tx;
	if(tilenum < editor.getMap().tileset1.length()){
	    drawTileset();
	    tileset_renderer.outlineTilesetTile((short)tilenum);
	    selectedTile = tilenum;
	}
	
	selectionBuf = null; //delete copied block
	addSelectedBlockButton.setDisable(true);
    }
    
    public void onBlocksetClicked(double x, double y){
	int bx = (int)x/32;
	int by = (int)y/32;
	int block = by*4 + bx;
	if(block < blockset.size()){
	    drawBlockset();
	    block_renderer.outlineRect(bx*32, by*32, 32, 32);
	    selectionBuf = blockset.get(block);
	}
    }
    
    
    
    public void onMapMouseMove(double x, double y){
	int pos[] = getTilePosAt((int)x, (int)y);
	int tx = pos[0];
	int ty = pos[1];
	
	GraphicsContext ctx = canvas.getGraphicsContext2D();
	ctx.clearRect(screenCursorX - 4, screenCursorY - 4, screenCursorW + 8, screenCursorH + 8);

	if(copyDragging){	//if user is currently copying a block
	    screenCursorW = 8 + Math.abs(dragStartX - 8*tx);
	    screenCursorH = 8 + Math.abs(dragStartY - 8*ty);
	}else if(selectionBuf != null){	//if user has copied a block
	    screenCursorW = selectionBuf.width * 8;
	    screenCursorH = selectionBuf.height * 8;
	}else{  //tile
	    screenCursorW = 8;
	    screenCursorH = 8;
	}
	
	if(copyDragging){
	    screenCursorX = Math.min(dragStartX, tx*8);
	    screenCursorY = Math.min(dragStartY, ty*8);
	}else{
	    screenCursorX = tx*8;
	    screenCursorY = ty*8;
	}
	
	ctx.setStroke(Color.WHITE);
	ctx.setLineWidth(1);
	ctx.strokeRect(screenCursorX ,screenCursorY, screenCursorW,screenCursorH);
	bottomtext.setText(String.format("X: %d Y: %d Tile: 0x%04X", screenCursorX, screenCursorY, editor.getTileDataAt(tx, ty)));
	
    }
    
    public void setup(MapEditor editor_, Canvas mapcanvas, MapRenderer mapRenderer, Stage stage){
	this.stage = stage;
	this.mapcanvas = mapcanvas;
	map_renderer = mapRenderer;
	canvas.setWidth(mapcanvas.getWidth());
	canvas.setHeight(mapcanvas.getHeight());
	editor = editor_;
	canvasPane.getChildren().clear();
	canvasPane.getChildren().add(canvas);
	canvasPane.getChildren().add(mapcanvas);
	canvas.toFront();
	renderer.setCanvas(mapcanvas); //renderer draws to the mapcanvas, top layer is drawn to manually with graphicscontext
	
	drawTileset();
    }
    
    public void loadBlockset(int level){
	blockset = ConfigReader.readBlockset(String.format("blocks/%02X.txt", level));
	drawBlockset();
    }
    
    private int[] getTilePosAt(int x, int y){
	int tx = x/8;
	int ty = y/8;

	if(horizontalLock){
	    tx = lockX/8;
	}
	
	if(verticalLock){
	    ty = lockY/8;
	}
	
	//if placing a block while dragging, align position to block grid
	if(!copyDragging && dragging && selectionBuf != null){
	    if(tx >= dragStartX/8){
		tx -= (tx - dragStartX/8) % selectionBuf.width;
	    }else{
		tx -= (selectionBuf.width - ((dragStartX/8 - tx) % selectionBuf.width)) % selectionBuf.width;
	    }
	    
	    if(ty >= dragStartY/8){
		ty -= (ty - dragStartY/8) % selectionBuf.height;
	    }else{
		ty -= (selectionBuf.height - ((dragStartY/8 - ty) % selectionBuf.height)) % selectionBuf.height;
	    }
	    
	}
	
	int result[] = {tx, ty};
	return result;
    }
    
    
    private void copySelection(){
	if(screenCursorW <= 8 && screenCursorH <= 8){
	    copyTileAt(dragStartX/8, dragStartY/8);
	    addSelectedBlockButton.setDisable(true);
	}else{
	    int startX = Math.min(dragStartX/8, screenCursorX/8);
	    int startY = Math.min(dragStartY/8, screenCursorY/8);
	    selectionBuf = editor.copyMapBlock(startX, startY, startX+(screenCursorW)/8, startY+(screenCursorH)/8);
	    addSelectedBlockButton.setDisable(false);
	}
    }
    
    private void copyTileAt(int tx, int ty){
	int tiledata = editor.getTileDataAt((int)tx, (int)ty);
	tileHflip = (tiledata & (1 << 10)) != 0;
	tileVFlip = (tiledata & (1 << 11)) != 0;
	selectedPal = (tiledata >> 12) & 0xF;
	paletteBox.getSelectionModel().select(selectedPal);
	hFlipButton.selectedProperty().set(tileHflip);
	vFlipButton.selectedProperty().set(tileVFlip);
	drawTileset();
	selectedTile = tiledata & 0x3FF;
	tileset_renderer.outlineTilesetTile((short)selectedTile);
	selectionBuf = null;
    }
    
    
    
    private void printSelectedBlock(){
	if(!copyDragging)return;
	
	System.out.println(selectionBuf);
    }
    
    
    public void setHorizontalLock(boolean value){
	if(!horizontalLock && value){
	    lockX = screenCursorX;
	}
	horizontalLock = value;
    }
    
    public void setVerticalLock(boolean value){
	if(!verticalLock && value){
	    lockY = screenCursorY;
	}
	verticalLock = value;
    }
}
