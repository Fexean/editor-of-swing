
package editorofswing.undo;



import editorofswing.undo.UndoCmd;
import editorofswing.types.Map;
import editorofswing.types.MapBlock;
import editorofswing.util.MapEditor;

public class BlockPlacementCmd implements UndoCmd{
    
    private Map map;
    private int x, y;
    private MapBlock oldBlock, newBlock;
    private boolean merge;
    
    public BlockPlacementCmd(MapEditor editor, int tx, int ty, MapBlock block, boolean merge){
	map = editor.getMap();
	this.merge = merge;
	newBlock = block;
	oldBlock = editor.copyMapBlock(tx, ty, tx+block.width, ty+block.height);
	x = tx;
	y = ty;
    }
    
    private void placeBlock(MapBlock block){
	for(int j = y; j < y + block.height; j++){
	    for(int i = x; i < x + block.width; i++){
		if(i >= map.wChunks*32 || j >= map.hChunks*32){continue;}
		map.layer1.setRaw(i, j, block.tmap.getRaw(i-x, j-y));
		map.wallcol.setCollision(i, j, block.cmap.getCollision(i-x, j-y));
	    }
	}
    }
    
    @Override
    public void redo(){
	placeBlock(newBlock);
    }
    
    @Override
    public void undo(){
	placeBlock(oldBlock);
    }
    
    @Override
    public boolean shouldMerge(){
	return merge;
    }
}

