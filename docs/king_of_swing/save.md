# Save File
The game is saved on a 32 kilobyte SRAM, however only the first 0x3420 bytes are used.  
During gameplay, a copy of the save file is kept in ram at 0x203C000. The game only accesses this copy which is then copied to/from SRAM as one large block when the game is saved or loaded.


## Save file structure

|name|size|
|--------|----|
|Saved game 1|0xD00
|Saved game 2|0xD00
|Saved game 3|0xD00
|Saved game 4 (?)|0xD00
|game name ("KING OF SWING EU" in ascii)|0x10
|password 1 entered | 0x1
|password 2 entered | 0x1
|password 3 entered | 0x1
|unknown|0x7
|field_341A|0x1
|field_341B|0x1
|checksum|0x4

I'm unsure what the fourth saved game is supposed to be since the game only lets the player choose from 3 saves, maybe just unused data?  
The checksum is calculated by adding up all the bytes before it in the save file.  


## Default save file
If the SRAM doesn't contain a valid save file, the game instead loads the default save data from the rom starting at 0x86D27D8.  
For a save file to be valid the game name and checksum must be correct and field_341A must be equal to field_341B.  



## Saved game structure

Byte at offset 1 is diddy mode flag (1 or 0).  
Best times for each level start at offset 0x304 in the savedata, the ram location of the times is calculated with the function at 0x89F7E4C (args: r0=world, r1=level). It offsets the start of the best times with �0x240 * playerCharacter + 0x18 * (level + 4 * world)�.    

Health for save 1 is at 0xDF9 in the save file
Banana count at 0xDFA
0xDFC **might** be a total banana counter.
Level completion flags start at 0xD10? One for each levelID?
0xDC0 contains player's position on world map, two bytes, both store the levelID of selected level, on arrows the first byte is the current world*16 and the second is the world pointed by the arrow*16.
0xD08 is a 32-bit value counting the total playtime (adventure mode only? levels only?)
These would indicate that "saved game 1" is actually something different

## Game name in different versions

|Version|String|
|----------|-------|
|Europe|KING OF SWING EU|
|Japan|Bura Bura Donkey|
|USA|???|

