# Main loop
The game's main loop is at 0x80002E8.  
The pseudocode for it would be something like:  

    while(1){
    	wait_for_vblank()
    	update_io_registers_from_ram()
    	read_keypresses()
    	update_objects()
    	update_vertical_screenscroll()
    	update_horizontal_screenscroll()
    	run_sprite_engine()
    	clear_oam_buffer()
    	sub_86BEB6C()
    	if(*(s8*)0x3000001 < 0){
    		goto changeState;
    	}
    }


as you can see, if the byte at 0x3000001 is between 0x80 and 0xff, a state change will occur at the end of the loop.
When this happens, all objects are deleted and a state-specific function is called to create objects for the new gamestate.
After changing state, 0x3000001 is set to 0 and the main loop continues.  
The function pointers for each state-specific function are at 0x800038C.  
The new gamestate is chosen with the byte at 0x3000002, here are the different states:  

|0x3000002 Value|Description|
|-------|--------------|
|0x00| Initial state, shows the Nintendo logo
|0x01| Title screen
|0x02| "Select game mode" screen
|0x03| Loads a level to be played
|0x04| "Multi-Pak Play" screen
|0x05|"Extras" menu
|0x06| Same as 0x3
|0x07| Immediately changes to state 0x3
|0x08| Single player jungle jam "Select Event" screen
|0x09| World map screen
|0x0A| Game over screen
|0x0B| Does nothing
|0x0C| Save file select screen
|0x0D| Character selection screen
|0x0E| null pointer, crashes game
|0x0F| "Single-player jungle jam" menu
|0x10| Jungle jam results screen
|0x11| Jungle jam "Try again" screen
|0x12| Jungle jam high scores screen
|0x13| "Multiplayer jungle jam" menu
|0x14| Communication error screen (maybe does something in multiplayer)
|0x15| "Single-Pak Play" menu
|0x16| Time attack menu
|0x17| Secret password screen
|0x18| Status screen shown after credits
|0x19| World map pause menu
|0x1A| Multiplayer jungle jam "Return to select event" screen
|0x1B| Health & safety screen, unused in the EU version
|0x1C| Initial language selection when starting the game for the first time
|0x1D| Language menu
|> 0x1D|Invalid

