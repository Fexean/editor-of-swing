
package editorofswing.types;
import editorofswing.util.Rom;


public class ClimbCollision implements java.io.Serializable{
    private static final long serialVersionUID = 1L;
    
    public int x, y;
    public short w, h;
    public short flags;
    
    public ClimbCollision(Rom rom, int addr){
	read(rom, addr);
    }
    
    public ClimbCollision(int x, int y, short w, short h, short flags){
	this.x = x;
	this.y = y;
	this.w = w;
	this.h = h;
	this.flags = flags;
    }
    
    public void write(Rom rom, int addr){
	rom.write16(addr, (short)(x+w/2));
	rom.write16(addr+2, (short)(y+h/2));
	rom.write8(addr+4, (byte)(w/2));
	rom.write8(addr+5, (byte)(h/2));
	rom.write16(addr+6, flags);
    }
    
    public void read(Rom rom, int addr){
	x = rom.read16(addr);
	y = rom.read16(addr+2);
	w = rom.read8(addr+4);
	h = rom.read8(addr+5);
	flags = rom.read16(addr+6);
	
	//In ingame format the rect only covers the bottom right quarter of the actual climbing area
	//So I move and resize it on read & write
	x -= w;
	y -= h;
	w *= 2;
	h *= 2;
    }
    
    public void print(){
	System.out.println("x:"+x + " y:"+ y+ " w:"+ w + " h:" + h + " flags:" + flags);
    }
}
