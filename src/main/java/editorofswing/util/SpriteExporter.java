package editorofswing.util;

import editorofswing.types.sprites.AffineSprite;
import editorofswing.types.sprites.DrawableSprite;
import editorofswing.types.sprites.RegularSprite;
import editorofswing.types.Palette;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.File;

public class SpriteExporter {

    private Rom rom;
    private Palette pal;

    public SpriteExporter(Rom rom) {
        this.rom = rom;
        pal = Palette.monochromePal(16);
    }

    private BufferedImage getSpriteImage(RomConstants.SpriteType spriteType, int graphicId) throws Exception{
        DrawableSprite spr;
        switch (spriteType) {
            case REGULAR:
                spr = DrawableSprite.fromRegularSpr(rom, RegularSprite.fromGraphicId(rom, graphicId), pal.convertToARGB(), 0);
                break;
            case AFFINE:
                spr = DrawableSprite.fromAffineSpr(rom, AffineSprite.fromGraphicId(rom, graphicId), pal.convertToARGB(), 0);
                break;
            case PLAYER:
                spr = DrawableSprite.fromPlayerSpr(rom, AffineSprite.fromPlayerGraphicId(rom, graphicId), pal.convertToARGB(), 0);
                break;
            default:
                throw new Exception("Invalid spriteType");
        }

        return spr.asImage();
    }

    public boolean exportSpritePng(RomConstants.SpriteType spriteType, int graphicId, File file) {
        try {
            BufferedImage img = getSpriteImage(spriteType, graphicId);
            BufferedImage imgIndexed = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_BYTE_INDEXED, pal.getColorModel());
            for(int y = 0;y<img.getHeight();y++){
                for(int x = 0;x<img.getWidth();x++){
                    imgIndexed.setRGB(x, y, img.getRGB(x, y));
                }
            }
            
            ImageIO.write(imgIndexed, "png", file);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    public boolean exportSpritePng(RomConstants.SpriteType spriteType, int graphicId, String filepath) {
        return exportSpritePng(spriteType, graphicId, new File(filepath));
    }

}
