# Time Attack Mode

## World names
The world name strings are actually sprites. Graphic IDs for each world name are in an array at 0x89F8138.  

## Levels
The list of level IDs used in the time attack mode is located at 0x89F73DC. It's indexed like this: �4*world + level�.  
Level names are sprites, list of their graphic IDs is at 0x89F73B4.  

## High scores
Best times for each level start at offset 0x304 in the savedata, the ram location of the times is calculated with the function at 0x89F7E4C (args: r0=world, r1=level). It offsets the start of the best times with �0x240 * playerCharacter + 0x18 * (level + 4 * world)�.  
The default best times come from the "empty save file" which starts at 0x86D27D8.  
There are 4 times for each level. Each time is stored in a 4 byte integer as a number of frames (60fps).  


## Variables
The timer is stored as a 32-bit integer at 0x30004E0