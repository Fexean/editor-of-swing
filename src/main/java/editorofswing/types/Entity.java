
package editorofswing.types;
import editorofswing.util.Rom;

public class Entity implements java.io.Serializable{
    private static final long serialVersionUID = 1L;
    
    public int spriteTableId;
    public int callback;
    public int x, y; //u16
    public int spr_field80;
    
    public Entity(Rom rom, int addr){
	read(rom, addr);
    }
    
    public Entity(int id, int cb, int x, int y, int field80){
	spriteTableId = id;
	callback = cb;
	this.x = x;
	this.y = y;
	spr_field80 = field80;
    }
    
    public void read(Rom rom, int addr){
	spriteTableId = rom.read32(addr);
	callback = rom.read32(addr+4);
	x = rom.read16(addr+8);
	y = rom.read16(addr+10);
	spr_field80 = rom.read32(addr+12);
    }
    
    public void write(Rom rom, int addr){
	rom.write32(addr, spriteTableId);
	rom.write32(addr+4, callback);
	rom.write16(addr+8, (short)x);
	rom.write16(addr+10, (short)y);
	rom.write32(addr+12, spr_field80);
    }
    
    public String toString(){
	return "Entity: " + spriteTableId + ", " + String.format("0x%08X",callback) + ", ("+ x+", "+y+"), "+String.format("0x%08X",spr_field80);
    }
}
