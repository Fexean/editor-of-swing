
package editorofswing.ui.util;
import java.util.ArrayList;
import javafx.scene.control.*;
import java.util.HashMap;
import javafx.geometry.Side;
import javafx.event.*;


public class CategoryChoiceBox extends Button{
    ArrayList<Menu> categories = new ArrayList();
    HashMap<String, RadioMenuItem> optNameToOption = new HashMap();
    
    ToggleGroup togglegroup = new ToggleGroup();
    ContextMenu ctx = new ContextMenu();
    EventHandler<ActionEvent> onValueChanged;
    
    public CategoryChoiceBox(){
	setMinWidth(150);
	setOnAction(e -> {
		ctx.show(this, Side.BOTTOM, 0, 0);
	});

    }
    
    public void addCategory(String name){
	Menu category = new Menu(name);
	categories.add(category);
	ctx.getItems().add(category);
    }
    
    public void addCategories(String ... names){
	for(String x : names){
	    addCategory(x);
	}
    }
    
    public void addOption(String option, int category){
	if(category >= categories.size() || category < 0){
	    return;
	}
	
	RadioMenuItem opt = new RadioMenuItem(option);
	opt.setOnAction(e -> onOptionClicked(e));
	opt.setToggleGroup(togglegroup);
	optNameToOption.put(option, opt);
	categories.get(category).getItems().add(opt);
    }
    
    private void onOptionClicked(ActionEvent e){
	final String newopt = ((RadioMenuItem)e.getSource()).getText();
	setText(newopt);
	onValueChanged.handle(e);
    }
    
    public void select(String option){	
	optNameToOption.get(option).selectedProperty().set(true);
	setText(option);
    }
    
    public void clear(){
	optNameToOption.clear();
	categories.clear();
	ctx.getItems().clear();
    }
    
    public void setOnSelection(EventHandler<ActionEvent> listener){
	onValueChanged = listener;
    }
    
    public String getSelected(){
	return getText();
    }
    
    public void clearSelection(){
	setText("");
    }
}
