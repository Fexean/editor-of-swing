package editorofswing.cli;

import java.util.ArrayList;
import java.io.File;
import editorofswing.types.Map;
import editorofswing.util.BackgroundExporter;
import editorofswing.util.BackgroundImporter;
import editorofswing.util.Rom;
import editorofswing.util.RomMisc;
import editorofswing.util.LevelInserter;
import editorofswing.util.RomConstants;

public class MapInserterCli {
    
    Rom rom;
    int language = 0;
    boolean fullRepoint = true;
    boolean minimizeTilesets = false;
    ArrayList<Integer> levelIds = new ArrayList();
    ArrayList<String> mapnames = new ArrayList();
    
    final static String helpStrings[] = {
	    "USAGE: editorofswing insert <romfile> [options] [maps]",
	    "",
	    "Options:",
	    "-r\t\tdon't fully repoint the new levels",
	    "-m\t\tremove unused tiles to save space",
	    "-l=<value>\tset language, only for EU roms, defaults to 0 (english)",
	    "",
	    "Format for [maps]:",
	    "<level ID>=<map filename>",
	    "For example: 1A=mymapfile.map would replace level 1A with mymapfile.map",
	    "You can specify multiple maps in this format, separated by spaces."
    };
    
    public static void cliMain(String[] args){
	if (args.length < 3){
	    CliUtil.printHelp(helpStrings);
	    System.exit(0);
	}
	
	MapInserterCli cli = new MapInserterCli(args);
	CliUtil.exit("Map insertion complete");
    }
    
    
    public MapInserterCli(String[] args){
	rom = CliUtil.tryOpenRom(args[1]);
	int i = parseOptions(args, 2);
	parseLevels(args, i);
	insertMaps();
	
	CliUtil.trySaveRom(rom);
    }
    
    private void insertMaps(){
	int addr = RomConstants.freespace;

	for(int i = 0; i<levelIds.size();i++){
	    try{
		File f = new File(mapnames.get(i));
		if(!f.canRead()){
		    CliUtil.exit("Unable to read file: "+mapnames.get(i));
		}
		Map map = Map.loadFromFile(f);
		
		if(minimizeTilesets){
		    System.out.println("Minimizing tileset...");
		    minimizeMapTilesets(map);
		}
		
		addr = RomMisc.align4(RomMisc.findFreeSpace(rom, addr, 3 + LevelInserter.getFreespaceNeeded(rom, map, levelIds.get(0), language, fullRepoint)));
		if(addr == -1){
		    CliUtil.exit("Unable to find freespace");
		}

		LevelInserter.insertMap(rom, map, levelIds.get(i), addr, fullRepoint, language);
		System.out.println(String.format("Inserted %s as level %02X at %06X" , mapnames.get(i),levelIds.get(i), addr));
	    }catch(Exception e){
		CliUtil.exit("Exception:"+e.getMessage());
	    }
	}
    }
    
    private int parseOptions(String[] args, int start){
	int i = start;
	for(;i<args.length && args[i].startsWith("-");i++){
	    if(args[i].equals("-r")){
		fullRepoint = false;
	    }else if(args[i].equals("-m")){
		minimizeTilesets = true;
	    }else if (args[i].startsWith("-l=")){
		try{
		    language = Integer.parseInt(args[i].substring(1 + args[i].indexOf("=")), 16);
		}catch(Exception e){
		    CliUtil.exit("Language must be an integer:" + args[i]);
		}
	    }else{
		CliUtil.exit("Invalid option:" + args[i]);
	    }
	}
	return i;
    }
    
    private int parseLevels(String[] args, int start){
	int i = start;
	for(;i<args.length;i++){
	    if(args[i].contains("=")){
		mapnames.add(args[i].substring(1 + args[i].indexOf("=")));
		try{
		    levelIds.add(Integer.parseInt(args[i].substring(0, args[i].indexOf("=")), 16));
		}catch(Exception e){
		    CliUtil.exit("Invalid levelId:" + args[i].substring(0, args[i].indexOf("=")) + " in arg:" + args[i]);
		}
	    }else{
		CliUtil.exit("Invalid map argument:" + args[i]);
	    }
	}
	if(levelIds.isEmpty()){
	    CliUtil.exit("No levels specified");
	}
	return i;
    }
    
    
    private void minimizeMapTilesets(Map map){
	try{
	    File bg1 =  File.createTempFile("bg1_tmp", ".png");
	    File bg2 =  File.createTempFile("bg2_tmp", ".png");
	    File bg3 =  File.createTempFile("bg3_tmp", ".png");
	    
	    BackgroundExporter.exportTilemap(bg1, map.layer1, map.pal, map.tileset1);
	    BackgroundExporter.exportTilemap(bg2, map.layer2, map.pal, map.tileset2);
	    BackgroundExporter.exportTilemap(bg3, map.layer3, map.pal, map.tileset2);
	    
	    BackgroundImporter.importBackground1(bg1, map, false);
	    BackgroundImporter.importSecondaryBackgrounds(bg2, bg3, map, false, false);
	    
	    bg1.delete();
	    bg2.delete();
	    bg3.delete();
	}catch(Exception e){
	    e.printStackTrace();
	    CliUtil.exit("Failed to minimize tileset");
	}
    }

}
