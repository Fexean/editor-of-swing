
package editorofswing.cli;
import editorofswing.util.LevelDeleter;
import editorofswing.util.*;

public class LevelDeleterCli{
    
    final static String helpStrings[] = {
	"USAGE: editorofswing delete-maps <romfile>",
    };
    
    public static void cliMain(String[] args){
	if (args.length < 2){
	    CliUtil.printHelp(helpStrings);
	    System.exit(0);
	}
	
	Rom rom = CliUtil.tryOpenRom(args[1]);
	LevelDeleter.deleteAllLevels(rom);
	CliUtil.trySaveRom(rom);
	CliUtil.exit("Level deletion complete");
    }

}
