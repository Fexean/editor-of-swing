# Backgrounds

Many of the game's backgrounds are loaded using the function at 0x841C8B0. This function loads the tiles and tilemap of a specific 256-color background to vram (palette must be loaded separately).
All the backgrounds share the same tileset which is uncompressed and located at 0x83F8D1C. The function takes 2 arguments: R0 contains a pointer to a tilemap (format described below) and R1 contains the screen base block (tilemap is written to 0x6000000 + 0x800*R1).  


## Tilemap format

|Type |Name|
|:----------|------------:|
|u8|width (in tiles)
|u8|height (in tiles)
|u16[width*height]|tilemap data

In the tilemap data the lowest 15 bits are used to select a tile from the shared tileset and the top bit is used to signify horizontal flip. Vertical flipping is not used.  



## Background Addresses

|Name|Tilemap address|Palette address
|:----------|------------|------------|
|Paon logo|0x840DF7C|0x840B59C
|Nintendo logo|0x840DACA|0x840B39C
|Health & safety|0x840BE9E|0x840A79C
|Title screen|0x840E42E|0x840B79C
|Initial language selection|0x840C350|0x840A99C
|Menu|0x840CCB4|0x840AD9C
|Game over| 0x840B99C|0x840A59C
|Time trial|0x840D618|0x840B19C
|Worldmap pause screen|0x840D166|0x840AF9C
|Language selection|0x840C802|0x840AB9C

