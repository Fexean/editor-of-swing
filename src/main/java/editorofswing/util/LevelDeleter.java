
package editorofswing.util;
import editorofswing.types.*;
import editorofswing.util.Rom;
import editorofswing.util.RomConstants;
import java.util.HashMap;

public class LevelDeleter {
    
    public static void deleteAllLevels(Rom rom){
	HashMap<String, Integer> levelNames = ConfigReader.readHexSymbols("levelnames.cfg");
	
	//delete climb collisions before touching climbmaps
	for(int level : levelNames.values()){
	    deleteLevelEntitiesClimbCol(rom, level);
	}
	
	if(rom.getVersion() == Rom.Version.EU){
	    deleteBgStructs(rom);
	}
	
	for(int level : levelNames.values()){
	    deleteLevelData(rom, level);
	}
	
	for(int level : levelNames.values()){
	    int borderPtr = rom.read32(RomConstants.getListValue(RomConstants.Table.LEVEL_BORDER, level));
	    rom.fill(borderPtr, 8, (byte)0);
	}
    }
    
    
    public static void deleteBgStructs(Rom rom){
	if(rom.getVersion() != Rom.Version.EU){
	    throw new IllegalArgumentException("Rom must be EU version, not " + rom.getVersion());
	}
	
	HashMap<String, Integer> levelNames = ConfigReader.readHexSymbols("levelnames.cfg");
	for(int lang = 1;lang <= 4; lang++){
	    for(int level : levelNames.values())
		deleteBgStruct(rom, level, lang, 0);
	}
    }
    
    
    private static void deleteBgStruct(Rom rom, int levelId, int language, int defaultLanguage){
	int borderPtr = rom.read32(RomConstants.getListValue(RomConstants.Table.LEVEL_BORDER, levelId));
	int w = 1 + (240 + rom.read16(borderPtr+6) - 1)/256;
	int h = 1 + (160 + rom.read16(borderPtr+2) - 1)/256;
	
	int bgstruct = RomConstants.getLevelBgStruct(rom, levelId, language);
	int defaultBgStruct = RomConstants.getLevelBgStruct(rom, levelId, defaultLanguage);
	
	//avoid clearing the tileset used by the tutorial stages for all languages
	if(rom.read32(bgstruct) != rom.read32(defaultBgStruct) && rom.read32(bgstruct) != 0x833E7C4){
	    clearTileset(rom, rom.read32(bgstruct));
	    rom.write32(bgstruct, rom.read32(defaultBgStruct));
	}
	
	if(rom.read32(bgstruct+4) != rom.read32(defaultBgStruct+4)){
	    clearTileset(rom, rom.read32(bgstruct+4));
	    rom.write32(bgstruct+4, rom.read32(defaultBgStruct+4));
	}
	
	if(rom.read32(bgstruct+8) != rom.read32(defaultBgStruct+8)){
	   clearChunkMap(rom, rom.read32(bgstruct+8), w, h, 0x800);
	   rom.write32(bgstruct+8, rom.read32(defaultBgStruct+8));
	}
	
	if(rom.read32(bgstruct+12) != rom.read32(defaultBgStruct+12)){
	    clearChunkMap(rom, rom.read32(bgstruct+12), w, h, 0x800);
	    rom.write32(bgstruct+12, rom.read32(defaultBgStruct+12));
	}
	
	if(rom.read32(bgstruct+16) != rom.read32(defaultBgStruct+16)){
	    clearChunkMap(rom, rom.read32(bgstruct+16), w, h, 0x800);
	    rom.write32(bgstruct+16, rom.read32(defaultBgStruct+16));
	}
    }
    
    	
    private static void deleteLevelEntitiesClimbCol(Rom rom, int levelId){
	Map map = new Map(rom, levelId, 0);
	clearData(rom, rom.read32(RomConstants.getListValue(RomConstants.Table.ENTITIES, levelId)), map.entities.size()*0x10);
	clearData(rom, rom.read32(RomConstants.getListValue(RomConstants.Table.CLIMB_COLLISION, levelId)), map.climbCol.size()*8);
    }
	
    
    private static void deleteLevelData(Rom rom, int levelId){
	final int nullchunkmap = RomConstants.nullchunk + 0x800;  //at the end of nullchunk there is a chunkmap with nothing but nullchunks
	int borderPtr = rom.read32(RomConstants.getListValue(RomConstants.Table.LEVEL_BORDER, levelId));
	int w = 1 + (240 + rom.read16(borderPtr+6) - 1)/256;
	int h = 1 + (160 + rom.read16(borderPtr+2) - 1)/256;
	
	int bgstruct = RomConstants.getLevelBgStruct(rom, levelId, 0);
	
	clearTileset(rom, rom.read32(bgstruct));
	rom.write32(bgstruct, RomConstants.nullchunk);
	
	clearTileset(rom, rom.read32(bgstruct+4));
	rom.write32(bgstruct+4, RomConstants.nullchunk);

	for(int i = 0;i<3;i++){
	    clearChunkMap(rom, rom.read32(bgstruct+8 + i*4), w, h, 0x800);
	    rom.write32(bgstruct+8 + i*4, nullchunkmap);
	}
	
	clearData(rom, rom.read32(RomConstants.getListValue(RomConstants.Table.LEVEL_BG_PAL, levelId)), 2*14*16);
	clearData(rom, rom.read32(RomConstants.getListValue(RomConstants.Table.LEVEL_SPR_PAL, levelId)), 2*256);
	
	clearChunkMap(rom, rom.read32(RomConstants.getListValue(RomConstants.Table.WALL_COLLISION, levelId)), Math.max(4, w), h, 0x400);
	rom.write32(RomConstants.getListValue(RomConstants.Table.WALL_COLLISION, levelId), nullchunkmap);
	
	clearChunkMap(rom, rom.read32(RomConstants.getListValue(RomConstants.Table.CLIMB_MAP, levelId)), Math.max(4, w), h, 0x400);
	rom.write32(RomConstants.getListValue(RomConstants.Table.CLIMB_MAP, levelId), nullchunkmap);
    }

    
    private static void clearData(Rom rom, int address, int size){
	rom.fill(address, size, (byte)0xFF);
    }
    
    
    private static void clearChunkMap(Rom rom, int mapAddr, int w, int h, int chunkSize){
	for(int x = 0;x<w;x++){
	    for(int y = 0;y<h;y++){
		int chunkAddr = rom.read32(mapAddr + 4*(x + 4*y));
		if(chunkAddr > RomConstants.nullchunk+32*32*2 && Rom.isRomPointer(chunkAddr)){
		    clearData(rom, chunkAddr, chunkSize);
		}
	    }
	}
	
	if(mapAddr != RomConstants.nullchunk + 0x800)	//avoid clearing the chunkmap made from nullchunks
	    clearData(rom, mapAddr, 4*h*4);
    }
    
    
    private static void clearTileset(Rom rom, int address){
	int tilecount = rom.read32(address);
	//if not already cleared
	if(tilecount != -1 && tilecount != 0){ 
	    clearData(rom, address, 4 + tilecount * 0x20);
	}
    }

}
