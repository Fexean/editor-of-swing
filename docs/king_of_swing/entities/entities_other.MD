## Player
DK will spawn here/next to this entity depending on the parameter value.  
This entity only works if it's the first object in table 1.  

**Parameter field**  

|size|name|info|
|----|------------:|:-----------:|
|u16|param| used in different ways depending on byte2 & byte3 ?
|u8|direction| depends on state, see table below
|u8|state| see table below

|**state** value|state description|**direction** usage|**param** usage|
|----|-------------|-------------|-------------|
|0| player starts on the side of the entity, and walks to the entity's X-position before giving player control. If this entity is in the air, the player will fall before walking. | byte2 controls the starting direction: 0=right, 1=left| unused?
|1| player starts standing still| controls facing direction: 0=right, 1=left| unused?
|2| player starts in a falling state| controls facing direction: 0=right, 1=left, 2=up, 3=down | unused?
|3| player starts in attacking state| controls attack direction: 0=right, 1=left, 2=up, 3=down | controls the length of the attack
|other| seems to just softlock the game by forcing the player to walk left forever | ??? | ???

### Parameter Examples:

|value|description|
|----|------------:|
|0x00000000|player starts from the left side and walks right (most common)
|0x00010000|player starts from the right and walks left
|0x03022000|player starts attacking upwards, the bottom 16 bits (0x2000) controls the attack length. (used in twister 2)

## LevelDataLoader
This loads the level tilesets and initializes the camera.  
Its position sets the initial camera position. The background tilemap will look glitchy if this is positioned incorrectly relative to the background. Usually its X-coordinate is 4 and Y-coordinate is (level_height - 156).

## Invis Trigger
This entity is an invisible 24x24 area that triggers entities with the same trigger_group when the player collides with this entity.  
Can be activated only once.  
There are also unused 24x48(0x89C22DC) and 48x24(0x89C22F0) versions of this entity.  

**Parameter field**  

|size|name|info|
|----|------------:|:-----------:|
|u8 |trigger_group|
|u8[3]| unused

## Warp Right
Warps the player if they are holding right or R within this entity's radius.
This entity is 1 pixel wide.

**Parameter field**  

|size|name|info|
|----|------------:|:-----------:|
|u8 |destination|level id
|u8|unused
|s16|height


## Warp
Warps the player if they collide with this 8x8 pixel entity.

**Parameter**  

|size|name|info|
|----|------------:|:-----------:|
|u8|destination|level id
|u8[3]|unused

## Level End
Ends the level if player holds right or R withing this entity's radius.

**Parameter field**  

|size|name|info|
|----|------------:|:-----------:|
|u8|destination|level id
|u8|width
|s16|height


## Bonus Warp
Warps the player to destination if they collide with this entity's hitbox (4x4 pixels).  
Used for warping the player to the bonus stage.  

**Parameter**  

|size|name|info|
|----|------------:|:-----------:|
|u8|destination|level id
|u8[3]|unused

## Bonus Return Spot
This is the spot at which the player will spawn when they return from a bonus stage.  
Its code runs once and it sets the player's position to its position if the byte at 0x30002B2 is non-zero (=if player returned from bonus).  
If this entity is not placed in a level, the player starts from the level starting spot when returning from a bonus.

**Parameter**  

|size|name|info|
|----|------------:|:-----------:|
|u32|player_param|this is copied to the player objects parameter field when the player is warped here


## Bonus Return Camera
Runs code once, scrolls the camera to its position if 0x30002B2 is non-zero (=if player returned from bonus).
The difference in X and Y coordinates between this entity and the LevelDataLoader must be a multiple of 8. Otherwise the game crashes when returning from a bonus.


## Bonus Return Warp
Warps the player to destination and sets 0x30002B2 to 1.  
The player must hold right or R to warp.  

**Parameter**  

|size|name|info|
|----|------------:|:-----------:|
|u8|destination|level id
|u8|width
|s16|height



## Bonus Timer
This entity creates the timer for bonus stages. It also plays the bonus music.  
The timer can't display values larger than 99 correctly.  
**Parameter**  

|size|name|info|
|----|------------:|:-----------:|
|u8|destination|level id that is warped to if the time runs out
|u8|unknown/unused
|s16|seconds


## Bonus Fall-off Return
Warps the player to destination and sets 0x30002B2 to 1.  
Used in Risky Reef Bonus to prevent the map from resetting when the player falls off.  

**Parameter**  

|size|name|info|
|----|------------:|:-----------:|
|u8|destination|level id
|u8|width
|s16|height



## Bonus Barrel
This works exactly like a Barrel Cannon.  
Uses palette 11.  
**Parameter**  

|size|name|info|
|----|------------:|:-----------:|
|u8 |unknown/unused
|bool8|manual_launch| if zero the cannon automatically launches the player, if non-zero the player has to press L+R to launch
|ang8|rotation
|u8|power|how far the player is launched


## Rotation Center
Works with: (Rotating)-Pegs and spikes  
**Parameter**  

|size|name|info|
|----|------------:|:-----------:|
|u8 |rotation_group|rotating entities with the same group will rotate around this
|u8|stop| if non-zero, the rotation will periodically stop for *stop* amount of time and continue for *start* amount of time.
|u8|start|
|u8|speed|the rotating entity's speed is also considered



## Tornado Wind
Sets the strength of the tornady wind by writing -strength to 0x30002B8.  
For the wind to actually work the second bit of the byte at 0x30002B0 must be set, this is done by the FX Tornado Background entity.   

**Parameter**  

|size|name|info|
|----|------------:|:-----------:|
|s32|strength|strenght of the wind, negative values move the player to the right while positive values move the player left.


## Ice Physics
This entity's code runs once and writes 8 to 0x30002B0, which makes the player slide around on the ground.


## Camera Setter
This entity scrolls the camera to its position when the level loads.
You can use this to set the starting camera somewhere other than LevelDataLoader's position.
The difference in X and Y coordinates between this entity and the LevelDataLoader must be a multiple of 8. Otherwise the game crashes when the map is loaded.



## Climb Area
This entity is an invisible climbable area.
It's used in Sassy Squatch's Lair to cover up the background pegs so that their grab priority is higher than the bombs'.

**Parameter**  

|size|name|info|
|----|------------:|:-----------:|
|u8|half_height| the climbable area is twice as high as this
|u8|half_width| the climbable area is twice as wide as this
|u16|unused|