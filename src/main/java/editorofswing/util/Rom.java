package editorofswing.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Arrays;

//java.nio.ByteBuffer

public class Rom {
    public enum Version {UNKNOWN, EU, US, JP, IQUE, KIOSK_DEMO};
    
    private String filename;
    private boolean useExternalResources = false;
    private String externalResPath;
    private byte buffer[];
    private Version version;
    private int maxsize;
    
	public Rom(String filename) throws Exception{
	    open(filename);
	    this.filename = filename;
	}
	
	public String getPath(){
	    return filename;
	}
	
	public int size(){
	    return buffer.length;
	}
        
        public int sizeUsed(){
            return maxsize;
        }
	
	public byte read8(int address){
	    address &= 0x1FFFFFF;
	    if(address >= buffer.length){
		throw new IllegalArgumentException("Rom address out of bounds."+String.format("0x%08X",address));
	    }
	    return buffer[address];
	}
	
	
	
	public short read16(int address){
	    address &= 0x1FFFFFF;
	    if(address >= buffer.length-1){
		throw new IllegalArgumentException("Rom address out of bounds."+String.format("0x%08X",address));
	    }
	    return (short)((buffer[address] & 0xFF)|(buffer[address+1]<<8));
	}
	
	
	
	public int read32(int address){
	    address &= 0x1FFFFFF;
	    if(address >= buffer.length-3){
		throw new IllegalArgumentException("Rom address out of bounds."+String.format("0x%08X",address));
	    }
                
	    int value = (buffer[address]&0xFF);
            value |= ((buffer[address+1]&0xFF)<<8);
            value |= ((buffer[address+2]&0xFF)<<16);
            value |= ((buffer[address+3]&0xFF)<<24);
            return value;
	}
	
	
	public byte[] read(int address, int bytes){
	    address &= 0x1FFFFFF;
	    if(address > buffer.length-bytes){
		throw new IllegalArgumentException("Rom address out of bounds."+String.format("0x%08X %02X",address, bytes));
	    }
	    byte[] data = new byte[bytes];
	    System.arraycopy(buffer, address, data, 0, bytes);
	    return data;
	}
	
	
	public void write8(int address, byte data){
	    address &= 0x1FFFFFF;
	    writeNotice(address, 1);
	    if(address == 0){throw new IllegalArgumentException("WRITE TO 0 =>"+String.format("0x%08X",data));}
	    if(address >= buffer.length){
		throw new IllegalArgumentException("Rom address out of bounds."+String.format("0x%08X",address));
	    }
	    buffer[address] = data;
	}
	
	
	public void write16(int address, short data){
	    address &= 0x1FFFFFF;
	    if(address >= buffer.length-1){
	    	throw new IllegalArgumentException("Rom address out of bounds."+String.format("0x%08X",address));
	    }
	    writeNotice(address, 2);
	    buffer[address+1] = (byte)(data>>8);
	    buffer[address] = (byte)(data & 0xFF);
	}
	
	
	public void write32(int address, int data){
	    address &= 0x1FFFFFF;
	    if(address >= buffer.length-3){
		throw new IllegalArgumentException("Rom address out of bounds."+String.format("0x%08X",address));
	    }
	    writeNotice(address, 4);
	    buffer[address+3] = (byte)((data >> 24) & 0xFF);
	    buffer[address+2] = (byte)((data >> 16) & 0xFF);
	    buffer[address+1] = (byte)((data >> 8) & 0xFF);
	    buffer[address] = (byte)(data & 0xFF);
	}
	

	
	public void fill(int address, int bytes, byte data){
	    address &= 0x1FFFFFF;
	    writeNotice(address, bytes);
	    Arrays.fill(buffer, address, address+bytes, data);
	}
	
	
	public void write(byte[] data, int address){
            address &= 0x1FFFFFF;
	    if(address >= buffer.length-data.length){
	    	throw new IllegalArgumentException("Rom address out of bounds."+String.format("0x%08X",address));
	    }
	    writeNotice(address, data.length);
	    for(int i = 0;i<data.length;i++){
		buffer[address+i] = data[i];
	    }
	}
	
	
	private void writeNotice(int addr, int len){
	    if(addr > maxsize){
		maxsize = addr + len;
	    }
	    int nullchunk = RomConstants.nullchunk & 0xFFFFFF;
	    if(addr >= nullchunk && addr < nullchunk + 32*32*2){
		new Exception(String.format("[writeNotice] %04x",addr)).printStackTrace();;
	    }

	}
	
	
	
	
	public void open(String filepath) throws Exception{
	    try{
		File file = new File(filepath);
                buffer = new byte[32*1024*1024];
		if(file.canRead()){
		    FileInputStream ifstream = new FileInputStream(file);
		    maxsize = ifstream.read(buffer);
		    ifstream.close();
		}else{
		    throw new IllegalArgumentException("Unable to read file.");
		}	
	    }catch(Exception e){
		throw new IllegalArgumentException("Unable to open file.");
	    }
	    
	    version = readVersion();
	}
	
	
    public void save(String filename) throws Exception{
	File file = new File(filename);
	if(file.canWrite()){
	    try(FileOutputStream ofstream = new FileOutputStream(file)){
                ofstream.write(buffer, 0, maxsize);
                ofstream.close();
            }catch(Exception e){
                throw new IllegalStateException("Unable to save to file");
            }		
	}else{
	    throw new IllegalStateException("Unable to write to file");
	}	
    }
	
    public void save() throws Exception{
        save(filename);
    }
    
    private Version readVersion(){
	int sig = read32(0xAC);
	switch(sig){
	    case 0x504B4242: //BBKP
		return Version.EU;
	    case 0x45475742: //BWGE
		return Version.KIOSK_DEMO;
	    case 0x434B4242: //BBKC
		return Version.IQUE;
	    case 0x454B4242: //BBKE
		return Version.US;
	    case 0x6A4B4242: //BBKJ
		return Version.JP;
	}
	return Version.UNKNOWN;
    }
    
    public Version getVersion(){
	return version;
    }
    
    public boolean isVersionSupported(){
	return version == Version.EU || version == Version.US;
    }
    
    public static boolean isRomPointer(int ptr){
	return ptr >> 24 == 8 || ptr >> 24 == 9;
    }
}
