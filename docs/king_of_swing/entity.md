# Entity
The term entity is used to describe objects placed in levels.

## Structure
The level's objects are created from the following structure:

|type|name|info|
|------|--------|-----------------------------------|
|u32|tableId| determines which array the object is placed in
|u32|callback| object's callback, called every frame
|s16|x|
|s16|y|
|u32|parameter| written to [object+0x80], the callback can use it in various ways

As you can see the entity structure doesn't contain any kind of id that would determine the entity's graphic or behavior, all of that is set in the callback function: the only difference between a banana and a tire is the callback pointer.  
The list of these structures is terminated with a negative 32-bit value, usually 0xFFFFFFFF.  

### tableId values
The tableId is used to index an array at 0x3000340 to get the object array pointer (0x3000340 + 8*tableId).  
the pointers are always initialized to the following values in function level_load_data.

|tableId|object array|
|-------|---------:|
|0|0x2012200
|1|0x2022000
|2|0x2014000
|3|0x2027800

Because the different object arrays are updated differently, tableId actually affects the behavior of an entity.  




## Different entity types
These files contain information about different kinds of entities, mostly focusing on their parameters. The parameters are presented as structures starting from the **least-significant** byte.  
[Pegs](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/king_of_swing/entities/entities_pegs.MD)  
[Enemies](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/king_of_swing/entities/entities_enemies.MD)  
[Items](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/king_of_swing/entities/entities_items.MD)  
[Levers](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/king_of_swing/entities/entities_levers.MD)  
[Visual effects](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/king_of_swing/entities/entities_fx.MD)  
[Other](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/king_of_swing/entities/entities_other.MD)  
[Unused](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/king_of_swing/entities/entities_unused.md)  
