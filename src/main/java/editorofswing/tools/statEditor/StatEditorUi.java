
package editorofswing.tools.statEditor;

import editorofswing.util.Rom;
import editorofswing.util.RomConstants;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.geometry.Insets;

public class StatEditorUi {
    Scene scene;
    Rom rom;
    PlayerStatTable playerStats[];
    
    final int STAT_COUNT = 26;
    
    final String statNames[] = {
	"Swing speed", "Swing speed (Fast)", "Swing speed (Slow)",
	"Swing jump speed", "Swing jump speed (Fast)", "Swing jump speed (Slow)",
	"Gravity", "Max falling speed", "Attack charge time", "Attack duration",
	"Attack speed", "Attack speed (Fast)", "Attack speed (Slow)",
	"Attack knockback", "Walking acceleration", "Ground jump speed", "Max walking speed",
	"Throw speed", "Charge anim start", "Flight barrel speed", "Flight barrel turning",
	"Swing radius 1", "Width (?)", "Height (?)", "Swing radius 2", "Unknown"
    };
    
    final String characterNames[] = {
	"Donkey Kong", "Diddy Kong", "Dixie Kong", "Funky Kong", "King K. Rool", "Kremling", "Wrinkly Kong", "Bubbles", "Kremling (Enemy)"
    };
    
    TextField normalFields[] = {};
    TextField specialFields[] = {};
    
    ComboBox characterSelect = new ComboBox();
    Button saveButton = new Button("Save changes to ROM");
    
    
    public StatEditorUi(Rom rom){
	this.rom = rom;

	playerStats = new PlayerStatTable[9];
	for(int i = 0; i<9;i++){
	    playerStats[i] = new PlayerStatTable(rom, RomConstants.getCharacterStatTable(i));
	}
	
	setupUi();
    }
    
    private void onSaveClicked(){
	saveFieldsToStats(characterSelect.getSelectionModel().getSelectedIndex());
	for(int i = 0; i<9;i++){
	    playerStats[i].saveStats(rom, RomConstants.getCharacterStatTable(i));
	}
	try{
	    rom.save();
	}catch(Exception e){
	    new Alert(Alert.AlertType.ERROR, "Failed to save ROM: " + e.getMessage()).showAndWait();
	}
    }
    
    
    
    private void loadStatsToFields(){
	int character = characterSelect.getSelectionModel().getSelectedIndex();
	PlayerStatTable stats = playerStats[character];
	
	for(int i = 0;i<STAT_COUNT;i++){
	    normalFields[i].setText(""+stats.stats[i].normalValue);
	    specialFields[i].setText(""+stats.stats[i].specialValue);
	}
    }
    
    private void saveFieldsToStats(int characterId){
	PlayerStatTable stats = playerStats[characterId];
	
	for(int i = 0;i<STAT_COUNT;i++){
	    try {
		int a = Integer.parseInt(normalFields[i].getText());
		int b = Integer.parseInt(specialFields[i].getText());
	        stats.stats[i].normalValue = a;
		stats.stats[i].specialValue = b;
	    } catch(Exception e){
		new Alert(Alert.AlertType.WARNING, statNames[i] + " has a non-integer value, not saving.").showAndWait();
	    }
	}
    }
    
    private void setupUi(){
	VBox root = new VBox();
	root.setAlignment(Pos.CENTER);
	root.setSpacing(5);
	root.setPadding(new Insets(8, 0, 8, 0)); //t r b l
	
	GridPane statGrid = new GridPane();
	statGrid.setPadding(new Insets(0, 0, 0, 10));
	
	statGrid.setHgap(8);
	statGrid.setVgap(4);
	
	statGrid.add(new Label("Normal"), 1, 0);
	statGrid.add(new Label("Gone bananas"), 2, 0);
	
	normalFields = new TextField[STAT_COUNT];
	specialFields = new TextField[STAT_COUNT];
	
	for(int i = 0;i<STAT_COUNT;i++){
	    statGrid.add(new Label(statNames[i]), 0, i+1);
	    
	    normalFields[i] = new TextField();
	    specialFields[i] = new TextField();
	    
	    normalFields[i].setPrefWidth(90);
	    specialFields[i].setPrefWidth(90);
	    
	    statGrid.add(normalFields[i], 1, i+1);
	    statGrid.add(specialFields[i], 2, i+1);
	}
	
	root.getChildren().add(new ScrollPane(statGrid));
	
	HBox charBox = new HBox();
	charBox.setAlignment(Pos.CENTER);
	charBox.getChildren().addAll(new Label("Character: "), characterSelect);

	root.getChildren().add(charBox);
	root.getChildren().add(saveButton);
	
	saveButton.setOnAction(e -> onSaveClicked());
	characterSelect.getItems().addAll(characterNames);
	characterSelect.getSelectionModel().selectFirst();
	characterSelect.getSelectionModel().selectedIndexProperty().addListener((a, oldValue, newValue)-> {
	    saveFieldsToStats(oldValue.intValue());
	    loadStatsToFields();
	});
	loadStatsToFields();
	
	scene = new Scene(root, 400, 360);
    }
    
    public static void openTool(Rom rom){
	StatEditorUi ui = new StatEditorUi(rom);
	ui.showAndWait();
    }
    
    public void showAndWait(){
	Stage stage = new Stage();
	stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("Character Stat Editor");
	stage.setScene(scene);
        stage.showAndWait();
    }
}
