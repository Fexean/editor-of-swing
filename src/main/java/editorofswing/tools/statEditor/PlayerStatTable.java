
package editorofswing.tools.statEditor;
import editorofswing.util.Rom;

public class PlayerStatTable {
    public PlayerStat stats[];
    
    public PlayerStatTable(Rom rom, int statTable){
	stats = new PlayerStat[26];
	loadStats(rom, statTable);
    }
    
    public void loadStats(Rom rom, int statTable){
	for(int i = 0;i<26;i++){
	    stats[i] = new PlayerStat();
	    stats[i].read(rom, rom.read32(statTable + 4*i));
	}
    }
    
    public void saveStats(Rom rom, int statTable){
	for(int i = 0;i<26;i++){
	    stats[i].write(rom, rom.read32(statTable + 4*i));
	}
    }
}
