

package editorofswing.util;
import editorofswing.types.*;


public class LevelInserter {
    
    
    public static int insertMap(Rom rom, Map map, int level_to_replace, int freespace, boolean repointEverything, int language){
	freespace |= 0x8000000;
	int bytesUsed = updateMapPointers(rom, map, level_to_replace, freespace, language, repointEverything);
	insertMapData(rom, map, level_to_replace);
	if(bytesUsed > 0){
	    rom.write8(freespace + bytesUsed, (byte)0x12);  //mark end of free space
	}
	return bytesUsed;
    }
    

    
    public static int updateMapPointers(Rom rom, Map map, int level_to_replace, int freespace, int lang, boolean repointEverything){
	int ogf = freespace;
	Map oldmap = new Map(rom, level_to_replace, lang);
	final int bgstruct = RomConstants.getLevelBgStruct(rom, level_to_replace, 0);
	
	//repoint chunk pointers and chunks if map is resized
	if(repointEverything || oldmap.hChunks < map.hChunks || (map.wChunks > 4 && oldmap.wChunks < map.wChunks)){
	    System.out.println(String.format("repointed all chunks: %06X", freespace));
	    freespace += writeChunkStructure(rom, RomConstants.getListValue(RomConstants.Table.WALL_COLLISION, level_to_replace), freespace, map.wallcol);
	    freespace += writeChunkStructure(rom, RomConstants.getListValue(RomConstants.Table.CLIMB_MAP, level_to_replace), freespace, generateClimbMap(map));
	    
	    freespace += writeChunkStructure(rom, bgstruct+8, freespace, map.layer1);
	    freespace += writeChunkStructure(rom, bgstruct+12, freespace, map.layer2);
	    freespace += writeChunkStructure(rom, bgstruct+16, freespace, map.layer3);
	    
	}else{
	    System.out.println(String.format("repointed nullchunks: %06X", freespace));
	    freespace += repointNullChunks(rom, RomConstants.getListValue(RomConstants.Table.WALL_COLLISION, level_to_replace), freespace, map.wallcol);
	    freespace += repointNullChunks(rom, RomConstants.getListValue(RomConstants.Table.CLIMB_MAP, level_to_replace), freespace, generateClimbMap(map));
	    
	    freespace += repointNullChunks(rom, bgstruct+8, freespace, map.layer1);
	    freespace += repointNullChunks(rom, bgstruct+12, freespace, map.layer2);
	    freespace += repointNullChunks(rom, bgstruct+16, freespace, map.layer3);
	}
	
	//If tileset is different it needs to be repointed, oldmap's tileset is likely also used by other maps
	if(repointEverything || !map.tileset1.equals(oldmap.tileset1)){
	    //4 bytes for tilecount + 32 for each tile
	    System.out.println(String.format("repointed tileset1: %06X %06X", freespace, bgstruct));
	    rom.write32(bgstruct, freespace);
	    freespace += 4 + 0x20*map.tileset1.length();
	}
	if(repointEverything || !map.tileset2.equals(oldmap.tileset2)){
	    System.out.println(String.format("repointed tileset2: %06X %06X", freespace, bgstruct+4));
	    rom.write32(bgstruct+4, freespace);
	    freespace += 4 + 0x20*map.tileset2.length();
	}
	
	//If entities are added, they must be repointed
	if(repointEverything || map.entities.size() > oldmap.entities.size()){
	    System.out.println(String.format("repointed ent: %06X", freespace));
	    rom.write32(RomConstants.getListValue(RomConstants.Table.ENTITIES, level_to_replace), freespace);
	    freespace += map.entities.size() * 16 + 4;
	}
	
	//If climbing areas are added, they must be repointed
	if(repointEverything || map.climbCol.size() > oldmap.climbCol.size()){
	    System.out.println(String.format("repointed climbcol: %06X", freespace));
	    rom.write32(RomConstants.getListValue(RomConstants.Table.CLIMB_COLLISION, level_to_replace), freespace);
	    freespace += map.climbCol.size() * 8;
	}
	
	if(repointEverything || !map.pal.equals(oldmap.pal)){
	    System.out.println(String.format("repointed pal: %06X", freespace));
	    rom.write32(RomConstants.getListValue(RomConstants.Table.LEVEL_BG_PAL, level_to_replace), freespace);
	    freespace += 0x20*14;
	}
	
	if(repointEverything || !map.sprpal.equals(oldmap.sprpal)){
	    System.out.println(String.format("repointed sprpal: %06X", freespace));
	    rom.write32(RomConstants.getListValue(RomConstants.Table.LEVEL_SPR_PAL, level_to_replace), freespace);
	    freespace += 0x20*16;
	}
	
	return freespace-ogf;
    }
    
    

     
    private static int writeChunkStructure(Rom rom, int chunkdst, int freespace, ChunkMap cmap){
	final int chunksize = cmap.getChunkSize();
	final int chunkcount = Math.max(cmap.getWChunks(),4)*cmap.getHChunks();
	int chunksWritten = 0;
	rom.write32(chunkdst, freespace);
	int i = 0;
	for(int y = 0;y<cmap.getHChunks();y++){
	    for(int x = 0;x < Math.max(cmap.getWChunks(), 4);x++){
		if(cmap.isChunkEmpty(x, y)){
		    rom.write32(freespace+4*i, RomConstants.nullchunk);
		}else{
		    rom.write32(freespace+4*i, freespace + 4*chunkcount + (chunksWritten)*chunksize);
		    chunksWritten++;
		}
		i++;
	    }
	}
	return 4*chunkcount + (chunksWritten)*(chunksize);  //amount of bytes used in freespace
    }
    

    
    
	        
    private static int repointNullChunks(Rom rom, int chunkdst, int freespace, ChunkMap tmap){
	final int chunkstruct = rom.read32(chunkdst);
	final int chunksize = tmap.getChunkSize();
	final int wChunks = tmap.getWChunks();
	
	int usedBytes = 0;
	int i = 0;
	for(int y = 0;y<tmap.getHChunks();y++){
	    for(int x = 0;x < wChunks;x++){
		if(tmap.isChunkEmpty(x, y)){	
		    //new chunk is empty, insert pointer to null chunk
		    rom.write32(chunkstruct+4*i, RomConstants.nullchunk); 
		}else if(rom.read32(chunkstruct+4*i) == RomConstants.nullchunk){
		    //new chunk is not empty but old is, repoint
		    rom.write32(chunkstruct+4*i, freespace);
		    freespace += chunksize;
		    usedBytes += chunksize;
		}
		i++;
	    }
	    i+= Math.max(0, 4 - wChunks);
	}
	return usedBytes;
    }
    

    
    
    //Assumes that all of the level pointers point to enough free space
    public static void insertMapData(Rom rom, Map map, int level_to_replace){
	final int bgstruct = RomConstants.getLevelBgStruct(rom, level_to_replace, 0); //Language is always english, maybe add an option to change it
	
	final int tileset1 = rom.read32(bgstruct);
	rom.write32(tileset1, map.tileset1.length());
	map.tileset1.saveTileset(rom, tileset1 + 4);
	
	final int tileset2 = rom.read32(bgstruct+4);
	rom.write32(tileset2, map.tileset2.length());
	map.tileset2.saveTileset(rom, tileset2 + 4);
	
	writeChunks(rom, map.layer1, rom.read32(bgstruct+8));
	writeChunks(rom, map.layer2, rom.read32(bgstruct+12));
	writeChunks(rom, map.layer3, rom.read32(bgstruct+16));
	
	final int colAddr = rom.read32(RomConstants.getListValue(RomConstants.Table.WALL_COLLISION, level_to_replace));
	writeChunks(rom, map.wallcol, colAddr);
	
	final int climbMapAddr = rom.read32(RomConstants.getListValue(RomConstants.Table.CLIMB_MAP, level_to_replace));
	writeChunks(rom, generateClimbMap(map), climbMapAddr);
	
	
	final int camAddr = rom.read32(RomConstants.getListValue(RomConstants.Table.LEVEL_BORDER, level_to_replace));
	rom.write16(camAddr, (short)map.cam_top);
	rom.write16(camAddr + 2, (short)map.cam_bot);
	rom.write16(camAddr + 4, (short)map.cam_left);
	rom.write16(camAddr + 6, (short)map.cam_right);
	
	int climbAddr = rom.read32(RomConstants.getListValue(RomConstants.Table.CLIMB_COLLISION, level_to_replace));
	for(ClimbCollision e : map.climbCol){
	    e.write(rom, climbAddr);
	    climbAddr += 8;
	}
	
	int entityAddr = rom.read32(RomConstants.getListValue(RomConstants.Table.ENTITIES, level_to_replace));
	for(Entity e : map.entities){
	    e.write(rom, entityAddr);
	    entityAddr += 16;
	}
	rom.write32(entityAddr, 0xF0000000);
	
	map.pal.savePalToRom(rom, rom.read32(RomConstants.getListValue(RomConstants.Table.LEVEL_BG_PAL, level_to_replace)), 32);
	map.sprpal.savePalToRom(rom, rom.read32(RomConstants.getListValue(RomConstants.Table.LEVEL_SPR_PAL, level_to_replace)), 0);
	
	rom.write32(RomConstants.getListValue(RomConstants.Table.LEVEL_SONG, level_to_replace), map.song);
	rom.write32(RomConstants.getListValue(RomConstants.Table.LEVEL_SCROLLFUNC, level_to_replace), map.scrollFunc);
	
	if(level_to_replace < RomConstants.junglejam_levels_start){
	    rom.write32(RomConstants.getListValue(RomConstants.Table.CRYSTAL_COCONUT_ID, level_to_replace), map.crystalCoconutId);
	    rom.write32(RomConstants.getListValue(RomConstants.Table.LEVEL_MEDAL_ADDR, level_to_replace), map.medalAddress);
	}
    }
    
    
    
    
    private static CollisionMap generateClimbMap(Map map){
	CollisionMap cmap = new CollisionMap(map.wChunks*32, map.hChunks*32);
	byte i = 1;
	for(ClimbCollision col : map.climbCol){
	    int startX = (col.x + col.x%8)/8;
	    int startY = (col.y+col.y%8)/8;
	    int width = col.w/8;
	    int height = col.h/8;
	    
	    //if tiles written before startX/Y, extend size to make up for it
	    if(col.y%8 > 0 && col.y%8 <= 4){height++;}
	    if(col.x%8 > 0 && col.x%8 <= 4){width++;}
	    
	    for(int y = startY;y < startY + height; y++){
		for(int x = startX;x < startX+width;x++){
		    cmap.setCollision(x, y, i);
		}
	    }
	    i++;
	}
	return cmap;
    }
    
    
    
    
    private static void writeChunks(Rom rom, ChunkMap cmap, int chunkpointers){
	for(int cx = 0; cx < cmap.getWChunks();cx++){
	    for(int cy = 0; cy < cmap.getHChunks();cy++){
		if(!cmap.isChunkEmpty(cx, cy)){
		    int chunkptr = rom.read32(chunkpointers + 4*cx + 4*4*cy);
		    if(chunkptr == 0){return;} //this should not even be possible but it seems to happen on fire necky
		    if(chunkptr == RomConstants.nullchunk){new Exception("[writeChunks] write to nullchunk").printStackTrace(); break;}
		    cmap.writeChunkToRom(rom, cx, cy, chunkptr);
		}
	    }
	}
    }
    
    
    
    
    public static int getFreespaceNeeded(Rom rom, Map map, int level_to_replace, int language, boolean repointEverything){
	int freespace = 0;
	Map oldmap = new Map(rom, level_to_replace, language);
	final int bgstruct = RomConstants.getLevelBgStruct(rom, level_to_replace, 0);
	
	//repoint chunk pointers and chunks if map is resized
	if(repointEverything || oldmap.hChunks < map.hChunks || (map.wChunks > 4 && oldmap.wChunks < map.wChunks)){
	    freespace += getChunkStructureSize(map.wallcol);
	    freespace += getChunkStructureSize(generateClimbMap(map));
	    
	    freespace += getChunkStructureSize(map.layer1);
	    freespace += getChunkStructureSize(map.layer2);
	    freespace += getChunkStructureSize(map.layer3);
	    
	}else{
	    freespace += getNullChunkRepointSize(rom, RomConstants.getListValue(RomConstants.Table.WALL_COLLISION, level_to_replace), map.wallcol);
	    freespace += getNullChunkRepointSize(rom, RomConstants.getListValue(RomConstants.Table.CLIMB_MAP, level_to_replace), generateClimbMap(map));
	    freespace += getNullChunkRepointSize(rom, bgstruct+8, map.layer1);
	    freespace += getNullChunkRepointSize(rom, bgstruct+12, map.layer2);
	    freespace += getNullChunkRepointSize(rom, bgstruct+16, map.layer3);
	}
	
	if(repointEverything || !map.tileset1.equals(oldmap.tileset1)){ freespace += 4 + 0x20*map.tileset1.length(); }
	if(repointEverything || !map.tileset2.equals(oldmap.tileset2)){ freespace += 4 + 0x20*map.tileset2.length(); }
	if(repointEverything || map.entities.size() > oldmap.entities.size()){ freespace += map.entities.size() * 16 + 4; }
	if(repointEverything || map.climbCol.size() > oldmap.climbCol.size()){ freespace += map.climbCol.size() * 8; }
	if(repointEverything || !map.pal.equals(oldmap.pal)){ freespace += 0x20*14; }
	if(repointEverything || !map.sprpal.equals(oldmap.sprpal)){ freespace += 0x20*16; }
	if(freespace > 0){freespace += 1;} //Freespace end marker byte
	return freespace;
    }
    
    
    
    
    private static int getChunkStructureSize(ChunkMap cmap){
	final int chunksize = cmap.getChunkSize();
	final int chunkcount = Math.max(cmap.getWChunks(),4)*cmap.getHChunks();
	int chunksWritten = 0;
	int i = 0;
	for(int y = 0;y<cmap.getHChunks();y++){
	    for(int x = 0;x < Math.max(cmap.getWChunks(), 4);x++){
		if(!cmap.isChunkEmpty(x, y)){
		    chunksWritten++; 
		}
		i++;
	    }
	}
	return 4*chunkcount + (chunksWritten)*(chunksize); 
    }
    

    
    
	        
    private static int getNullChunkRepointSize(Rom rom, int chunkdst, ChunkMap tmap){
	final int chunkstruct = rom.read32(chunkdst);
	final int chunksize = tmap.getChunkSize();
	final int wChunks = tmap.getWChunks();
	
	int usedBytes = 0;
	int i = 0;
	for(int y = 0;y<tmap.getHChunks();y++){
	    for(int x = 0;x < wChunks;x++){
		if(tmap.isChunkEmpty(x, y)){	
		}else if(rom.read32(chunkstruct+4*i) == RomConstants.nullchunk){
		    usedBytes += chunksize;
		}
		i++;
	    }
	    i+= Math.max(0, 4 - wChunks);
	}
	return usedBytes;
    }
}


	