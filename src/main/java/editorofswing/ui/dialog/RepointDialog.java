/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package editorofswing.ui.dialog;

import java.awt.Point;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.canvas.*;
import javafx.event.*;
import javafx.scene.paint.Color;
import javafx.scene.Scene;
import javafx.geometry.Insets;
import javafx.stage.Popup;

import java.util.ArrayList;
import javafx.collections.ObservableList;
import javafx.collections.FXCollections;
import javafx.beans.value.*;
import javafx.beans.*;
import javafx.beans.value.ObservableValue;
import javafx.stage.Stage;


import java.util.EventListener;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Pos;

import java.util.Optional;

public class RepointDialog{
    
    public static int getRepointAddr(int defaultAddress, int bytecount){
	TextInputDialog diag = new TextInputDialog(String.format("0x%06X", defaultAddress));
	diag.setContentText("Address:");
	diag.setHeaderText(String.format("Bytes needed: 0x%02X", bytecount));
	diag.setTitle("Repoint dialog");
	
	Optional<String> result = diag.showAndWait();
	if(!result.isPresent()){
	    return -1;
	}
	int ptr = editorofswing.util.StringUtil.hexStrToInt(result.get()) & 0x9FFFFFF;
	if(ptr < 0 || ptr > 32*1024*1024){
	    return -1;
	}
	
	return ptr;
    }
}
