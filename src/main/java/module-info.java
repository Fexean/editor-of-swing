module editorofswing {
    requires javafx.controls;
    //requires javafx.graphics;
    requires java.desktop;
    
    opens editorofswing to javafx.graphics;

    exports editorofswing;
}
