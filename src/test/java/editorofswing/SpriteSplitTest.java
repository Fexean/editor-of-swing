package editorofswing;

import org.junit.Test;
import static org.junit.Assert.*;
import editorofswing.util.RectUtil;
import java.awt.Rectangle;
import java.util.ArrayList;

public class SpriteSplitTest {

    @Test
    public void testNoSplit() {
		ArrayList<Rectangle> rects = new ArrayList();
		rects.add(new Rectangle(0, 0, 64, 64));
		rects = RectUtil.splitRectsIntoSprites(rects);
		assertEquals(1, rects.size());
    }

	@Test
    public void testSplitTo2() {
		ArrayList<Rectangle> rects = new ArrayList();
		rects.add(new Rectangle(0, 0, 128, 64));
		rects = RectUtil.splitRectsIntoSprites(rects);
		assertEquals(2, rects.size());
    }

	@Test
    public void testSplitTo4() {
		ArrayList<Rectangle> rects = new ArrayList();
		rects.add(new Rectangle(0, 0, 128, 128));
		rects = RectUtil.splitRectsIntoSprites(rects);
		assertEquals(4, rects.size());
    }


}
