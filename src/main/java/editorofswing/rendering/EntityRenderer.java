
package editorofswing.rendering;

import editorofswing.types.Entity;
import editorofswing.types.Map;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import java.util.HashMap;
import editorofswing.types.sprites.*;
import editorofswing.types.*;
import editorofswing.util.Rom;

public class EntityRenderer {
    GraphicsContext renderer;
    
    boolean drawSprites;
    SpriteRenderer sprRenderer;
    int sprpal[];
    Rom rom;
    
    //entity callback pointer is the key for both of these
    HashMap<Integer, DrawableSprite> spriteCache;
    HashMap<Integer, EntityTemplate> entTemplates;
    
    public EntityRenderer(Canvas c){
        renderer = c.getGraphicsContext2D();
	drawSprites = false;
    }
    
    
    public void setEntityTemplates(HashMap<String, EntityTemplate> entTemplates){
	this.entTemplates = new HashMap();
	for(EntityTemplate t : entTemplates.values()){
	    this.entTemplates.put(t.callback, t);
	}
	clearCache();
    }
    
    public void clearCache(){
        spriteCache = new HashMap();
    }
    
    public void setRom(Rom rom){
	sprRenderer = new SpriteRenderer(rom);
	sprRenderer.setGraphicsContext(renderer);
	this.rom = rom;
    }
    
    public void setPalette(Palette sprpal){
	this.sprpal = sprpal.convertToARGB();
    }
    
    public void setSpriteDrawing(boolean enabled){
	drawSprites = enabled;
    }
    
    
    
    private boolean tryDrawEntSprite(Entity ent){
	EntityTemplate template = entTemplates.get(ent.callback);
	if(template != null && template.spriteType != -1){
	    DrawableSprite spr = spriteCache.get(ent.callback);
	    if(spr == null){
		try{
		    switch(template.spriteType){ //-1=undefined, 0=regular, 1=affine, 2=player
			case 0:
			    spr = DrawableSprite.fromRegularSpr(rom, RegularSprite.fromGraphicId(rom, template.graphicId), sprpal, template.paletteSlot);
			    break;
			case 1:
			    spr = DrawableSprite.fromAffineSpr(rom, AffineSprite.fromGraphicId(rom, template.graphicId), sprpal, template.paletteSlot);
			    break;
			case 2:
			    spr = DrawableSprite.fromPlayerSpr(rom, AffineSprite.fromPlayerGraphicId(rom, template.graphicId), sprpal, template.paletteSlot);
			    break;
			default:
			    return false;
		    }
		}catch(Exception e){
		    System.out.println("[drawEntSprite]" + e.getMessage());
		    return false;
		}
	    }
	    spriteCache.put(template.callback, spr);
	    sprRenderer.draw(spr, ent.x - spr.w/2, ent.y - spr.h/2);
	    return true;
	}
	return false;
    }
    
    private void drawEntity(Entity ent, boolean isSelected){
	Color rectColor = isSelected ? Color.rgb(255,255,0, 1.0) : Color.rgb(0,255,255, 1.0);
	if(drawSprites && tryDrawEntSprite(ent)){
	    renderer.setStroke(rectColor);
	    renderer.strokeRect((ent.x-8), (ent.y-8), 16, 16);
	}else{
	    renderer.setFill(rectColor);
	    renderer.fillRect((ent.x-8), (ent.y-8), 16, 16);
	}
    }
    
    public void drawEntities(Map map){
	drawEntities(map, null);
    }
    
    public void drawEntities(Map map, Entity selected){
	for(Entity ent : map.entities){
	    if(ent != selected){
		drawEntity(ent, false);
	    }
	}
	
	if(selected != null){
	    drawEntity(selected, true);
	}
    }
}
