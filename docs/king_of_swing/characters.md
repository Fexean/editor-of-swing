# Playable characters
The game has multiple playable characters. Here are the IDs for each:

|Character id|name|
|-------|---------------|
|0x00| Donkey Kong
|0x01| Diddy Kong
|0x02| Dixie Kong
|0x03| Funky Kong
|0x04| King K. Rool
|0x05| Kremling
|0x06| Wrinkly Kong
|0x07| Bubbles
|0x08| Tutorial Wrinkly Kong

The playable characters are selected by the bytes in a 4 byte long array at 0x30002A0. The first byte is player 1's character, and the rest are for players 2-4.
The character id is stored in the player objects' field22 (byte).  

## Color variations
Each character has 4 different color palettes that can be chosen in the jungle jam mode.
The array of selected color palettes is at 0x30002A4 (byte / player). Its values don't have any effect in Adventure mode.  
The color variation id (0 to 3) is stored at offset 0x1CF inside the player object.  

A list of pointers to the character color palettes can be found at 0x86D84D8 in the rom. There are 4 pointers for each character, ordered by character id, one for each color variation. So the pointer for a palette can be found at `0x86D84D8 + 0x10 * characterId + 0x4 * colorVariation`.
Each pointer actually points to an array of 8 palettes (128 colors). The first palette contains the normally used colors, the next 5 are probably for going bananas and the last two are for fire and ice (when the player gets hit by Fire Necky/Sassy Squatch).


# Stats
Each character has its own set of 26 different stats that control different gameplay aspects.  
The stat table is an array of pointers where each pointer points to an array of 4 32-bit integers for that specific stat. The first of these ints is the value of the stat normally, the second one is the stat's value when the player is "going bananas"/invulnerable, and the last two values are always 0 and probably unused.  
The stat table for each character can be found at `0x85ABFC4 + 0x208 * id` (the pointers to these can be found at 0x85BDF54), where id is the character id. The ids are slightly different for stats: Tutorial Wrinkly Kong uses DK's stats and id 0x8 corresponds with enemy Kremlings.  

Here are the different stats by index number:

|Stat index|Description|
|-------|---------------|
|0x00| Swing speed (how fast the player is rotated when they are swinging on a peg)
|0x01| Swing speed 2 (Spinner fast)
|0x02| Swing speed 3 (Spinner slow)
|0x03| Swing jump speed (how much velocity the player gains after releasing a swing)
|0x04| Swing jump speed 2 (higher jump from spinner entity)
|0x05| Swing jump speed 3 (lower jump from spinner entity)
|0x06| Gravity (falling acceleration)
|0x07| Max falling speed
|0x08| Attack charge time (how long the player must hold l+r before they can attack)
|0x09| Attack duration
|0x0A| Attack speed (how fast the player moves during the attack)
|0x0B| Attack speed 2 (higher jump from spinner entity)
|0x0C| Attack speed 3 (lower jump from spinner entity)
|0x0D| Attack knockback (against other players)
|0x0E| Walking acceleration
|0x0F| Ground jump speed (when tapping l+r on the ground)
|0x10| Max walking speed
|0x11| Throw speed (How fast thrown rocks & bombs move, does not affect things thrown mid-air)
|0x12| Charge anim start (Dk's animation changes this many frames before an attack is fully charged with l+r?)
|0x13| Flight barrel speed
|0x14| Flight barrel turning speed
|0x15| Swing radius 1
|0x16| Player width (doesn't affect enemy collisions?)
|0x17| Player height (doesn't affect enemy collisions?)
|0x18| Swing radius 2
|0x19| ???

The game accesses these values with the function at 0x85C246C ("getPlayerStat"), it takes the stat index in R0 and a pointer to a player object in R11 and returns the value of the requested stat in R0.  


# Sound effects
The game has a dedicated function for playing character sound effects: 0x85C27F0 ("playPlayerSE"). It takes the player object in R11 and a sound effect id in R0 and plays the correct sound for that character.  
Here is a list of the different sound effect ids:  

|Index|Description|
|-------|---------------|
|0x00| Took damage
|0x01| Grabbed peg
|0x02| Grabbed peg? (when is this used??)
|0x03| Charging attack
|0x04| ???
|0x05| Collided with something during attack (plays when shot from barrel cannons, does it play on normal attacks?)
|0x06| Rock throw
|0x07| ???
|0x08| About to fall off edge (DK & Diddy only)
|0x09| Clapping celebration (Same as 0x12???)
|0x0A| DK celebration (Idle / Level exit)
|0x0B| Fell off the map (DK & Diddy only)
|0x0C| Bouncing off a wall
|0x0D| Released charged attack
|0x0E| Used "Going bananas"
|0x0F| Tried to heal while at full health / used "going bananas" or heal while not having enough bananas
|0x10| DK Idle (Do other characters use this??)
|0x11| Death (DK & Diddy only)
|0x12| Jungle Jam celebration
|0x13| Clapping celebration (Same as 0x12???)

The sound effect tables can be found at 0x85BDF7C, one pointer / character. Each pointer points to 0x14 sound pointers as described above.


# Animations
The animation table is at 0x85B2B1C, one pointer / character. Each pointer points to a list of 99(0x63) animations. The animations are 8 bytes long, with the first four being a pointer and the next 4 are length with some special bits?  
List of animations by index:  

|Index|Description|
|-------|---------------|
|0x00| Midair holding left
|0x01| Midair holding right
|0x02 | ???
|0x03 | ???
|0x04 | Midair, nothing held
|0x05 | ???
|0x06 | ???
|0x07 | ???
|0x08 | ???
|0x09 | ???
|0x0A | ???
|0x0B | ???
|0x0C | ???
|0x0D | ???
|0x0E | Standing (facing left)
|0x0F | Standing (facing right)
|0x10 | Walk left
|0x11 | Walk right
|0x12 | ???
|0x13 | ???
|0x14 | ???
|0x15 | ???
|0x16 | Jump (facing left)
|0x17 | Jump (facing right)
|0x18 | ???
|0x19 | ???
|0x1A | ???
|0x1B | ???
|0x1C | ???
|0x1D | ???
|0x1E | Mid-air damage
|0x1F | ???
|0x20 | ???
|0x21 | ???
|0x22 | ???
|0x23 | About to fall off ledge (facing left)
|0x24 | About to fall off ledge (facing right)
|0x25 | ???
|0x26 | ???
|0x27 | ???
|0x28 | ???
|0x29 | ???
|0x2A | ???
|0x2B | ???
|0x2C | ???
|0x2D | ???
|0x2E | ???
|0x2F | ???
|0x30 | ???
|0x31 | ???
|0x32 | ???
|0x33 | ???
|0x34 | ???
|0x35 | ???
|0x36 | ???
|0x37 | ???
|0x38 | ???
|0x39 | ???
|0x3A | ???
|0x3B | Level entry (on worldmap screen)
|0x3C | ???
|0x3D | ???
|0x3E | ???
|0x3F | ???
|0x40 | ???
|0x41 | ???
|0x42 | Death
|0x43 | ???
|0x44 | ???
|0x45 | ???
|0x46 | Level exit animation
|0x47 | Sassy squatch level exit 
|0x48 | ???
|0x49 | ???
|0x4A | ???
|0x4B | ???
|0x4C | Jungle Jam victory
|0x4D | ???
|0x4E | Squished in crusher wall
|0x4F | ???
|0x50 | ???
|0x51 | ???
|0x52 | ???
|0x53 | ???
|0x54 | ???
|0x55 | ???
|0x56 | ???
|0x57 | ???
|0x58 | ???
|0x59 | ???
|0x5A | ???
|0x5B | ???
|0x5C | ???
|0x5D | ???
|0x5E | ???
|0x5F | Ice damage taken
|0x60 | Ice death
|0x61 | ???
|0x62 | ???



# Player object structure

The object of player 1 is always at 0x2022000.

(Excluding the fields mentioned in the object page)  

|Offset|Size|Description|
|-------|-------|--------|
|0x4|2|keys held
|0x6|2|keys pressed
|0x8|1|player state (indexes function table at 0x85AD238)
|0x21|1|bit0 set if player is on the ground in jungle jam, bit2 is set when player is standing at the edge of a platform
|0x22|1|character id
|0x84|4|X-velocity
|0x88|4|Y-velocity
|0xB0|2|animation id
|0xE4|4|pointer to animation table
|0xE8|4|pointer to stat table
|0xF4|4|pointer to sound effect table
|0x117|1|set to 1 if the player is in a flight barrel
|0x1CF|1|color variation id
|0x1D0|4|pointer to flight barrel object?

