# Level data
The level data in king of swing is located in multiple different arrays across the rom which are indexed with the level id.  
If you are interested in the locations of these arrays, check out the offsets.cfg files.  



## Chunkmap
"Chunkmap" is a structure used with various level data. It's essentially just a two-dimensional array of pointers, where each pointer points to a 32x32 tile (256x256 pixel) chunk of the map.  
The width of a chunkmap is always 4 chunks, so to access the chunk at (chunkX, chunkY) you would index the chunkmap with 4*chunkY+chunkX.  
The chunks are uncompressed and thus they can be directly read from the rom.  
The chunkmap's dimensions are not stored anywhere, Editor of Swing guesses them from the camera boundaries.  



## Level backgrounds

The level background data is stored in in the following format:
|size(bytes)|name|
|--------------|-------|
|4|tileset 1 pointer
|4|tileset 2 pointer
|4|tilemap 1 pointer
|4|tilemap 2 pointer
|4|tilemap 3 pointer

The tilemaps are chunkmaps where each chunk is a 32x32 tilemap in the [typical GBA tilemap format](https://problemkaputt.de/gbatek.htm#lcdvrambgscreendataformatbgmap).  
Tilemap 1 is for background 1, tilemap 2 is for background 2 and tilemap 3 is for background 3.  
Tileset 1 is used with tilemap 1, while tileset 2 is used with tilemaps 2 & 3.  
In the European version of the rom each level contains 5 copies of this structure - one for each language.  

### Tileset
Tilesets are in the following format
|size(bytes)|name|
|--------------|-------|
|4|tilecount
|0x20*tilecount|tiles

The tiles are uncompressed 4bpp tiles which are copied to VRAM with DMA3.  



## Collision map
The collision map is a chunkmap that contains a byte for each tile of the map (so each chunk is 32*32 bytes long => 0x400 bytes).
Each byte contains the collision for the corresponding tile: the top 4 bits of the collision byte determine the type of collision while the bottom 4 bits determine which sides of the tile that collision applies to.
The collision map is one chunk taller than the other chunkmaps to allow the player to fall under the level.  



## Climbing

### Climb collisions
Each level has an array of climb collisions which describe what areas are climbable. Each peg has its own entry.  
They are in the following format:

|type|name|
|-----|-------|
|u16|x
|u16|y
|u8|width
|u8|height
|u16|flags

Only the lowest bit of "flags" is ever used. If set the player is pulled to the center of the climb collision. This is used for 1x1 pegs.  
**NOTE:** The coordinates actually point to the *center* of the climb collision rather than the top left corner.
Also the width and height are actually only *half* of their actual values so effectively each climb collision actually describes the bottom-right quarter of the climbable area.

### Climbmap
The climbmap is a chunkmap that contains a byte for each tile of the map (so each chunk is 32*32 bytes long => 0x400 bytes).
Each byte contains the index of the climb collision at that tile. The indexes actually start at 1 and 0 is used to signify the lack of a climb collision.
The game uses this map to quickly look up the climb collision at a given position.



## Camera
The rectangular boundaries of the camera are stored like this:

|type|name|
|-----|-------|
|u16|top
|u16|bottom
|u16|left
|u16|right

The bottom of the camera rectangle is actually at bottom+160 and the right side is actually at right+240.  


## Entities
See: [Entity](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/king_of_swing/entity.md)  


## Palettes
Each level has a 256-color sprite palette and a 256-color background palette.
The first 32 colors of the background palette are shared with all the levels so each level actually has 224 unique colors. This shared palette (referred to as globalpal in my code) is used for player's lifebar, banana count and the pause menu.  

## Medals & Crystal Coconuts
The first 0x70 levels have 32-bit values for both medals and crystal coconuts which are used to tell if the player has found the medal/coconut.  
For coconuts the game stores an offset to the variable used to store the coconut state (0x3000000 + coconut_offset).  
For medals the game stores a pointer to the variable used to store the medal state.  
For both the value is set to 0xFFFFFFFF on levels with no coconut/medal, and the byte at the chosen address is the same for both: 00=not collected, 01=collected  

## Song
Each level has a pointer to a song data that is played.  
The songs are in the format used by the sappy sound engine.  