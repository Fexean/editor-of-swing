# Sprite Engine
GBA's sprite hardware can be a little tedious to use manually, so games often implement higher level systems to make the usage of sprites easier.  
This file details the "sprite engine" used in DK: King of Swing.  

## Sprite Engine features
- Automatic tile allocation & loading  
- Automatic affine parameter allocation & calculation  
- Building sprites from smaller "subsprites" to form large sprites or non-rectangular sprites  
- Tilemaps for sprites (allows each tile of a sprite to be selected from a larger tileset)  
- Sprite layer scrolling (similar to the hardware background scrolling, but for sprites)  
- 6 different sprite modes for different uses  


## Sprite structure
King of Swing does not have a dedicated data structure for sprites, instead the object structure is also used to represent sprites.  
For an object to be drawn, a pointer to it must be stored in an array at 0x3003798, this is done in the function at 0x86CCEA0, which is called by the object system.    
There are 6 ways in which an object can be drawn. The byte at offset 0x20 in the object decides how the object is drawn.
The engine checks the bits of this value in the following order: 3, 5, 4, 1, 0. Each bit has its own routine (described below).  


## Sprite Templates / Tilesets
There are 3 sets of tilesets & sprite templates that are used to create the sprites:  

|name|description|tileset pointer|template pointer|
|-------|---------------|------------------|---------------------|
|Player sprites|Sprites used for playable characters|0x30001A0 (points to 0x85C72C4)|0x3000190 (points to 0x85C5004)
|Affine sprites|Sprites that consist of a single affine sprite|0x30001A4 (points to 0x852E280)|0x3000194 (points to 0x856BAC0)
|Regular sprites|Sprites that consist of multiple smaller subsprites, can't be affine|0x30001A8 (points to 0x8459A60)|0x3000198 (points to 0x8557AC0)

The tiles are just normal uncompressed 4bpp tiles, templates are described below.

### Player sprite template structure

|type|name|description|
|-----|--------|--------------|
|s8|yOffset|subtracted from object.y
|u8|attr0_top8|top 8 bits of OAM attribute 0
|s8|xOffset|subtracted from object.x
|u8| attr1_top8|top 8 bits of OAM attribute 1
|u8| preAlloc|this amount of tile 0s is allocated to the spriteTilePtrList before the sprite's tiles.
|u8|tilecount|size of tilemap
|u16[]| tilemap|one value for each tile of the sprite, selects which tile from the tileset is used
|u16| postAlloc|this amount of tile 0s is allocated to the spriteTilePtrList after the sprite's tiles.

("tile 0" refers to the first tile in the tileset)

### Affine sprite template structure
Same structure as the player sprite template.

### Regular sprite template structure

|type|name|description|
|-----|--------|--------------|
|u16|subspriteCount|how many subsprites come after this

followed by, for each subsprite:

|type|name|description|
|-----|--------|--------------|
|u8| size| top 2 bits set oam shape, next 2 set oam size, bottom 4 bits are unused?
|u8| flipXOffset| this is added twice to the x coordinate if the sprite is horizontally flipped
|u8| unk|
|u8| subspriteTileCount|this is used as the size of the "tilemap"
|s16| yOffset|added to the object's y-position
|s16| xOffset|added to the object's x-position
|u16[]| tilemap|one value for each tile of the subsprite, selects which tile from the tileset is used



# Different "sprite types"
The way in which an object is drawn depends on object.field_20 (u8)


## Bit 5 set (0x20)
Draws a single sprite from the Affine sprites table.  
- Indexes affine template with object.graphic_id (field 50 (u16))  
- if field 28 (u16) or field 2A (u16) of the object is larger than 0x100, the double/disable flag in OAM attribute 0 is set  
- uses object+0x54 as source for ObjAffineSet bios function to calculate affine parameters


## Bit 3 set (0x8)
Draws a single sprite from the Affine sprite table. Always enables the OBJ Window and disable/double size bits in the OAM attributes.  
- Indexes affine template with object.graphic_id
- uses object+0x54 as source for ObjAffineSet bios function to calculate affine parameters


## Bit 4 set (0x10)
Draws a basic OAM sprite without any tile or affine parameter allocation.  
- object.graphic_id is used as vram tile number  
- object.x & object.y determine position  
- object.pal sets palette  
- bits 4 & 5 of object.priority (field 53 (u8)) sets priority  
- object.field_4C (s16) is added to x-coordinate  
- object.field_4E (s16) is added to y-coordinate  
- object.field_4A (u16)  is ORred to attribute 1  
- object.field_48 (u16)  is ORred to attribute 0  


## Bit 0 set (type 0x1)
Draws a single affine sprite from the player sprite table.  
- Indexes player sprite template with graphic_id
- doesn't draw the sprite if its offscreen
- uses object+0x54 as source for ObjAffineSet bios function to calculate affine parameters


## Bit 1 set (type 0x2)
This is used to draw playable characters.  
Draws 2 or 3 sprites from the player sprite table, one for the object, another for object+4*sizeof(object) and a third for object+8*sizeof(object).  The order in which they are drawn depends on object.field_22 (u8).  
- if bit 0 is also set, the code for it will run instead (see above)
- each of the 3 sprites are drawn in the same way as they would be if their bit 0 was set

### Draw order
if field_22 is 6 or 8: draw obj, obj+4  
if field_22 is 1 or 2: draw obj+8, obj, obj+4  
if field_22 is 3, 4, 5, 7 or 9: obj+4, obj, obj+8  
else: draw obj, obj+4, obj+8  


## None of the above bits set (type 0x0)
Uses regular sprite templates & tiles to draw multiple subsprites according to the template.  
This is used for text and for most graphics in the game.  

- Uses language and object's graphics id to index sprite template list: (index = language * 0x1000 + object.graphic_id).  
- if first bit of object.field_5E is set, the sprite is flipped horizontally.  
- Object's x, y, priority and pal fields are used, also field_5E's bits 6 & 7 set the "OBJ Mode" in attribute 0 for all the subsprites  