# Audio
Like most GBA games, King of swing uses the m4a sound engine.  
For some reason Sappy can't find the game's song table, but [GBA Mus Ripper](https://github.com/jpmac26/gba-mus-ripper) can and dumps the game's music fine.  

Here is the output of GBA Mus Ripper's sappy_detector:

    Sappy sound engine detector (c) 2015 by Bregalad and loveemu
    
    Sound engine detected at offset 0x9fcf7c
    # of song levels: 10
    Engine parameters:
    Main Volume: 15 Polyphony: 12 channels, Dac: 9 bits, Sampling rate: 7884 Hz
    Song table located at: 0x9fff54