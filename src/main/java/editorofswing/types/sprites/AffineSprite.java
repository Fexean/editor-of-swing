
package editorofswing.types.sprites;
import editorofswing.util.Rom;
import editorofswing.util.RomConstants;

public class AffineSprite {
    public byte yOffset;
    public byte attr0_top8;
    public byte xOffset;
    public byte attr1_top8;
    public byte preAlloc;
    public byte tilecount;
    public short tmap[];
    public short postAlloc;
    
    public AffineSprite(Rom rom, int templateAddr){
	yOffset = rom.read8(templateAddr);
	attr0_top8 = rom.read8(templateAddr+1);
	xOffset = rom.read8(templateAddr+2);
	attr1_top8 = rom.read8(templateAddr+3);
	preAlloc = rom.read8(templateAddr+4);
	tilecount = rom.read8(templateAddr+5);
	
	tmap = new short[tilecount];
	for(int i = 0;i<tilecount;i++){
	    tmap[i] = rom.read16(templateAddr + 6 + 2*i);
	}
	
	postAlloc = rom.read16(templateAddr + 6 + 2*tilecount);
    }
    
    
    public void writeToRom(Rom rom, int templateAddr){
	rom.write8(templateAddr, yOffset);
	rom.write8(templateAddr+1, attr0_top8);
	rom.write8(templateAddr+2, xOffset);
	rom.write8(templateAddr+3, attr1_top8);
	rom.write8(templateAddr+4, preAlloc);
	rom.write8(templateAddr+5, tilecount);
	
	for(int i = 0;i<tilecount;i++){
	    rom.write16(templateAddr + 6 + 2*i, tmap[i]);
	}
	
	rom.write16(templateAddr + 6 + 2*tilecount, postAlloc);
    }
    
    public void writeToRomAffine(Rom rom, int graphicId){ //note: doesn't repoint
        writeToRom(rom, RomConstants.getListValue(RomConstants.Table.AFFINE_SPR_TEMPLATE, graphicId));
    }
    
    public void writeToRomPlayer(Rom rom, int graphicId){ //note: doesn't repoint
        writeToRom(rom, RomConstants.getListValue(RomConstants.Table.PLAYER_SPR_TEMPLATE, graphicId));
    }
    
    public static AffineSprite fromGraphicId(Rom rom, int id){
	int template = rom.read32(RomConstants.getListValue(RomConstants.Table.AFFINE_SPR_TEMPLATE, id));
	return new AffineSprite(rom, template);
    }
    
    public static AffineSprite fromPlayerGraphicId(Rom rom, int id){
	int template = rom.read32(RomConstants.getListValue(RomConstants.Table.PLAYER_SPR_TEMPLATE, id));
	return new AffineSprite(rom, template);
    }
    
    public int getWidth(){
	final byte oamWidths[] = {8,16,8,-1,16,32,8,-1,32,32,16,-1,64,64,32,-1};
	int size = (attr1_top8 >> 6) & 3;
	int shape = (attr0_top8 >> 6) & 3;
	return oamWidths[size<<2 + shape];
    }
    
    public int getHeight(){
	final byte oamHeights[] = {8,8,16,-1,16,8,32,-1,32,16,32,-1,64,32,64,-1};
	int size = (attr1_top8 >> 6) & 3;
	int shape = (attr0_top8 >> 6) & 3;
	return oamHeights[size<<2 + shape];
    }
}
