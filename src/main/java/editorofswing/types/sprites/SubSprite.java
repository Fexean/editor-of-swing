
package editorofswing.types.sprites;

import editorofswing.types.Tilemap;
import editorofswing.util.Rom;

public class SubSprite {
    public byte size;
    public byte flipXOffset;
    public byte unk;
    public byte subspriteTileCount;
    public short xOffset;
    public short yOffset;
    public short tmap[];
    
    public SubSprite(Rom rom, int address){
	size = rom.read8(address);
	flipXOffset = rom.read8(address+1);
	unk = rom.read8(address+2);
	subspriteTileCount = rom.read8(address+3);
	yOffset = rom.read16(address+4);
	xOffset = rom.read16(address+6);
	
	
	tmap = new short[subspriteTileCount];
	for(int i = 0;i<subspriteTileCount;i++){
	    tmap[i] = rom.read16(address + 8 + i*2);
	}
    }
    
    public SubSprite(int w, int h){
        size = 0;
        if(w == 16 && h == 8) size = (byte)(1 << 6);
        if(w == 8 && h == 16) size = (byte)(2 << 6);
        if(w == 16 && h == 16) size = (byte)(1 << 4);
        if(w == 32 && h == 8) size = (byte)(1 << 4 | 1 << 6);
        if(w == 8 && h == 32) size = (byte)(1 << 4 | 2 << 6);
        if(w == 32 && h == 32) size = (byte)(2 << 4);
        if(w == 32 && h == 16) size = (byte)(2 << 4 | 1 << 6);
        if(w == 16 && h == 32) size = (byte)(2 << 4 | 2 << 6);
        if(w == 64 && h == 64) size = (byte)(3 << 4);
        if(w == 64 && h == 32) size = (byte)(3 << 4 | 1 << 6);
        if(w == 32 && h == 64) size = (byte)(3 << 4 | 2 << 6);
        
	flipXOffset = 0;
	unk = 0;
	subspriteTileCount = (byte)(w*h/64);
	yOffset = 0;
	xOffset = 0;

	tmap = new short[subspriteTileCount];
    }
    
    public void writeToRom(Rom rom, int address){
	rom.write8(address, size);
	rom.write8(address+1, flipXOffset);
	rom.write8(address+2, unk);
	rom.write8(address+3, subspriteTileCount);
	rom.write16(address+4, yOffset);
	rom.write16(address+6, xOffset);
	for(int i = 0;i<subspriteTileCount;i++){
	    rom.write16(address + 8 + i*2, tmap[i]);
	}
    }
    
    public int getSize(){
	return 8 + subspriteTileCount*2;
    }
    
    public int getWidth(){
	final byte oamWidths[] = {8,16,8,-1,16,32,8,-1,32,32,16,-1,64,64,32,-1};
	int shape = (size >> 6) & 3;
	int size_ = (size >> 4) & 3;
	int idx = (size_ << 2) + shape;
	return oamWidths[idx];
    }
    
    public int getHeight(){
	final byte oamHeights[] = {8,8,16,-1,16,8,32,-1,32,16,32,-1,64,32,64,-1};
	int shape = (size >> 6) & 3;
	int size_ = (size >> 4) & 3;
	int idx = (size_ << 2) + shape;
	return oamHeights[idx];
    }
    
    
    @Override
    public String toString(){
	return String.format("size:%02X flipXOffset:%02X unk:%02X subspriteTileCount:%02X x:%d y:%d", size, flipXOffset, unk, subspriteTileCount, xOffset, yOffset);
    }
}
