

package editorofswing.util;
import java.util.HashMap;

public class RomConstants {
    public enum Table {
	CLIMB_COLLISION, WALL_COLLISION, CLIMB_MAP,
	LEVEL_BORDER, ENTITIES, LEVEL_BG_PAL, LEVEL_SPR_PAL,
	LEVEL_GLOBALPAL, LEVEL_SONG, LEVEL_SCROLLFUNC,
	CRYSTAL_COCONUT_ID, LEVEL_MEDAL_ADDR,
	REGULAR_SPR_TEMPLATE, AFFINE_SPR_TEMPLATE, PLAYER_SPR_TEMPLATE,
    };
    
    public enum SpriteType{
        REGULAR, AFFINE, PLAYER
    };
    
    private static int climbCollisionList;
    private static int wallCollisionList;
    private static int levelDimensionList;
    private static int levelEntityList;
    private static int levelBgList;
    private static int levelPalList;
    private static int levelSongs;
    private static int levelScrollFuncs;
    private static int levelCrystalCoconutIds;
    private static int levelMedalList;
    private static int globalLevelPal; //first 32 colors of bg palette
    private static int characterStatTable;
    
    public static int freespace;
    public static int nullchunk;
    public static int junglejam_levels_start;
    
    public static int emptySaveFile;
    public static int TimeAttackWorldNames;
    public static int TimeAttackLevelNames;
    public static int TimeAttackLevelIds;
    
    public static int regularSprTemplates;
    public static int affineSprTemplates;
    public static int playerSprTemplates;
    public static int regularSprTiles;
    public static int affineSprTiles;
    public static int playerSprTiles;
    public static int spritePtrLiteralPool;
    
    public static int regularTileCnt, affineTileCnt, playerTileCnt;
    
    public static void initOffsetsForRom(Rom rom){
	String path = rom.getVersion() + "_VERSION/offsets";
	HashMap<String, Integer> offsets = ConfigReader.readHexSymbols(path);
	
	climbCollisionList = offsets.get("climbCollisionList");
	wallCollisionList = offsets.get("wallCollisionList");
	levelDimensionList=offsets.get("levelDimensionList");
	levelEntityList=offsets.get("levelEntityList");
	levelBgList=offsets.get("levelBgList");
	levelPalList=offsets.get("levelPalList");
	levelSongs=offsets.get("levelSongs");
	levelScrollFuncs=offsets.get("levelScrollFuncs");
	levelCrystalCoconutIds=offsets.get("levelCrystalCoconutIds");
	levelMedalList=offsets.get("levelMedalList");
	globalLevelPal=offsets.get("globalLevelPal");
	freespace=offsets.get("freespace");
	nullchunk=offsets.get("nullchunk");
	junglejam_levels_start = offsets.get("junglejam_levels_start");
	
	emptySaveFile=offsets.get("emptySaveFile");
	TimeAttackWorldNames=offsets.get("TimeAttackWorldNames");
	TimeAttackLevelNames=offsets.get("TimeAttackLevelNames");
	TimeAttackLevelIds=offsets.get("TimeAttackLevelIds");
	
	spritePtrLiteralPool=offsets.get("SpritePointerPool");
	characterStatTable = offsets.get("CharacterStats");
        
        regularTileCnt = offsets.get("RegularSpriteTileCount");
        affineTileCnt = offsets.get("AffineSpriteTileCount");
        playerTileCnt = offsets.get("PlayerSpriteTileCount");
        
        initSpriteOffsets(rom);
    }
    
    private static void initSpriteOffsets(Rom rom){
        regularSprTemplates = rom.read32(spritePtrLiteralPool+0x14);
	affineSprTemplates = rom.read32(spritePtrLiteralPool+0x10);
	playerSprTemplates = rom.read32(spritePtrLiteralPool+0xc);
	regularSprTiles = rom.read32(spritePtrLiteralPool+8);
	affineSprTiles = rom.read32(spritePtrLiteralPool+4);
	playerSprTiles = rom.read32(spritePtrLiteralPool);
    }
    
    public static int getListValue(Table list, int index){
	switch(list){
	    case CLIMB_COLLISION:
		return climbCollisionList + 8*index;
	    case CLIMB_MAP:
		return climbCollisionList + 8*index + 4;    //
	    case WALL_COLLISION:
		return wallCollisionList + 4*index;
	    case LEVEL_BORDER:
		return levelDimensionList + 4*index;
	    case ENTITIES:
		return levelEntityList + 4*index;
	    case LEVEL_BG_PAL:
		return levelPalList + 8*index;
	    case LEVEL_SPR_PAL:
		return levelPalList + 8*index+4;
	    case LEVEL_GLOBALPAL:
		return globalLevelPal;
	    case LEVEL_SONG:
		return levelSongs + 4*index;
	    case LEVEL_SCROLLFUNC:
		return levelScrollFuncs + 4*index;
	    case CRYSTAL_COCONUT_ID:
		return levelCrystalCoconutIds + 4*index;
	    case LEVEL_MEDAL_ADDR:
		return levelMedalList + 4*index;
	
	    case REGULAR_SPR_TEMPLATE:
		return regularSprTemplates + 4*index;  
	    case AFFINE_SPR_TEMPLATE:
		return affineSprTemplates + 4*index;
	    case PLAYER_SPR_TEMPLATE:
		return playerSprTemplates + 4*index;
	}
	return 0;
    }
    
    
    
    /*
    bgstruct:	
	int tileset1 = rom.read32(bgstruct);
	int tileset2 = rom.read32(bgstruct+4);
	int tilemap1 = rom.read32(bgstruct+8);
	int tilemap2 = rom.read32(bgstruct+12);
	int tilemap3 = rom.read32(bgstruct+16);
    */
    public static int getLevelBgStruct(Rom rom, int levelIndex, int language){
	if(rom.getVersion() == Rom.Version.EU){
	    return rom.read32(levelBgList + 4*language) + 20*levelIndex;
	}else{
	    return levelBgList + 20*levelIndex;
	}
    }
    
    
    public static int getCharacterStatTable(int characterId){
	return characterStatTable + 0x208 * characterId;
    }
    
    public static int getSpriteTilePtr(SpriteType type){
        switch(type){
            case REGULAR: return regularSprTiles;
            case AFFINE: return affineSprTiles;
            case PLAYER: return playerSprTiles;
        }
        return 0;
    }
}
