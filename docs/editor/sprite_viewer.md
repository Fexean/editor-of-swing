# Sprite Viewer

The sprite viewer tool lets you not only view [sprites](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/king_of_swing/sprite.md) but also replace them. It can be accessed from the Tools menu in the main window.


## Functionality

The window contains a choicebox that lets you select one of the three sprite tables: regular, affine and player. You can use the spinbox to select the graphic ID of the selected sprite.

The window also contains the following:

**Save sprite** Saves selected sprite as PNG. Useful for inserting edited versions of existing sprites.

**Insert sprite** Lets you select a PNG from your computer that will be inserted over the currently selected sprite. The image should be a 16-color indexed image.

When a sprite is inserted, its template will be created in free space, and its tiles will be inserted at the first continuous empty arena in the tileset of the selected sprite type.

**Repoint sprite tiles** Allows you to repoint the shared sprite tilesets. Insertion won't really work before doing this since the game doesn't have empty tiles by default. Allocating all 65536 tiles for all three tilesets takes around 6MB of ROM space.

**Free sprite tiles** If enabled, the insert sprite option will delete the tiles of the sprite that is replaced, allowing them to be reused by later sprite insertions. You should avoid using this with sprites that you didn't insert since the vanilla game's sprites can share tiles with other sprites, and deleting them can thus corrupt other sprites.

