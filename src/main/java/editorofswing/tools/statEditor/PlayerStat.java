
package editorofswing.tools.statEditor;
import editorofswing.util.Rom;

public class PlayerStat {
    public int normalValue;
    public int specialValue;
    
    void read(Rom rom, int address){
	normalValue = rom.read32(address);
	specialValue = rom.read32(address + 4);
    }
    
    void write(Rom rom, int address){
	rom.write32(address, normalValue);
	rom.write32(address + 4, specialValue);
    }
}
