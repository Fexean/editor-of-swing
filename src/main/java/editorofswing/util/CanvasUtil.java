
package editorofswing.util;
import javafx.scene.canvas.*;

public class CanvasUtil {
    public static void scaleCanvas(Canvas canvas, double scale){
	canvas.setTranslateX(canvas.getWidth());
	canvas.setTranslateY(canvas.getHeight());
	canvas.setWidth(scale*canvas.getWidth());
	canvas.setHeight(scale*canvas.getHeight());
	canvas.setScaleX(scale);
	canvas.setScaleY(scale);
    }
    
}
