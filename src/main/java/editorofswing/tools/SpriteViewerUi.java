
package editorofswing.tools;

import editorofswing.types.*;
import editorofswing.util.*;
import editorofswing.util.RomConstants;
import javafx.scene.control.*;
import javafx.scene.canvas.Canvas;
import javafx.scene.layout.*;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.geometry.Insets;
import editorofswing.rendering.SpriteRenderer;
import editorofswing.types.sprites.*;
import javafx.scene.paint.Color;
import editorofswing.ui.dialog.FileDialog;

import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;

public class SpriteViewerUi {
    Rom rom;
    Scene scene;
    SpriteRenderer renderer;
    
    Palette monochrome;
    
    ComboBox spriteTypeSelector = new ComboBox();
    Label errorLabel = new Label();
    Label templateAddrLable = new Label();
    Canvas canvas = new Canvas(256, 256);
    Button exportButton = new Button("Save as PNG...");
    Button importButton = new Button("Import Sprite...");
    Button repointButton = new Button("Repoint Tiles...");
    Spinner spriteSelector = new Spinner(0, 0xffff, 0);
    RadioButton freeTilesToggle = new RadioButton("Delete old tiles on import");
    
    public SpriteViewerUi(Rom rom){
	this.rom = rom;
	renderer = new SpriteRenderer(rom);
	
	VBox root = new VBox();
	setupUi(root);
	scene = new Scene(root, 400, 450);
        monochrome = Palette.monochromePal(16);
    }
    
    private void setupUi(VBox root){
	renderer.setCanvas(canvas);
	root.setAlignment(Pos.CENTER);
	root.setPadding(new Insets(10, 10, 10, 10));
	root.setSpacing(10);
	
	spriteSelector.setEditable(true);
	spriteTypeSelector.getItems().addAll("Regular", "Affine", "Player");
	spriteTypeSelector.getSelectionModel().select(0);
        
        HBox buttonBox = new HBox();
        buttonBox.getChildren().addAll(exportButton, importButton, repointButton);
        buttonBox.setSpacing(10);
	root.getChildren().addAll(errorLabel, canvas, templateAddrLable, spriteSelector, spriteTypeSelector, buttonBox, freeTilesToggle);
	
	spriteSelector.valueProperty().addListener((a,prevValue,value) -> selectSprite((Integer)value));
	spriteTypeSelector.valueProperty().addListener(e -> selectSprite((Integer)spriteSelector.valueProperty().get()));
	exportButton.setOnAction(e -> exportImagePng());
        importButton.setOnAction(e -> importSprite());
        repointButton.setOnAction(e -> repointSprTiles());
        
	selectSprite(0);
    }
    
    
    private void selectSprite(int graphicId){
	errorLabel.setText("");
	renderer.clearCanvas();
	try{
	    int template = 0;
	    switch(spriteTypeSelector.getSelectionModel().getSelectedIndex()){
		case 0:
		    renderer.drawRegular(RegularSprite.fromGraphicId(rom, graphicId), 0, 0, monochrome, 0);
		    template = rom.read32(RomConstants.getListValue(RomConstants.Table.REGULAR_SPR_TEMPLATE, graphicId));
		    break;
		case 1:
		    renderer.drawAffine(AffineSprite.fromGraphicId(rom, graphicId), 0, 0, monochrome, 0);
		    template = rom.read32(RomConstants.getListValue(RomConstants.Table.AFFINE_SPR_TEMPLATE, graphicId));
		    break;
		case 2:
		    renderer.drawPlayer(AffineSprite.fromPlayerGraphicId(rom, graphicId), 0, 0, monochrome, 0);
		    template = rom.read32(RomConstants.getListValue(RomConstants.Table.PLAYER_SPR_TEMPLATE, graphicId));
		    break;
	    }
	    templateAddrLable.setText(String.format("Template address: 0x%08X", template));
            exportButton.setDisable(false);
	}catch(Exception e){
	    errorLabel.setText("Unable to draw sprite:\n" + e.getMessage());
	    templateAddrLable.setText("Template address: --------");
            exportButton.setDisable(true);
	}
    }
    
    
    private void importSprite(){
        int graphicId = (int)spriteSelector.getValue();
        int sprType = spriteTypeSelector.getSelectionModel().getSelectedIndex();
        RomConstants.SpriteType spriteType = RomConstants.SpriteType.values()[sprType];
        SpriteInserter inserter = new SpriteInserter(rom);
        
        
        java.io.File f = FileDialog.openFile(scene.getWindow(), "Select sprite to import", "PNG Image", "*.png");
        if(f == null){
            return;
        }
        try{
            BufferedImage img = ImageIO.read(f);
            int tileNum = inserter.findFreeTileNum(spriteType, img);
            int freespace = RomMisc.align4(RomMisc.findFreeSpace(rom, RomConstants.freespace, 10000)); //TODO: maybe find out space needed
            
            if(freespace == -1) throw new Exception("No freespace found");
            if(tileNum == -1) throw new Exception("No free tiles found. Did you repoint the tilesets?");
            
            if(freeTilesToggle.isSelected()){
                inserter.freeSpriteTiles(spriteType, graphicId);
            }
            
            switch(spriteType){
		case AFFINE:
                    inserter.insertImgAsAffineSprite(graphicId, img, freespace, tileNum);
		    break;
		case REGULAR:
		    inserter.insertImgAsRegularSprite(graphicId, img, freespace, tileNum);
		    break;
		case PLAYER:
		    inserter.insertImgAsPlayerSprite(graphicId, img, freespace, tileNum);
		    break;
	    }
            selectSprite(graphicId); //redraw
            rom.save();
        }catch(Exception e){
            new Alert(Alert.AlertType.ERROR, e.getMessage()).showAndWait();
        }
    }
    
    private void exportImagePng(){
        int graphicId = (int)spriteSelector.getValue();
        int sprType = spriteTypeSelector.getSelectionModel().getSelectedIndex();
        RomConstants.SpriteType spriteType = RomConstants.SpriteType.values()[sprType];
        
        java.io.File f = FileDialog.saveAs(scene.getWindow(), "Save sprite as...", "PNG Image", "*.png", graphicId+".png");
        if(f == null){
            return;
        }
        SpriteExporter exporter = new SpriteExporter(rom);
        exporter.exportSpritePng(spriteType, graphicId, f);
    }
    
    private void repointSprTiles(){
        int[] oldCounts = {RomConstants.affineTileCnt, RomConstants.regularTileCnt, RomConstants.playerTileCnt};
        int[] newCounts = {0x10000, 0x10000, 0x10000};
        TextField[] fields = new TextField[6];
        
        GridPane root = new GridPane();
        root.setPadding(new Insets(0, 0, 0, 10));
	
	root.setHgap(8);
	root.setVgap(4);
        
        root.add(new Label("Old"), 1, 0);
        root.add(new Label("New"), 2, 0);
        root.add(new Label("Affine tiles"), 0, 1);
        root.add(new Label("Regular tiles"), 0, 2);
        root.add(new Label("Player tiles"), 0, 3);
        
        for(int i = 0;i<3;i++){
            fields[i] = new TextField(""+oldCounts[i]);
            fields[i+3] = new TextField(""+newCounts[i]);
            fields[i].setPrefWidth(70);
            fields[i+3].setPrefWidth(70);
            root.add(fields[i], 1, i+1);
            root.add(fields[i+3], 2, i+1);
        }
        
        Stage stage = new Stage();
        Button repoint = new Button("Repoint");
        repoint.setOnAction(ev -> {
            try{
                int freespace = RomMisc.align4(rom.sizeUsed()); //place tiles at the end of rom to avoid slow freespace search
                if(freespace > 32*1024*1024 - 0x20*(newCounts[0] + newCounts[1] + newCounts[2]))
                    throw new Exception("No freespace at end of ROM.");

                for(int i = 0;i<3;i++){
                    oldCounts[i] = Integer.parseInt(fields[i].getText());
                    newCounts[i] = Integer.parseInt(fields[i+3].getText());
                }
                new SpriteRepointer(rom).repointSpriteTiles(freespace, oldCounts, newCounts);
                rom.save();
                stage.close();
            }catch(Exception e){
                new Alert(Alert.AlertType.ERROR, e.getMessage()).showAndWait();
            }
        });
        root.add(repoint, 0, 4);
        showAndWait(stage, new Scene(root, 280, 140), "Repoint Sprite Tilesets");
    }
    
    public static void openTool(Rom rom){
	SpriteViewerUi ui = new SpriteViewerUi(rom);
	ui.showAndWait(new Stage(), ui.scene, "Sprite Viewer");
    }
    
    public void showAndWait(Stage stage, Scene scene, String title){
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle(title);
	stage.setScene(scene);
	stage.setResizable(false);
        stage.showAndWait();
    }
    
    
}
