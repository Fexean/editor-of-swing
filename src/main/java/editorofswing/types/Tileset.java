
package editorofswing.types;
import editorofswing.util.Rom;
import java.util.ArrayList;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.File;



public class Tileset implements java.io.Serializable{
    private static final long serialVersionUID = 1L;
    
    private ArrayList<Tile> tileset;
    
    public Tileset(Rom rom, int tilesetStruct){
        tileset = new ArrayList();
	int tileCount = rom.read32(tilesetStruct);
	if(tileCount > 16*1024 || tileCount < 0){   //upper limit is arbitrary
	    new Exception("[Tileset] tilecount is invalid ("+tileCount+String.format(" @0x%06X", tilesetStruct)).printStackTrace();
	    tileCount = 0;
	}
	byte[] tileBuffer = rom.read(tilesetStruct+4, tileCount * 32);
	for(int i = 0;i<tileBuffer.length;i+=32){
            tileset.add(Tile.loadTile4bpp(tileBuffer, i));
        }
    }
    
    public Tileset(Rom rom, int tilesetStart, int tilecount){
	tileset = new ArrayList();
	byte[] tileBuffer = rom.read(tilesetStart, tilecount * 32);
	for(int i = 0;i<tileBuffer.length;i+=32){
            tileset.add(Tile.loadTile4bpp(tileBuffer, i));
        }
    }
    
    public Tileset(int tileCount){
        tileset = new ArrayList<>();
        for(int i = 0;i<tileCount;i++){
            tileset.add(new Tile());
        }
    }
    
    public Tileset(ArrayList<Tile> t){
	tileset = t;
    }
    
    public Tile getTile(int index){
        if(index >= 0  && index < tileset.size()){
            return tileset.get(index);
        }
        return new Tile();
    }
    
    public ArrayList<Tile> getTiles(){
	return tileset;
    }
    
    public void setTile(int index, Tile t){
	if(index >= 0  && index < tileset.size()){
            tileset.set(index, t);
        }
    }
    
    
    public boolean saveTileset(Rom rom, int address){
        int offset = 0;
        for(Tile t : tileset){
            t.storeToRom4bpp(rom, address+offset);
            offset+=32;
        }
        return false;
    }

    
    public int length(){
	return tileset.size();
    }
    
    
    public void exportPng(File file, Palette pal) throws Exception{
	int w = (int)Math.sqrt(tileset.size());
	for(;w > 1 && tileset.size() % w != 0;w--);
	int h = tileset.size() / w;
	
	BufferedImage img = new BufferedImage(8*w,8*h, BufferedImage.TYPE_BYTE_INDEXED,pal.getColorModel());
	for(int i = 0;i<tileset.size();i++){
	    tileset.get(i).drawTileToBufferedImage(pal, img, 8*(i%w), (i/w)*8);
        }
	ImageIO.write(img, "png", file);	
    }
    
    public Tileset(File file) throws Exception{
        this(ImageIO.read(file));
    }
    
    public Tileset(BufferedImage img) throws Exception{
	if(img.getType() != BufferedImage.TYPE_BYTE_INDEXED){
	    throw new IllegalArgumentException("File is not a indexed image!" + img.getType());
	}
	int w = img.getWidth() / 8;
	tileset = new ArrayList();
	for(int i = 0;i<img.getHeight()*w/8;i++){
	    int x = (i%w)*8;
	    int y = (i/w)*8;
	    tileset.add(new Tile(img, x, y, true));
	}
    }
    
    
    
    @Override
    public boolean equals(Object other){
	if(other instanceof Tileset){
	    Tileset other_ = (Tileset)other;
	    if(other_.length() != length()){
		return false;
	    }
	    return tileset.equals(other_.tileset);
	}
	return false;
    }
}
