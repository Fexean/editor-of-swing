package editorofswing.types;

import javafx.scene.paint.Color;
import java.awt.image.IndexColorModel;
import java.io.BufferedReader;
import java.io.File;
import editorofswing.util.Rom;


public class Palette implements java.io.Serializable{
    private static final long serialVersionUID = 1L;	
    public Color[] colors;

	
	public Palette(int c){
		if(c > 0){
			colors = new Color[c];
		}
	}
        
        public Palette(Rom rom, int offset){
            Palette p = loadPalFromRom(rom, offset, 16);
            colors = p.colors;
        }
        
        
        public Palette(Rom rom, int offset, int count){
            if(count > 15){
		(new Exception("[Palette]Misuse of palette, use palcount instead of color count!")).printStackTrace();
		count/=16;
	    }
	    Palette tmp = loadPalFromRom(rom, offset, count*16);
            colors = tmp.colors;
        }
        
        
        public Palette(){
            colors = new Color[16];
        }
	
	public static short RGBtoGBA(byte r, byte g, byte b){
		r = (byte)((r >> 3) & 0x1F);
		g = (byte)((g >> 3) & 0x1F);
		b = (byte)((b >> 3) & 0x1F);
		short result = (short)r;
		result |= (short)(g<<5);
		result |= (short)(b<<10);
		return (short)(result&0x7FFF);
	}
	
	public static Color GBAtoRGB(short color){
                int r = (color&0x1F) << 3;
		int g = ((color>>5)&0x1F) << 3;
		int b = ((color>>10)&0x1F) << 3;
		return Color.rgb(r,g,b);
	}
	
	public static short colorToGBA(Color c){
		byte r = (byte)(c.getRed()*255);
		byte g = (byte)(c.getGreen()*255);
		byte b = (byte)(c.getBlue()*255);
		return RGBtoGBA(r,g,b);
	}
	
	
	public static Palette loadPalFromRom(Rom rom, int address, int colors){
		Palette pal = new Palette(colors);
		for(int i = 0;i<colors;i++){
			short color = rom.read16(address+i*2);
			pal.colors[i] = GBAtoRGB(color);
		}
		return pal;
	}
	
	
	public void savePalToRom(Rom rom, int address){
	    savePalToRom(rom, address, 0);
	}
	
	public void savePalToRom(Rom rom, int address, int startingColor){
	    if(colors.length > 256){
		return;
	    }
	    for(int i = 0;i+startingColor<colors.length;i++){
		rom.write16(address+i*2, colorToGBA(colors[startingColor + i]));
	    }
	}
	
	public void concatenate(Palette pal){
	    Color newpal[] = new Color[colors.length + pal.colors.length];
	    System.arraycopy(colors, 0, newpal, 0, colors.length);
	    System.arraycopy(pal.colors, 0, newpal, colors.length, pal.colors.length);
	    colors = newpal;
	}
	
	public void printColors(){
            System.out.println("Palette: ");
            for(int i = 0;i<colors.length;i++){
                System.out.print("("+256*colors[i].getRed()+" "+256*colors[i].getGreen()+" "+256*colors[i].getBlue()+") ");
            }
            System.out.println("");
        }
        
        
        public boolean containsColor(Color color){
            for(int i = 0;i<colors.length;i++){
                if(colors[i] == color){
                    return true;
                }
            }
            return false;
        }
        
        public byte indexOf(Color color){
            for(byte i = 0;i<colors.length;i++){
                if(colors[i] == color){
                    return i;
                }
            }
            return -1;
        }
        
        public int closest(Color c){ 
            double minDif=999;
            int minID=0;
            for(int i = 0;i<colors.length;i++){
                Color a = colors[i];
                double difR = Math.abs(c.getRed() - a.getRed());
                double difG = Math.abs(c.getGreen() - a.getGreen());
                double difB = Math.abs(c.getBlue() - a.getBlue());
                double dist = Math.sqrt(difR * difR + difG * difG + difB * difB);
                if(dist < minDif){
                    minDif = dist;
                    minID = i;
                }
            }
	    
            //System.out.println("Dif:" + minDif + " Color:"+255*c.getRed() +" " + 255*c.getGreen() + " "+255*c.getBlue() + " PalColor:"+255*colors[minDex].getRed() +" " + 255*colors[minDex].getGreen() + " "+255*colors[minDex].getBlue());
            return minID;     
        }
	
	public Palette(IndexColorModel cm){
	    colors = new Color[cm.getMapSize()];
	    byte[] r,g,b;
	    r = new byte[colors.length];
	    g = new byte[colors.length];
	    b = new byte[colors.length];
	    cm.getReds(r);
	    cm.getGreens(g);
	    cm.getBlues(b);
	    int index = 0;
	    for(Color c : colors){
		colors[index] = Color.rgb(r[index]&0xFF, g[index]&0xFF, b[index]&0xFF);
		index++;
	    }
	}
	
	public IndexColorModel getColorModel(){
	    byte[] r,g,b;
	    r = new byte[colors.length];
	    g = new byte[colors.length];
	    b = new byte[colors.length];
	    int index = 0;
	    for(Color c : colors){
		r[index] = (byte)(c.getRed()*255);
		g[index] = (byte)(c.getGreen()*255);
		b[index++] = (byte)(c.getBlue()*255);
	    }
	    return new IndexColorModel(8, colors.length, r, g, b);
	}
	
	
	
	public boolean exportJASC(File f){
	    try{
		java.io.FileWriter of = new java.io.FileWriter(f);
		of.write("JASC-PAL\n0100\n" + colors.length + "\n");
		for(Color c : colors){
		    of.write((int)(c.getRed()*255) + " " + (int)(c.getGreen()*255) + " " + (int)(c.getBlue()*255) + "\n");
		}
		of.close();
	    }catch(Exception e){
		return false;
	    }
	    return true;
	}
	
	public static Palette importJASC(File f){
	    Palette pal;
	    try{
		BufferedReader in = new BufferedReader(new java.io.FileReader(f));
		if(!in.readLine().equals("JASC-PAL") || !in.readLine().equals("0100")){
		    in.close();
		    return null;
		}
		int colorcnt = Integer.parseInt(in.readLine());
		if(colorcnt > 256){
		    in.close();
		    return null;
		}
		pal = new Palette(colorcnt);
		for(int i = 0;i<colorcnt;i++){
		    String c[] = in.readLine().split(" ");
		    int r = Integer.parseInt(c[0]);
		    int g = Integer.parseInt(c[1]);
		    int b = Integer.parseInt(c[2]);
		    pal.colors[i] = Color.rgb(r, g, b);
		}
		
	    }catch(Exception e){
		return null;
	    }
	    
	    return pal;
	}
	
	
	public int[] convertToARGB(){
	    int argb[] = new int[256];
	    for(int i = 0;i<colors.length;i++){
		Color c = colors[i];
		int color = 0xFF000000 | ((int)(c.getBlue()*255)) | ((int)(c.getRed()*255)<<16) | ((int)(c.getGreen()*255)<<8);
		argb[i] = color;
	    }
	    return argb;
	}
	
	
	private void writeObject(java.io.ObjectOutputStream strm) throws java.io.IOException{
	    strm.writeShort(colors.length);
	    for(int i = 0;i<colors.length;i++){
		strm.writeShort(colorToGBA(colors[i]));
	    }
	}
	
	private void readObject(java.io.ObjectInputStream strm) throws java.io.IOException{
	    short size = strm.readShort();
	    colors = new Color[size];
	    for(int i = 0;i<colors.length;i++){
		colors[i] = GBAtoRGB(strm.readShort());
	    }
	}
	
	
        public static Palette monochromePal(int size){
            Palette p = new Palette(size);
            for(int i = 0;i<size;i++){
                p.colors[i] = Color.rgb(i*256/size, i*256/size, i*256/size);
            }
            return p;
        }
        
	@Override
	public boolean equals(Object other){
	    if(other instanceof Palette){
		Palette other_ = (Palette)other;
		if(colors.length != other_.colors.length){
		    return false;
		}
		
		//return colors.equals(other_.colors);
		
		for(int i = 0;i<colors.length;i++){
		    if(colorToGBA(colors[i]) != colorToGBA(other_.colors[i])){
			return false;
		    }
		    
		}
		
		return true;

	    }
	    return false;
	}
}
