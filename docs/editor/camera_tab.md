# Camera-tab
![Camera-tab Screenshot](https://gitlab.com/Fexean/editor-of-swing/-/raw/master/docs/editor/screenshots/cameratab_screenshot.png "Camera-tab Screenshot")  

In this tab you can modify the boundaries of the camera.  
You can do so either by modifying the values in the boxes on the right or by dragging the red camera rectangle on the map.  
The editor uses the camera boundary size to figure out the size of the map so if you shrink the camera area and reload the map, the map will become smaller.
