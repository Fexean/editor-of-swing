# Object
"Object" is a very general data structure. It can be used to:

- Run code every frame
- Show a graphic at a specific location on the screen (e.g. title screen logo)
- Run code every frame and display a graphic (e.g. level entities)


There are multiple object arrays in RAM:

|Address |Size (in objects)       | Used for
|:----------|------------:|----------|
|0x2012000|16| General purpose(?)
|0x2014000|112| General purpose(?)
|0x2023800|32| General purpose(?)
|0x2027800|68| General purpose(?)
|0x2030000|96| General purpose(?)

The game uses different functions for updating each object array. These functions are called in updateObjects (0x85C4A00), which runs every frame in the mainloop.

## Structure
The object struct is 0x200 bytes long. Most of that data doesn't seem to have a set meaning and different objects can use different structures. 

|Offset|Size|name|info|
|:--------|----|-------|-----|
|0x0|4|callback|function pointer, called every frame if non-zero, pointer to object is passed as a parameter in R11
|0x8|1|stateId| [General purpose] used in entity functions to keep track of the object's current state
|0x9|1|substateId| [General purpose] used in entity functions to keep track of the object's current state within a *stateId*
|0x10|4|x32| 32-bit x coordinate
|0x14|4|y32| 32-bit y coordinate
|0x20|1|spriteMode| selects how the object is drawn
|0x22|1|drawOrder| selects the order in which player part sprites are drawn
|0x28|2|scaleX|
|0x2A|2|scaleY|
|0x50|2|graphic_id|this is the id of the object's sprite
|0x52|1|palette|sprite palette number (in palram)
|0x54|6|affine parameters| this is used as the source for ObjAffineSet, (scaleX & scaleY get copied to here???)
|0x58|2|sprite rotation (part of above)
|0x7C|1|hitbox_xOffset| [General purpose?] added to the object's x-coordinate to get the entity's hitbox's x-coordinate
|0x7D|1|hitbox_yOffset| [General purpose?] added to the object's y-coordinate to get the entity's hitbox's y-coordinate
|0x7E|1|hitbox_halfwidth| [General purpose?] the width is actually double this value
|0x7F|1|hitbox_halfheight| [General purpose?] the height is actually double this value
|0x80|4|ent_param| [General purpose] entity's parameter is copied here
