
package editorofswing.tools;
import editorofswing.types.*;
import editorofswing.util.*;
import editorofswing.util.RomConstants;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.event.*;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class TimeAttackEditorUi {
    final int WORLD_COUNT = 5;
    final int LEVELS_IN_WORLD = 4;
    
    short worldSpriteIds[] = new short[WORLD_COUNT];
    short levelSpriteIds[] = new short[WORLD_COUNT * LEVELS_IN_WORLD];
    short levelIds[] = new short[WORLD_COUNT * LEVELS_IN_WORLD];
    int levelTimes[][] = new int[2 * WORLD_COUNT * LEVELS_IN_WORLD][];
    
    TextField levelSpriteFields[] = {new TextField(), new TextField(), new TextField(), new TextField()};
    TextField levelTimeFields[][] = new TextField[LEVELS_IN_WORLD][];
    TextField levelIdFields[] = {new TextField(), new TextField(), new TextField(), new TextField()};
    TextField worldSprField = new TextField();
    CheckBox diddyMode = new CheckBox("Diddy mode");
    Spinner worldSelector = new Spinner(1, WORLD_COUNT, 1);
    Scene scene;
    Rom rom;
    
    
    public TimeAttackEditorUi(Rom rom){
	VBox root = new VBox();
	setupUi(root);
	scene = new Scene(root, 620, 320);
	this.rom = rom;
	loadDataFromRom();
	setFieldsFromData(0, false);
    }
    
    private void setupUi(VBox root){
	root.setSpacing(10);
	root.setAlignment(Pos.CENTER);
	
	HBox topbox = new HBox();
	topbox.getChildren().add(new Label("World:"));
	topbox.getChildren().add(worldSelector);
	topbox.getChildren().add(new Label("World name:"));
	topbox.getChildren().add(worldSprField);
	topbox.setSpacing(4);
	topbox.setAlignment(Pos.CENTER);
	root.getChildren().add(topbox);
	
	GridPane levelgrid = new GridPane();
	levelgrid.setHgap(10);
	levelgrid.setVgap(20);
	levelgrid.setAlignment(Pos.CENTER);
	for(int i = 1; i<=LEVELS_IN_WORLD;i++){
	    levelgrid.add(new Label("Level "+i), 0, i);
	    levelgrid.add(new Label("Level ID:"), 1, i);
	    levelgrid.add(levelIdFields[i-1], 2, i);
	    
	    levelgrid.add(new Label("Name sprite:"), 3, i);
	    levelgrid.add(levelSpriteFields[i-1], 4, i);
	    levelgrid.add(new Label("Times:"), 5, i);
	    
	    levelTimeFields[i-1] = new TextField[4];
	    for(int j = 0;j<4;j++){
		TextField timefield = new TextField();
		timefield.setPrefWidth(55);
		levelgrid.add(timefield, 6+j, i, 1, 1);
		levelTimeFields[i-1][j] = timefield;
	    }
	    levelIdFields[i-1].setPrefWidth(40);
	    levelSpriteFields[i-1].setPrefWidth(60);
	}
	root.getChildren().add(levelgrid);

	worldSprField.setPrefWidth(60);
	worldSelector.setPrefWidth(60);
	
	root.getChildren().add(diddyMode);
	
	Button saveButton = new Button("Save changes to ROM");
	root.getChildren().add(saveButton);
	
	worldSelector.valueProperty().addListener((a,prevValue,value) -> {    
	    boolean diddy = diddyMode.selectedProperty().get();
	    setDataFromFields((Integer)prevValue-1, diddy);
	    setFieldsFromData((Integer)value-1, diddy);
	});
	diddyMode.selectedProperty().addListener(e ->  {
	    int world = (Integer)worldSelector.valueProperty().get() - 1;
	    setDataFromFields(world, !diddyMode.selectedProperty().get());
	    setFieldsFromData(world, diddyMode.selectedProperty().get());
	});
	
	saveButton.setOnAction(e->{
	    boolean diddy = diddyMode.selectedProperty().get();
	    int world = (Integer)worldSelector.valueProperty().get() - 1;
	    setDataFromFields(world, diddy);
	    saveDataToRom();
	});
    }
    
    
    public static void openTool(Rom rom){
	TimeAttackEditorUi ui = new TimeAttackEditorUi(rom);
	ui.showAndWait();
    }
    
    public void showAndWait(){
	Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("Time Attack Editor");
	stage.setScene(scene);
        stage.showAndWait();
    }
    
    
    //expects time in format XX:YY.ZZ
    private int timeStringToFrames(String str){
	try{
	    int minutes = Integer.min(9, Integer.parseInt(str.substring(0, str.indexOf(':'))));
	    int seconds = Integer.min(59, Integer.parseInt(str.substring(str.indexOf(':')+1, str.indexOf('.'))));
	    int milliseconds = 10*Integer.min(99, Integer.parseInt(str.substring(1 + str.indexOf('.')).substring(0, 2)));
	    return (60000*minutes + 1000*seconds + milliseconds)*60/1000;
	}catch(Exception e){
	    return -1;
	}
    }
    
    private String framesToTimeString(int frames){
	int minutes = Integer.min(frames / (60*60), 9);
	frames = frames % (60*60);
	int seconds = frames / 60;
	frames = frames % (60);
	int millis = frames * 1000 / 600;
	return String.format("%01d:%02d.%02d", minutes, seconds, millis);
    }
    
    
    private void setFieldsFromData(int world, boolean diddy){
	worldSprField.setText(String.format("0x%04X", worldSpriteIds[world]));
	
	for(int j = 0;j<LEVELS_IN_WORLD;j++){
	    levelSpriteFields[j].setText(String.format("0x%04X", levelSpriteIds[LEVELS_IN_WORLD*world + j]));
	    levelIdFields[j].setText(String.format("0x%02X", levelIds[LEVELS_IN_WORLD*world + j]));
	    
	    for(int time = 0;time<4;time++){
		if(diddy){
		    levelTimeFields[j][time].setText(framesToTimeString(levelTimes[LEVELS_IN_WORLD*world+j + LEVELS_IN_WORLD*WORLD_COUNT][time]));
		}else{
		    levelTimeFields[j][time].setText(framesToTimeString(levelTimes[LEVELS_IN_WORLD*world+j][time]));
		}
	    }
	}
    }
    
    private void setDataFromFields(int world, boolean diddy){
	worldSpriteIds[world] = (short)StringUtil.hexStrToInt(worldSprField.getText());
	
	for(int j = 0;j<LEVELS_IN_WORLD;j++){
	    levelSpriteIds[LEVELS_IN_WORLD*world + j] =(short)StringUtil.hexStrToInt(levelSpriteFields[j].getText());
	    levelIds[LEVELS_IN_WORLD*world + j] =(short)StringUtil.hexStrToInt(levelIdFields[j].getText());
	    
	    for(int time = 0;time<4;time++){
		if(diddy){
		    levelTimes[LEVELS_IN_WORLD*world+j + LEVELS_IN_WORLD*WORLD_COUNT][time] = timeStringToFrames(levelTimeFields[j][time].getText());
		}else{
		    levelTimes[LEVELS_IN_WORLD*world+j][time] = timeStringToFrames(levelTimeFields[j][time].getText());
		}
	    }
	}
    }
    
    private void loadDataFromRom(){
	for(int i = 0;i<WORLD_COUNT;i++){
	    worldSpriteIds[i] = rom.read16(RomConstants.TimeAttackWorldNames + i*2);
	    
	    for(int j = 0;j<LEVELS_IN_WORLD;j++){
		levelSpriteIds[LEVELS_IN_WORLD*i + j] = rom.read16(RomConstants.TimeAttackLevelNames + 8*i + 2*j);
		levelIds[LEVELS_IN_WORLD*i + j] = rom.read8(RomConstants.TimeAttackLevelIds + 4*i + j);
		
		levelTimes[LEVELS_IN_WORLD*i+j] = new int[4];
		levelTimes[LEVELS_IN_WORLD*i+j + LEVELS_IN_WORLD*WORLD_COUNT] = new int[4];
		for(int time = 0;time<4;time++){
		    //dk times
		    levelTimes[LEVELS_IN_WORLD*i+j][time] = rom.read32(RomConstants.emptySaveFile + 0x304 + 0x18 * (j + 4 * i) +  4*time);
		    
		    //diddy times
		    levelTimes[LEVELS_IN_WORLD*i+j + LEVELS_IN_WORLD*WORLD_COUNT][time] = rom.read32(RomConstants.emptySaveFile + 0x304 + 0x240 + 0x18 * (j + 4 * i) +  4*time);
		}
	    }
	}
    }
    
    private void saveDataToRom(){
	for(int i = 0;i<WORLD_COUNT;i++){
	    rom.write16(RomConstants.TimeAttackWorldNames + i*2, worldSpriteIds[i]);
	    
	    for(int j = 0;j<LEVELS_IN_WORLD;j++){
		rom.write16(RomConstants.TimeAttackLevelNames + 8*i + 2*j, levelSpriteIds[LEVELS_IN_WORLD*i + j]);
		rom.write8(RomConstants.TimeAttackLevelIds + 4*i + j, (byte)levelIds[LEVELS_IN_WORLD*i + j]);

		for(int time = 0;time<4;time++){
		    rom.write32(RomConstants.emptySaveFile + 0x304 + 0x18 * (j + 4 * i) +  4*time, levelTimes[LEVELS_IN_WORLD*i+j][time]);
		    rom.write32(RomConstants.emptySaveFile + 0x304 + 0x240 + 0x18 * (j + 4 * i) +  4*time, levelTimes[LEVELS_IN_WORLD*i+j + LEVELS_IN_WORLD*WORLD_COUNT][time]);
		}
	    }
	}
	
	try{
	    rom.save();
	}catch(Exception e){
	    new Alert(Alert.AlertType.ERROR, "Failed to save ROM!").showAndWait();
	}
    }
}
