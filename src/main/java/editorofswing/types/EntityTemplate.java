
package editorofswing.types;

public class EntityTemplate {
    
    public String name;
    public int callback;
    public int objectTable;
    public int category;
    
    public byte paletteSlot;	//-1 if undefined
    public byte spriteType; //-1=undefined, 0=regular, 1=affine, 2=player
    public int graphicId;   //graphic id for above sprite table
    
    public EntityTemplate(String line) throws Exception{
	name = line.substring(0, line.indexOf('='));
	String p[] = line.substring(line.indexOf('=')+1).split(";");
	callback = Integer.parseInt(p[0], 16);

	objectTable = parseArrayInt(p, 1, -1);
	category = parseArrayInt(p, 2, 6);
	paletteSlot = (byte)parseArrayInt(p, 3, -1);
	spriteType = (byte)parseArrayInt(p, 4, -1);
	graphicId = parseArrayInt(p, 5, -1);
    }
    
    private int parseArrayInt(String p[], int index, int defaultValue){
	if(index < p.length){
	    return Integer.parseInt(p[index], 10);
	}else{
	    return defaultValue;
	}
    }
    
    @Override
    public String toString(){
	return name + "=" + String.format("%07X;%d;%d;%d;%d;%d", callback, objectTable, category, paletteSlot, spriteType, graphicId);
    }
}
