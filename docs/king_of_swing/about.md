# About these documents
These are my reverse-engineering notes for the European version of DK: King of Swing.
They are mostly targeted towards more experienced romhackers who want to understand the inner workings of the game, but some parts like the entity documentation is useful for anyone looking to hack the game.   
If you spot a mistake or have something to add, feel free to create a merge request.  

# Notation

## Hex
Numbers prefixed with '0x' are hexadecimal numbers. Numbers without '0x' should always be decimal numbers (unless I've made a mistake).

## Memory addresses
All memory addresses are presented within the GBA memory map (addresses in the 0x8000000s refer to rom, addresses in the 0x2000000s refer to ewram, etc.)

## Variable types
I often describe variable types with these names:  
u8 = unsigned 8-bit integer  
u16 = unsigned 16-bit integer  
u32 = unsigned 32-bit integer  
s8 = signed 8-bit integer  
s16 = signed 16-bit integer  
s32 = signed 32-bit integer  
bool8 = 8-bit boolean value (set to 0 or 1)  
