
package editorofswing.types.sprites;

import java.util.ArrayList;
import editorofswing.util.Rom;
import editorofswing.util.RomConstants;

public class RegularSprite {
    public ArrayList<SubSprite> subsprites;
    
    public RegularSprite(Rom rom, int templateAddr) throws Exception{
	if(!Rom.isRomPointer(templateAddr)){
	    throw new Exception(String.format("Invalid SpriteTemplate pointer: %08X", templateAddr));
	}
	int subspritecount = rom.read16(templateAddr);
	if(subspritecount <= 0 || subspritecount > 128){
	    throw new Exception(String.format("Invalid subspritecount: %d", subspritecount));
	}
	templateAddr+=2;
	
	subsprites = new ArrayList();
	for(int i = 0;i<subspritecount;i++){
	    SubSprite sub = new SubSprite(rom, templateAddr);
	    subsprites.add(sub);
	    templateAddr += sub.getSize();
	}
    }
    
    public void writeToRom(Rom rom, int addr){
        rom.write16(addr, (short)subsprites.size());
        for(int i = 0;i<subsprites.size();i++){
	    SubSprite sub = subsprites.get(i);
            sub.writeToRom(rom, addr+2);
	    addr += sub.getSize();
	}
    }
    
    public static RegularSprite fromGraphicId(Rom rom, int id) throws Exception{
	int template = rom.read32(RomConstants.getListValue(RomConstants.Table.REGULAR_SPR_TEMPLATE, id));
	return new RegularSprite(rom, template);
    }
    
    public int getWidth(){
	int minX = Integer.MAX_VALUE;
	int maxX = Integer.MIN_VALUE;
	for(SubSprite sub : subsprites){
	    minX = Math.min(minX, sub.xOffset);
	    maxX = Math.max(maxX, sub.xOffset + sub.getWidth());
	}
	return maxX-minX;
    }
    

    public int getHeight(){
	int minY = Integer.MAX_VALUE;
	int maxY = Integer.MIN_VALUE;
	for(SubSprite sub : subsprites){
	    minY = Math.min(minY, sub.yOffset);
	    maxY = Math.max(maxY, sub.yOffset + sub.getHeight());
	}
	return maxY-minY;
    }
}
