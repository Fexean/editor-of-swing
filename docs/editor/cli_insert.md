# Command Line Interface
Editor of Swing contains a command line interface with various functionality.  
It can be invoked by running Editor of Swing from the command line with a specific command as the first argument.  
All available commands are detailed below.  


# Map Inserter (insert)

This tool allows you to batch insert exported map files to a rom.


## Usage
    USAGE: editorofswing insert <romfile> [options] [maps]
    Options:
    -r		don't fully repoint the new levels
    -m		remove unused tiles from tilesets to save space
    -l=<value>	set language, only for EU roms, defaults to 0 (english)
    
    Format for [maps]:
    <level ID>=<map filename>
    For example: 1A=mymapfile.map would replace level 1A with mymapfile.map
    You can specify multiple maps in this format, separated by spaces.

## Example
If you wanted to replace maps 0x19 (Banana Bungalow 1) and 0x1A (Banana Bungalow 2) with maps in files 1.map and 2.map, you could do it with the command:  
`editorofswing insert kingofswing.gba 19=1.map 1A=2.map`


# Map Deleter (delete-maps)
**EXPERIMENTAL**  
This tool goes through all maps and deletes the following data: palettes, tilesets, collisionmaps, tilemaps, climbmaps, entities, climb collisions.  


## Usage
    USAGE: editorofswing delete-maps <romfile>



# Bg Struct Deleter (delete-bgstructs)
**EXPERIMENTAL**  
This tool deletes Bg structs (level tilesets & tilemaps) for all languages but english and repoints the other languages to use the english tilesets and tilemaps.  


## Usage
    USAGE: editorofswing delete-bgstructs <romfile>



# Sprite inserter (spr-insert)
Inserts PNG images to the game as sprites. The images must be indexed 16 color images.

## Usage
    USAGE: editorofswing spr-insert <romfile> [options] [sprites]
    
    Options:
    --affine                insert sprite into affine table (default)
    --player                insert sprite into player table
    --regular               insert sprite into regular table
    -tile=<id>              insert tiles starting at index id in sprite tileset
    
    Format for [sprites]:
    <Graphic ID>=<png filename>
    For example: 1A=spr.png would replace graphic 0x1A with spr.png
    You can specify multiple sprites in this format, separated by spaces.
    If -tile is not specified, the first empty tile will be chosen.



# Sprite Dumper (spr-dump)
Saves sprites as PNG images.

## Usage
    USAGE: editorofswing spr-dump <romfile> [options] <range start> <range end>
    
    Options:
    --affine                dump sprites from affine table (default)
    --player                dump sprites from player table
    --regular               dump sprites from regular table
    --dir=<dir>             store dumped sprite in <dir> directory
    
    <range start> and <range end> should be hexadecimal numbers for dumped sprite id range.


## Example
`editorofswing spr-dump kingofswing.gba --player --dir=sprites 40 6A` - Dumps sprites from the player table to a folder called "sprites" (must already exist). Sprites with IDs between 0x40 and 0x6A will be dumped.


# Sprite Tile Repointer (spr-repoint)
Repoints sprite tilesets to the end of the ROM and adds empty tiles to them so that new sprites can be inserted without overwriting existing tiles.

## Usage
    USAGE: editorofswing spr-repoint <romfile> [newCounts] [oldCounts]
    
    Options:
    [newCounts] and [oldCounts] should be colon separated lists of hexadecimals with no spaces (example:1AC,200,200).
    Tile count values should be in order: affine, regular, player.
    if not given, newCounts will default to maximum (0x10000) and oldCounts will default to vanilla ROM values.

## Examples
`editorofswing spr-repoint kingofswing.gba` - makes all 3 tilesets have room for 0x10000 tiles

`editorofswing spr-repoint kingofswing.gba 6000,6A00,5000` - makes affine have 0x6000 tiles, regular 0x6A00 tiles and player 0x5000 tiles



