
package editorofswing.ui.dialog;

import javafx.stage.FileChooser;
import javafx.stage.Window;
import java.io.File;
import editorofswing.util.Rom;

public class FileDialog {
    
    private static String lastDir = ".";
    
    public static Rom openROM(Window window){
	   File chosen = openFile(window, "Open ROM", "GBA ROMs", "*.gba");
            if(chosen != null && chosen.exists() && chosen.canWrite() && chosen.canRead()){
                try{
                    return new Rom(chosen.getAbsolutePath());
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        return null;  
    }
    
    public static File openFile(Window window, String windowName, String type, String extension){
	FileChooser fileChooser = new FileChooser();
	fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(type, extension));
	fileChooser.setTitle(windowName);
	fileChooser.setInitialDirectory(new File(lastDir));
	File result = fileChooser.showOpenDialog(window);
	if(result != null){
	    lastDir = result.getParent();
	}
	return result;
    }
    
    
    public static File saveAs(Window window, String windowName, String type, String extension, String initialName){
	FileChooser fileChooser = new FileChooser();
	fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(type, extension));
	fileChooser.setTitle(windowName);
	fileChooser.setInitialDirectory(new File(lastDir));
	fileChooser.setInitialFileName(initialName);
	File result =  fileChooser.showSaveDialog(window);
	if(result != null){
	    lastDir = result.getParent();
	}
	return result;
    }
    
}
