
package editorofswing.mainwindow;

import editorofswing.rendering.MapRenderer;
import editorofswing.tools.*;
import editorofswing.tools.statEditor.StatEditorUi;
import editorofswing.types.Map;
import editorofswing.ui.dialog.ConfirmationDialog;
import editorofswing.ui.dialog.FileDialog;
import editorofswing.ui.dialog.RepointDialog;
import editorofswing.util.ConfigReader;
import editorofswing.util.LevelInserter;
import editorofswing.util.MapEditor;
import editorofswing.util.Rom;
import editorofswing.util.RomConstants;
import editorofswing.util.RomMisc;
import java.util.HashMap;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


public class MainWindow{
        
    boolean romLoaded = false;
    boolean mapOpen = false;
    boolean autoRepoint = true;
    boolean askBeforeDiscard = true;
    boolean drawEntities = true;
    
    MapRenderer renderer = new MapRenderer();
    Rom rom;
    Map currentMap;
    MapEditor editor = new MapEditor();;
    Canvas mapcanvas = new Canvas();
    MenuBar menubar;
    BorderPane root = new BorderPane();
    TabPane tabpane;
    ListView levelList;
    
    Menu toolsMenu;
    Menu menu2Language;
    MenuItem menu1SaveMapRom;
    
    Label romLabel = new Label();
    Label lvlLabel = new Label();
    
    HashMap<String, Integer> levelNames;
    
    CollisionTab coltab;
    MapTab maptab;
    EntityTab enttab;
    ClimbingTab climbTab;
    CameraTab camtab;
    OtherTab othertab;
    int currentLvlId = -1;
    int language = 0;
    
    Stage primaryStage;
    Scene scene;
    

    public void setupUI(Stage primaryStage) {
	this.primaryStage = primaryStage;
	
	levelNames = ConfigReader.readHexSymbols("levelnames.cfg");
	levelList = new ListView<String>(FXCollections.observableArrayList(ConfigReader.getKeys("levelnames.cfg")));
	levelList.setPrefWidth(180);
	VBox leftbox = new VBox();
	
	romLabel.setPadding(new Insets(20, 10, 10, 10) );
	lvlLabel.setPadding(new Insets(0, 10, 10, 10) );
	levelList.setPrefHeight(480);
	leftbox.getChildren().addAll(new ScrollPane(levelList), romLabel, lvlLabel);
	root.setLeft(leftbox);
	updateLabels();
	
	tabpane = new TabPane();
	tabpane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);
	
	root.setCenter(tabpane);
	
	createMenuBar();
	root.setTop(menubar);
	
	maptab = new MapTab();
	renderer.setCanvas(maptab.getCanvas());
	tabpane.getTabs().add(maptab);	
	
	coltab = new CollisionTab();
	tabpane.getTabs().add(coltab);
	
	climbTab = new ClimbingTab();
	tabpane.getTabs().add(climbTab);
	
	enttab = new EntityTab();
	tabpane.getTabs().add(enttab);
	
	camtab = new CameraTab();
	tabpane.getTabs().add(camtab);
	
	othertab = new OtherTab();
	othertab.setOnResizeClicked(e -> onMapResize(othertab.getMapDimensions()));
	
	tabpane.getTabs().add(othertab);
	tabpane.getSelectionModel().selectedItemProperty().addListener(
		new ChangeListener<Tab>(){
		    @Override
		    public void changed(ObservableValue<? extends Tab> ov, Tab t, Tab t1){
			onTabChange();
		    }
		}
	);
	tabpane.setDisable(true);
	levelList.setDisable(true);
	
	scene = new Scene(root, 780, 640);
	primaryStage.setTitle("Editor of Swing - 1.1");
	primaryStage.setScene(scene);
	primaryStage.show();
	
	levelList.getSelectionModel().selectedItemProperty().addListener(
		new ChangeListener<String>(){
		    @Override
		    public void changed(ObservableValue<? extends String> ov, String t, String t1){
			onLevelListSelection(t);
		    }
		}
	);
	
	scene.setOnKeyPressed(e -> keypressHandler(e));
	scene.setOnKeyReleased(e -> keypressHandler(e));
    }
    
    private void createMenuBar(){
	menubar = new MenuBar();
	menubar.getMenus().addAll(
		createFileMenu(),
		createOptionsMenu(),
		createToolsMenu()
	);
    }
    
    private Menu createFileMenu(){
	Menu menu = new Menu("File");
	MenuItem menu1OpenRom = new MenuItem("Open ROM...");
	menu1SaveMapRom = new MenuItem("Save map to ROM (Overwrite)");
	MenuItem menu1SaveMapRomRepoint = new MenuItem("Save map to ROM (Repointed)");
	MenuItem menu1SaveMap = new MenuItem("Save map as...");
	MenuItem menu1ImportMap = new MenuItem("Import map...");
	MenuItem menu1Undo = new MenuItem("Undo");
	MenuItem menu1Redo = new MenuItem("Redo");
	menu.getItems().addAll(menu1OpenRom, menu1SaveMapRom, menu1SaveMapRomRepoint, menu1SaveMap, menu1ImportMap, menu1Undo, menu1Redo);
	
	menu1OpenRom.setOnAction(e -> openRom());
	menu1SaveMapRom.setOnAction(e -> saveMapToRom(false));
	menu1SaveMapRomRepoint.setOnAction(e -> saveMapToRom(true));
	menu1SaveMap.setOnAction(e -> saveMap());
	menu1ImportMap.setOnAction(e -> importMap());
	menu1Undo.setOnAction(e->{editor.undo(); redrawTabs();});
	menu1Redo.setOnAction(e->{editor.redo(); redrawTabs();});
	
	menu1OpenRom.setAccelerator(KeyCombination.keyCombination("CTRL+O"));
	menu1SaveMapRom.setAccelerator(KeyCombination.keyCombination("CTRL+S"));
	menu1Undo.setAccelerator(KeyCombination.keyCombination("CTRL+Z"));
	menu1Redo.setAccelerator(KeyCombination.keyCombination("CTRL+Y"));
	
	menu.setOnShown(e->{
	    menu1Undo.setDisable(!editor.getUndoStack().canUndo());
	    menu1Redo.setDisable(!editor.getUndoStack().canRedo());
	    menu1SaveMapRom.setDisable(rom == null || currentLvlId == -1);
	    menu1SaveMapRomRepoint.setDisable(rom == null || currentLvlId == -1);
	    menu1SaveMap.setDisable(currentMap == null);
	});
	
	menu.setOnHidden(e->{
	    //Enable undo & redo to always allow the use of hotkeys
	    menu1Undo.setDisable(false);
	    menu1Redo.setDisable(false);
	});
	return menu;
    }
    
    private Menu createOptionsMenu(){
	Menu menu2 = new Menu("Options");
	RadioMenuItem menu2AutoRepoint = new RadioMenuItem("Auto-Repoint");
	menu2AutoRepoint.setSelected(autoRepoint);
	
	RadioMenuItem menu2AutoDiscard = new RadioMenuItem("Ask before discarding changes");
	menu2AutoDiscard.setSelected(askBeforeDiscard);
	
	RadioMenuItem menu2DrawBg = new RadioMenuItem("Draw level backgrounds");
	menu2DrawBg.setSelected(true);
	
	RadioMenuItem menu2DrawEnts = new RadioMenuItem("Draw entity sprites");
	menu2DrawEnts.setSelected(true);
	
	menu2AutoRepoint.setOnAction(e -> autoRepoint = menu2AutoRepoint.selectedProperty().get());
	menu2AutoDiscard.setOnAction(e -> askBeforeDiscard = menu2AutoDiscard.selectedProperty().get());
	menu2DrawBg.setOnAction(e->{
	    renderer.setMapBackgroundDrawing(menu2DrawBg.selectedProperty().get());
	    redrawTabs();
	});
	
	menu2DrawEnts.setOnAction(e->{
	    drawEntities = menu2DrawEnts.selectedProperty().get();
	    updateEntTabSpriteRendering();
	    redrawTabs();
	});
	
	menu2Language = new Menu("ROM Language");
	ToggleGroup langGroup = new ToggleGroup();
	RadioMenuItem languageItemEnglish = createLanguageMenuItem("English", langGroup, 0);
	languageItemEnglish.selectedProperty().set(true);
	menu2Language.getItems().addAll(
		languageItemEnglish,
		createLanguageMenuItem("French", langGroup, 1),
		createLanguageMenuItem("German", langGroup, 2),
		createLanguageMenuItem("Italian", langGroup, 3),
		createLanguageMenuItem("Spanish", langGroup, 4)
	);
	menu2Language.setDisable(true);
	
	menu2.getItems().addAll(menu2AutoRepoint, menu2AutoDiscard, menu2DrawBg, menu2DrawEnts, menu2Language);
	return menu2;
    }
    
    
    private Menu createToolsMenu(){
	toolsMenu = new Menu("Tools");
	toolsMenu.setDisable(true);
	
	MenuItem timeAttack = new MenuItem("Time attack editor");
	timeAttack.setOnAction(e->{
	    TimeAttackEditorUi.openTool(rom);
	});
	
	MenuItem spriteViewerTool = new MenuItem("Sprite viewer");
	spriteViewerTool.setOnAction(e->{
	    SpriteViewerUi.openTool(rom);
	});
	
	MenuItem statEditorTool = new MenuItem("Character stat editor");
	statEditorTool.setOnAction(e -> StatEditorUi.openTool(rom));
	
	toolsMenu.getItems().addAll(timeAttack, spriteViewerTool, statEditorTool);
	return toolsMenu;
    }
    
    private RadioMenuItem createLanguageMenuItem(String name, ToggleGroup group, int value){
	RadioMenuItem m = new RadioMenuItem(name);
	m.setToggleGroup(group);
	m.setOnAction(e-> language = value);
	return m;
    }
    
    private void saveMap(){
	tabpane.requestFocus();
	java.io.File mapfile = FileDialog.saveAs(primaryStage, "Save map", "*.map", "*.map", String.format("%02X.map", currentLvlId));
	if (mapfile != null) {
	    try{
		currentMap.saveToFile(mapfile);
	    }catch(Exception e){
		new Alert(Alert.AlertType.ERROR, "Failed to save map: " + e.getMessage()).showAndWait();
	    }
	    editor.setModified(false);
	}
    }
    
    private void openRom(){
	Rom selectedRom = FileDialog.openROM(primaryStage);
	if(selectedRom != null){
	    if(!selectedRom.isVersionSupported()){ //TODO: Make offsets for other versions
		new Alert(Alert.AlertType.ERROR, "Unsupported ROM version!").showAndWait();
		return;
	    }
	    rom = selectedRom;
	    RomConstants.initOffsetsForRom(rom);
	    enttab.loadEntityTemplates(rom.getVersion());
	    updateEntTabSpriteRendering();
	    levelList.setDisable(false);
	    romLoaded = true;
	    if(rom.getVersion() == Rom.Version.EU){
		menu2Language.setDisable(false);
	    }else{
		menu2Language.setDisable(true);
	    }
	    toolsMenu.setDisable(false);
	}
	updateLabels();
    }
    
    
    
    
    private void saveMapToRom(boolean fullRepoint){
	tabpane.requestFocus();
	int freespace;
	final int spaceNeeded = LevelInserter.getFreespaceNeeded(rom, currentMap, currentLvlId, language, fullRepoint) + 3; //+3 for alignment
	if(autoRepoint || spaceNeeded == 3){
	    freespace = RomMisc.align4(RomMisc.findFreeSpace(rom, RomConstants.freespace, spaceNeeded));
	}else{
	    freespace = RepointDialog.getRepointAddr(RomMisc.align4(RomMisc.findFreeSpace(rom, RomConstants.freespace, spaceNeeded)), spaceNeeded);
	}
	
	if (freespace == -1) {
	    new Alert(Alert.AlertType.INFORMATION, "Map not saved.").showAndWait();
	    return;
	}

	int usedbytes = LevelInserter.insertMap(rom, currentMap, currentLvlId, freespace, fullRepoint, language);
	System.out.println("[SaveMapRom] " + usedbytes + " bytes of freespace used");

	if (romLoaded) {
	    try {
		rom.save();
		editor.setModified(false);
	    } catch (Exception ex) {
		new Alert(Alert.AlertType.ERROR, "Failed to save ROM!").showAndWait();
	    }
	}
    }
    
    private void importMap(){
	java.io.File f = FileDialog.openFile(primaryStage, "Open map", "*.map", "*.map");
	if(f == null){return;}
	try{
	    currentMap = Map.loadFromFile(f);
	    updateUi();
	    enttab.loadEntityTemplates(currentMap.version);
	    if(!romLoaded || currentMap.version != rom.getVersion()){
                //don't load sprites from rom if there is no rom or it's the wrong version
                enttab.setRendererRom(null);
            }
	    if(romLoaded && currentMap.version != rom.getVersion()){
		new Alert(Alert.AlertType.WARNING, "Map is for a different rom version!\n\nMap version: "+currentMap.version+"\nRom version: " + rom.getVersion()).showAndWait();
	    }
	}catch(Exception e){
	    new Alert(Alert.AlertType.ERROR, "Failed to open map: "+e.getMessage()).showAndWait();
	}
    }
    
    private void onLevelListSelection(String previous){
	String level = (String)levelList.getSelectionModel().getSelectedItem();
	int levelId = levelNames.get(level);
	if(levelId != currentLvlId){
	    if(askBeforeDiscard && mapOpen && editor.isMapModified() && !ConfirmationDialog.getConfirmation("Unsaved changes", "The current map has unsaved changes.\nDiscard changes?")){
		javafx.application.Platform.runLater(() -> levelList.getSelectionModel().select(previous));
		return;
	    }
	    setupMap(levelId);
	}
    }
    
    private void updateEntTabSpriteRendering(){
	if(drawEntities){
	    enttab.setRendererRom(rom);
	}else{
	    enttab.setRendererRom(null);
	}
    }
    
    private void updateUi(){
	editor.setMap(currentMap);
	int w = currentMap.wChunks*256;
	int h = currentMap.hChunks*256;
	mapcanvas.setWidth(w);
	mapcanvas.setHeight(h);
	mapcanvas.getGraphicsContext2D().clearRect(0, 0, w, h);
	renderer.setCanvas(mapcanvas);
	renderer.drawMap(currentMap);
	tabpane.setDisable(false);
	onTabChange();	//Updates the current tab content
	mapOpen = true;
	updateLabels();
    }
    
    private void setupMap(int lvlIndex){
	currentMap = new Map(rom, lvlIndex, language);
	currentLvlId = lvlIndex;
	menu1SaveMapRom.setDisable(false);	//allow ctrl+s
	updateUi();
    }
    
    
    private void onTabChange(){
	setupTab(tabpane.getSelectionModel().getSelectedIndex());
    }
    
    private void onMapResize(javafx.util.Pair<Integer, Integer> dim){
	int w = dim.getKey();
	int h = dim.getValue();
	int wChunks = 1+(w-1)/256;
	int hChunks = 1+(h-1)/256;
	if(wChunks < 1 || hChunks < 1){
	    new Alert(Alert.AlertType.ERROR, "Map dimensions must be positive!").showAndWait();
	    return;
	}
	if(wChunks > 4 && hChunks > 1){
	    new Alert(Alert.AlertType.ERROR, "Map width can be larger than 1024 only if height is 256!").showAndWait();
	    return;
	}
	
	currentMap.resize(wChunks, hChunks);
	updateUi();
    }
    
    private void setupTab(int tab){
	switch(tab){
	    case 0:
		maptab.setup(editor, mapcanvas, renderer, primaryStage);
		maptab.drawBlockset();
		break;
	    case 1:
		coltab.setup(editor, mapcanvas);
	    break;
	    
	    case 2:
		climbTab.setup(editor, mapcanvas, scene);
	    break;
	    case 3:
		enttab.setup(editor, mapcanvas);
                enttab.clearRendererCache();
	    break;
	    case 4:
		camtab.setup(editor, mapcanvas, scene);
		break;
	    case 5:
		othertab.setup(editor, primaryStage);
	    break;
	}
	
	if(othertab.tilesetModified){
	    redrawTabs();
	    othertab.tilesetModified = false;
	}
    }
    
    
    private void redrawTabs(){
	renderer.drawMap(currentMap);

	switch(tabpane.getSelectionModel().getSelectedIndex()){
	    case 1:
		coltab.redraw();
	    break;
	    
	    case 2:
		climbTab.redrawClimbingAreas();
	    break;
	    case 3:
		enttab.redraw();
	    break;
	}
    }
    
    
    
    private void keypressHandler(KeyEvent e){
	final boolean pressed = e.getEventType() == KeyEvent.KEY_PRESSED;
	if(e.getCode() == javafx.scene.input.KeyCode.X){
	    maptab.setVerticalLock(pressed);
	    coltab.setVerticalLock(pressed);
	}else if(e.getCode() == javafx.scene.input.KeyCode.C){
	    maptab.setHorizontalLock(pressed);
	    coltab.setHorizontalLock(pressed);
	}
    }
    
    private void updateLabels(){
	if(!romLoaded){
	    romLabel.setText("Rom: None");
	}else{
	    romLabel.setText("Rom: " + rom.getVersion() + " Version");
	}
	
	if(currentLvlId == -1){
	    lvlLabel.setText("Level ID: None");
	}else{
	    lvlLabel.setText(String.format("Level ID: 0x%02X", currentLvlId));
	}
    }
    
}
