
package editorofswing.undo;

import editorofswing.undo.UndoCmd;
import editorofswing.types.Map;

public class TilePlacementCmd implements UndoCmd{
    
    private Map map;
    private int x, y;
    private short oldTile, newTile;
    private boolean merge;
    
    public TilePlacementCmd(Map map, int tx, int ty, short tiledata, boolean merge){
	this.map = map;
	this.merge = merge;
	newTile = tiledata;
	oldTile = map.layer1.getRaw(tx, ty);
	x = tx;
	y = ty;
    }
    
    @Override
    public void redo(){
	map.layer1.setRaw(x,y, newTile);
    }
    
    @Override
    public void undo(){
	map.layer1.setRaw(x,y, oldTile);
    }
    
    @Override
    public boolean shouldMerge(){
	return merge;
    }
}
