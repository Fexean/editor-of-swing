# Time attack editor
![screenshot](https://gitlab.com/Fexean/editor-of-swing/-/raw/master/docs/editor/screenshots/time_attack_editor.png "screenshot")  

This tools allows you to modify the time attack mode.
At the top you can select which world/page to edit. Next to that you can select the graphic_id of the sprite used to display that world's name.  
Below that there are fields for each of the 4 levels shown on that page.  
For each level you can change the level ID that is loaded when that level is selected, the graphic_id of the sprite used to display the level's name and the default high score times. The time 0:00.00 represents an empty slot.  
Times should be entered in the format X:YY:ZZ where X is the minutes, Y is the seconds and ZZ are the hundredth seconds. Internally the times are stored as a number of frames so the time you enter may change slightly due to rounding.  
At the bottom you can select whether you're editing the times for diddy mode and you can save your changes.