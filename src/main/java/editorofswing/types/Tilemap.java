
package editorofswing.types;

import editorofswing.util.Rom;


public class Tilemap implements java.io.Serializable, ChunkMap{
    private static final long serialVersionUID = 1L;
    
    public int width;
    public int height;
    public short[] map;
    
    
    public Tilemap(int w, int h){
        width = w;
        height = h;
        map = new short[w*h];
    }
    
    public Tilemap(Rom rom, int address, int w, int h){
        map = new short[w*h];
        width = w;
        height = h;
	
	for(int ty = 0;ty<h; ty+= 32){
	    for(int tx = 0;tx<w; tx+= 32){
		int chunkptr = rom.read32((address + (8*ty&0xFFFFFF00) / 16) + tx/32 * 4);
		
		if(!Rom.isRomPointer(chunkptr))
		    System.out.println(String.format("Chunkmap (%08X => %04X) has an invalid chunk pointer: %08X", address, ((address + (8*ty&0xFFFFFF00) / 16) + tx/32 * 4),  chunkptr));

		//for each entry in chunk
		for(int y = 0;y<32;y++){
		    for(int x = 0;x<32;x++){
			map[ty * w + tx + (y*w + x)] = rom.read16(chunkptr + 2*(y*32 + x));
		    }
		}
		
	    }
	}
    }
    

    public static short getRawValue(short tile, byte pal, boolean hFlip, boolean vFlip){
	int raw = tile | (pal << 12);
	if(hFlip){raw |= 1<<10;}
	if(vFlip){raw |= 1<<11;}
	return (short)raw;
    }
    
    public void fill(short value){
	java.util.Arrays.fill(map, value);
    }
    
    
    public void print(){
        System.out.println("Tilemap: "+width+"x"+height);
	for(int i = 0;i<width*height;i++){
            System.out.print(String.format("%02X %02X ",(map[i]&0xFF), (((map[i]&0xFF00)>>8)&0xFF)));
            if(i%width == 0 && i!=0){
                System.out.println("");
            }
        }
	System.out.println("");
    }
    
    
    public byte[] getChunk(int tx, int ty){
	tx&=~0x1F;
	ty&=~0x1F;
	byte[] chunk = new byte[32*32*2+4];
	chunk[0]=chunk[2]=32;
	chunk[1]=chunk[3]=0;
	for(int i = 0;i<32;i++){
	    for(int j = 0;j<32;j++){
		short data = getRaw(tx+j, ty+i);
		chunk[4+(32*i+j)*2+1] = (byte)((data>>8) & 0xFF);
		chunk[4+(32*i+j)*2] = (byte)(data & 0xFF);
	    }
	}
	return chunk;
    }
    
    @Override
    public void writeChunkToRom(Rom rom, int cx, int cy, int addr){
	if(cx*32 >= width || cy*32 >=height){
	    return;
	}
	
	for(int j = 0;j<32;j++){
	    for(int i = 0;i<32;i++){
		rom.write16(addr + i*2 + j*32*2, getRaw(cx*32+i, cy*32+j));
	    }
	}
    }
    
    public void setTile(int x, int y, short tile, byte pal, boolean hFlip, boolean vFlip){
	setRaw(x, y,  getRawValue(tile, pal, hFlip, vFlip));
    }
    
    public short getTileAt(int x, int y){
	if(x>=width || y>=height){
            (new Exception("[getTileAt] Out of bounds "+x+" "+y+" ("+width+"x"+height+")")).printStackTrace();
	    return 0;
        }
	short val = map[width*y + x];
        return (short)(val & 0x3FF);
    }
    
    
    public void setTileAt(int x, int y, short tile){
	if(x>=width || y>=height){
	    (new Exception("[setTileAt] Out of bounds")).printStackTrace();
            return;
        }
        map[width*y + x] = (short)(tile & 0x3FF);
    }
    
    public void setFlips(int x, int y, boolean hFlip, boolean vFlip){
        map[width*y+x] &= 0xF3FF;
        map[width*y+x] |= ((hFlip) ? 1<<10 : 0) | ((vFlip) ? 1<<11 : 0);
        if(x>=width || y>=height){
	    (new Exception("[setFlips] Out of bounds")).printStackTrace();
            return;
        }
    }
    
    public boolean getHFlip(int x, int y){
        if(x>=width || y>=height){
	    (new Exception("[getHFlip] Out of bounds "+x+" "+y+" ("+width+"x"+height+")")).printStackTrace();
            return false;
        }
        return ((map[width*y+x] & (1<<10)) != 0);
    }
    
    public boolean getVFlip(int x, int y){
        if(x>=width || y>=height){
            (new Exception("[getVFlip] Out of bounds "+ x+ " "+y+" ("+width+"x"+height+")")).printStackTrace();
            return false;
        }
        return ((map[width*y+x] & (1<<11)) != 0);
    }
    
    public void setPal(int x, int y, byte pal){
	if(x>=width || y>=height){
            return;
        }
        map[width*y+x] |= ((pal&0xF)<<12);
    }
    
    public byte getPal(int x, int y){
        if(x>=width || y>=height){
	    (new Exception("[getPal] Out of bounds "+ x+ " "+y+" ("+width+"x"+height+")")).printStackTrace();
	    return -1;
        }
        return (byte)((map[width*y+x] >> 12)&0xF);
    }
    
    
    public short getRaw(int x, int y){
	if(x>=width || y>=height){
            (new Exception("[Tilemap::getRaw] Out of bounds")).printStackTrace();
	    return 0;
        }
	return map[width*y+x];
    }
    
    public void setRaw(int x, int y, short value){
    if(x>=width || y>=height){
            (new Exception("[Tilemap::setRaw] Out of bounds")).printStackTrace();
	    return;
        }
	map[width*y+x] = value;
    }
    
    
    
    public void copyBlock(Tilemap src, int x, int y){
	for(int i = y;i<y+src.height;i++){
	    for(int j = x;j<x+src.width;j++){
		if(i < this.height && j<this.width){
		    this.setRaw(j,i, src.getRaw(j-x,i-y));
		}else{
		    return;
		}
	    }
	}
    }
    
    
    @Override
    public boolean isChunkEmpty(int cx, int cy){
	if(cx*32 >= width || cy*32 >=height){
	    return true;
	}
	for(int j = 0;j<32;j++){
	    for(int i = 0;i<32;i++){
		if(getRaw(cx*32+i, cy*32+j) != 0){
		    return false;
		}
	    }
	}
	return true;
    }
    
    @Override
    public boolean isChunkEmpty(int i){
	int cx = i % getWChunks();
	int cy = i / Math.max(getWChunks(), 4);
	return isChunkEmpty(cx, cy);
    }
    
    @Override
    public int getEmptyChunkCount(){
	int cw = width/32;
	int ch = height/32;
	int count = 0;
	for(int i = 0;i<cw;i++){
	    for(int j = 0;j<ch;j++){
		if(isChunkEmpty(i, j)){
		    count++;
		}
	    }
	}
	return count;
    }
    
    @Override
    public int getWChunks(){
	return width / 32;
    }
    
    @Override
    public int getHChunks(){
	return height / 32;
    }
    
    @Override
    public int getChunkSize(){
	return 2*32*32;
    }
    
    public int getSize(){
	return width*height*2;
    }
    
    public void resize(int w, int h){
	short newmap[] = new short[w*h];
	for(int y = 0;y<Math.min(h, height);y++){
	    System.arraycopy(map, y*width, newmap, y*w, Math.min(width, w));
	}
	map = newmap;
	width = w;
	height = h;
    }
}
