
package editorofswing.util;
import editorofswing.types.*;
import editorofswing.undo.*;

public class MapEditor {
    Map currentMap;
    UndoStack undostack;
    boolean mapModified;
    
    public MapEditor(Map map){
	setMap(map);
	undostack = new UndoStack(50);
    }
    
    public MapEditor(){
	setMap(null);
	undostack = new UndoStack(50);
    }
    
    public void setCollisionAt(int x, int y, byte col, boolean dragging){
	undostack.addCommand(new CollisionPlacementCmd(currentMap, x, y, col, dragging));
	mapModified = true;
    }
    
    public int getCollisionAt(int x, int y){
	return Byte.toUnsignedInt(currentMap.wallcol.getCollision(x, y));
    }
    
    public void setTileAt(int x, int y, short raw, boolean dragging){
	undostack.addCommand(new TilePlacementCmd(currentMap, x, y, raw, dragging));
	mapModified = true;
    }
    
    public void setTileAt(int x, int y, short tile, byte pal, boolean hflip, boolean vflip, boolean dragging){
	setTileAt(x, y, (short)(((hflip ? 1 : 0)<<10) | ((vflip ? 1 : 0)<<11) | (pal << 12) | tile), dragging);
    }
    
    public int getTileDataAt(int x, int y){
	return Short.toUnsignedInt(currentMap.layer1.getRaw(x, y));
    }
    
    public void setBlockAt(int x, int y, MapBlock block, boolean dragging){
	undostack.addCommand(new BlockPlacementCmd(this, x, y, block, dragging));
	mapModified = true;
    }
    
    public Entity getEntityAt(int x, int y){
	x += 8;
	y += 8;
	//Assumes entity is a 16x16 square
	for(Entity e : currentMap.entities){
	    if(x >= e.x && x < e.x + 16
	    && y >= e.y && y < e.y + 16){
		return e;
	    }
	}
	return null;
    }
    
    public ClimbCollision getClimbAreaAt(int x, int y){
	for(ClimbCollision col : currentMap.climbCol){
	    if(x >= col.x && x < col.x+col.w
	    && y >= col.y && y < col.y+col.h){
		return col;
	    }
	}
	return null;
    }
    
    
    public void floodFillCollision(int x, int y, byte value){
	int original = getCollisionAt(x, y);
	setCollisionAt(x, y, (byte)original, false); //command for the flood fill commands to merge with  
	collisionfloodFill(value, (byte)original, x, y);
    }
    
    
    private void collisionfloodFill(byte value, byte original, int x, int y){
	if(x < 0 || y <0 || x >= currentMap.wChunks*32 || y >= currentMap.hChunks*32){return;}
	if(original == value){return;}
	if(currentMap.wallcol.getCollision(x, y) != original){return;}
	
	setCollisionAt(x, y, value, true);
	collisionfloodFill(value, original, x-1,y);
	collisionfloodFill(value, original, x,y-1);
	collisionfloodFill(value, original, x+1,y);
	collisionfloodFill(value, original, x,y+1);
    }
    
    
    public void floodFillTile(int x, int y, short value){
	short original = currentMap.layer1.getRaw(x, y);
	setTileAt(x, y, original, false); //command for the flood fill commands to merge with  
	tilefloodFill(value, (byte)original, x, y);
    }
    
    
    private void tilefloodFill(short value, short original, int x, int y){
	if(x < 0 || y <0 || x >= currentMap.wChunks*32 || y >= currentMap.hChunks*32){
	    return;
	}
	if(original == value){return;}
	if(currentMap.layer1.getRaw(x, y) != original){return;}
	
	setTileAt(x, y, value, true);
	tilefloodFill(value, original, x-1,y);
	tilefloodFill(value, original, x,y-1);
	tilefloodFill(value, original, x+1,y);
	tilefloodFill(value, original, x,y+1);
    }
    
    
   
    public MapBlock copyMapBlock(int x1, int y1, int x2, int y2){
	int w = Math.abs(x1-x2);
	int h = Math.abs(y1-y2);
	int x = Math.min(x1, x2);
	int y = Math.min(y1, y2);
	
	if(x + w >= currentMap.wChunks*32){
	    w = currentMap.wChunks*32 - x;
	}
	
	if(y + h >= currentMap.hChunks*32){
	    h = currentMap.hChunks*32 - y;
	}
	
	MapBlock result = new MapBlock(w, h);
	
	
	
	for(int i = 0;i<w;i++){
	    for(int j = 0;j<h;j++){
		result.tmap.setRaw(i, j, currentMap.layer1.getRaw(i+x, j+y));
		result.cmap.setCollision(i, j, currentMap.wallcol.getCollision(i+x, j+y));
	    }
	}
	
	return result;
    }
    
    public void setCameraBorders(int top, int bottom, int left, int right){
	currentMap.cam_bot = bottom;
	currentMap.cam_top = top;
	currentMap.cam_left = left;
	currentMap.cam_right = right;
	mapModified = true;
    }
    
    
    public void setMap(Map map){
	currentMap = map;
	mapModified = false;
    }
    public Map getMap(){
	return currentMap;
    }
    
    
    public void undo(){
	undostack.undo();
    }
    
    
    public void redo(){
	undostack.redo();
    }
    
    public UndoStack getUndoStack(){
	return undostack;
    }
    
    
    
    public boolean isMapModified(){
	return mapModified;
    }
    
    public void setModified(){
	mapModified = true;
    }
    
    public void setModified(boolean value){
	mapModified = value;
    }
}
