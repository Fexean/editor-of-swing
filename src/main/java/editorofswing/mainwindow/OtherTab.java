
package editorofswing.mainwindow;

import java.io.File;

import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.event.*;
import javafx.geometry.Insets;
import javafx.stage.Stage;

import editorofswing.util.StringUtil;
import editorofswing.util.MapEditor;
import editorofswing.ui.dialog.FileDialog;
import editorofswing.ui.dialog.BackgroundToolDialog;
import editorofswing.types.*;

public class OtherTab extends Tab{
    
    Button resizeButton;
    TextField widthField, heightField, songField, scrollfuncField, coconutIdField, medalAddrField;
    MapEditor editor;
    Stage stage;
    
    public boolean tilesetModified = false;
    
    public OtherTab(){
	GridPane root = new GridPane();
	root.setPadding(new Insets(20, 20, 20, 20)); 
	root.setVgap(10);
	root.setHgap(5);
	
	widthField = new TextField();
	heightField = new TextField();
	songField = new TextField();
	scrollfuncField = new TextField();
	coconutIdField = new TextField();
	medalAddrField = new TextField();
	resizeButton = new Button("Resize map");
	Button exportBgPalButton = new Button("Export Background Palette");
	Button exportSprPalButton = new Button("Export Sprite Palette");
	Button importBgPalButton = new Button("Import Background Palette");
	Button importSprPalButton = new Button("Import Sprite Palette");
	Button exportTileset1Button = new Button("Export Primary Tileset");
	Button importTileset1Button = new Button("Import Primary Tileset");
	Button openBgToolButton = new Button("Background Importer/Exporter");
	
	songField.setTooltip(new Tooltip("Pointer to song data"));
	scrollfuncField.setTooltip(new Tooltip("Pointer to a function that moves backgrounds"));
	coconutIdField.setTooltip(new Tooltip("IWRAM offset where crystal coconut state is stored (FFFFFFFF for none)"));
	medalAddrField.setTooltip(new Tooltip("Address where medal state is stored (FFFFFFFF for none)"));

	root.add(new Label("Song:"), 0, 0, 1, 1);
	root.add(songField, 0, 1, 1, 1);
	
	root.add(new Label("Scroll function:"), 1, 0, 1, 1);
	root.add(scrollfuncField, 1, 1, 1, 1);
	
	root.add(new Label("Coconut ID:"), 0, 2, 1, 1);
	root.add(coconutIdField, 0, 3, 1, 1);
	
	root.add(new Label("Medal address:"), 1, 2, 1, 1);
	root.add(medalAddrField, 1, 3, 1, 1);
	
	GridPane palgrid = new GridPane();
	palgrid.add(exportBgPalButton, 0, 0, 1, 1);
	palgrid.add(importBgPalButton, 0, 1, 1, 1);
	palgrid.add(exportSprPalButton, 1, 0, 1, 1);
	palgrid.add(importSprPalButton, 1, 1, 1, 1);
	palgrid.setVgap(5);
	palgrid.setHgap(60);
	TitledPane palpane = new TitledPane("Palettes", palgrid);
	palpane.setCollapsible(false);
	root.add(palpane, 0, 4, 2, 1);

	
	GridPane tsbox = new GridPane();
	tsbox.add(exportTileset1Button, 0, 0);
	tsbox.add(importTileset1Button, 1, 0);
	TitledPane tspane = new TitledPane("Primary Tileset", tsbox);
	tspane.setCollapsible(false);
	tsbox.setHgap(10);
	root.add(tspane, 0, 5, 1, 1);
	
	GridPane resizeGrid = new GridPane();
	TitledPane resizepane = new TitledPane("Map Size", resizeGrid);
	resizepane.setCollapsible(false);
	resizeGrid.add(new Label("Width:"), 0, 0, 1, 1);
	resizeGrid.add(widthField, 1, 0, 1, 1);
	resizeGrid.add(new Label("Height:"), 0, 1, 1, 1);
	resizeGrid.add(heightField, 1, 1, 1, 1);
	resizeGrid.add(resizeButton, 0, 2, 1, 1);
	root.add(resizepane, 0, 6, 1, 1);
	root.add(openBgToolButton, 0, 7, 2, 1);
	
	
	setContent(new ScrollPane(root));
	setText("Other");
	
	songField.focusedProperty().addListener(e -> updateMapFromFields());
	coconutIdField.focusedProperty().addListener(e -> updateMapFromFields());
	scrollfuncField.focusedProperty().addListener(e -> updateMapFromFields());
	medalAddrField.focusedProperty().addListener(e -> updateMapFromFields());
	
	exportBgPalButton.setOnAction(e -> exportBgPal());
	exportSprPalButton.setOnAction(e -> exportSprPal());
	importBgPalButton.setOnAction(e ->importBgPal());
	importSprPalButton.setOnAction(e ->importSprPal());
	exportTileset1Button.setOnAction(e -> exportTileset1());
	importTileset1Button.setOnAction(e -> importTileset1());
	openBgToolButton.setOnAction(e -> openBgTool());
    }
    
    private void exportBgPal(){
	File f = FileDialog.saveAs(stage, "Save Background Palette", "JASC-PAL", "*.pal", "bgpal.pal");
	if(f != null){
	    editor.getMap().pal.exportJASC(f);
	}
    }
    
    private void exportSprPal(){
	File f = FileDialog.saveAs(stage, "Save Sprite Palette", "JASC-PAL", "*.pal", "sprpal.pal");
	if(f != null){
	    editor.getMap().sprpal.exportJASC(f);
	}
    }
    
    private void importBgPal(){
	File f = FileDialog.openFile(stage, "Import Background Palette", "JASC Palette", "*.pal");
	if(f != null){
	    Palette pal = Palette.importJASC(f);
	    if(pal != null){
		editor.getMap().pal = pal;
		tilesetModified = true;
	    }else{
		new Alert(Alert.AlertType.ERROR, "Failed to read palette!").showAndWait();
	    }
	}
    }
    
    private void importSprPal(){
	File f = FileDialog.openFile(stage, "Import Sprite Palette", "JASC Palette", "*.pal");
	if(f != null){
	    Palette pal = Palette.importJASC(f);
	    if(pal != null){
		editor.getMap().sprpal = pal;
		tilesetModified = true;
	    }else{
		new Alert(Alert.AlertType.ERROR, "Failed to read palette!").showAndWait();
	    }
	}
    }
    
    private void exportTileset1(){
	File f = FileDialog.saveAs(stage, "Export Primary Tileset", "PNG Image", "*.png", "tiles1.png");
	if(f != null){
	    try{
		editor.getMap().tileset1.exportPng(f, editor.getMap().pal);
	    }catch(Exception x){
		new Alert(Alert.AlertType.ERROR, "Failed to export tileset!").showAndWait();
	    }
	}
    }
    
    private void importTileset1(){
	File f = FileDialog.openFile(stage, "Import Primary Tileset", "PNG Image", "*.png");
	if(f != null){
	    try{
		Tileset tileset = new Tileset(f);
		if(tileset.length() > 512){
		    throw new IllegalArgumentException("Too many tiles: " + tileset.length() + " (max 512)");
		}
		editor.getMap().tileset1 = tileset;
		tilesetModified = true;
	    }catch(Exception e){
		new Alert(Alert.AlertType.ERROR, "Failed to import tileset: " + e.getMessage()).showAndWait();
	    }
	}
    }
    
    private void openBgTool(){
	Map map = editor.getMap();
	BackgroundToolDialog dialog = new BackgroundToolDialog(map);
	dialog.showAndWait();
	tilesetModified = true;
    }
    
    public void setOnResizeClicked(EventHandler<ActionEvent> handler){
	resizeButton.setOnAction(handler);
    }
    
 
    public javafx.util.Pair<Integer, Integer> getMapDimensions(){
	int w = StringUtil.strToInt(widthField.getText());
	int h = StringUtil.strToInt(heightField.getText());
	return new javafx.util.Pair<>(w, h);
    }
    
    public void setup(MapEditor editor, Stage stage){
	this.stage = stage;
	this.editor = editor;
	Map map = editor.getMap();
	songField.setText(String.format("0x%08X",map.song));
	scrollfuncField.setText(String.format("0x%08X",map.scrollFunc));
	heightField.setText(""+map.h);
	widthField.setText(""+map.w);
	coconutIdField.setText(String.format("0x%08X", map.crystalCoconutId));
	medalAddrField.setText(String.format("0x%08X", map.medalAddress));
    }
    
    private void updateMapFromFields(){
	Map map = editor.getMap();
	int coconutid = StringUtil.hexStrToInt(coconutIdField.getText());
	int song = StringUtil.hexStrToInt(songField.getText());
	int medaladdr = StringUtil.hexStrToInt(medalAddrField.getText());;
	int scroll = StringUtil.hexStrToInt(scrollfuncField.getText());
	
	if(map.crystalCoconutId != coconutid || map.medalAddress != medaladdr
	|| map.song != song || map.scrollFunc != scroll)
	{
	    editor.setModified();
	}
	
	map.crystalCoconutId = coconutid;
	map.medalAddress = medaladdr;
	map.scrollFunc = scroll;
	map.song = song;
    }
}
