
package editorofswing.ui.dialog;


import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.*;
import javafx.stage.*;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.control.TabPane;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import java.io.File;
import editorofswing.types.*;

import editorofswing.util.BackgroundExporter;
import editorofswing.util.BackgroundImporter;
import javafx.geometry.Insets;

public class BackgroundToolDialog {
    
    Map map;
    Scene scene;
    TextField bg1path, bg2path, bg3path;
    
    public BackgroundToolDialog(Map map){
	this.map = map;
	GridPane root = new GridPane();
	setupUi(root);
	scene = new Scene(root, 400, 350);
    }
    
    public void showAndWait(){
	Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("Background Importer/Exporter");
	stage.setScene(scene);
        stage.showAndWait();
    }
    
    private void setupUi(GridPane root){
	bg1path = new TextField();
	bg2path = new TextField();
	bg3path = new TextField();
	
	GridPane primarybgGrid = new GridPane();
	primarybgGrid.add(new Label("BG1:"), 0, 0, 1, 1);
	primarybgGrid.add(bg1path, 1, 0, 1, 1);
	primarybgGrid.add(createButton("Browse...", e->onBrowse(1)), 2, 0, 1, 1);
	primarybgGrid.add(createButton("Import", e->onImportPrimary()), 0, 1, 1, 1);
	primarybgGrid.add(createButton("Export BG1 Image", e -> onExportTilemap(map.tileset1, map.layer1, "bg1.png")), 1, 1, 1, 1);
	primarybgGrid.setVgap(10);
	primarybgGrid.setHgap(5);
	TitledPane primaryPane = new TitledPane("Primary backgrounds", primarybgGrid);
	primaryPane.setCollapsible(false);
	root.add(primaryPane, 0, 0);
	
	GridPane secondarybgGrid = new GridPane();
	secondarybgGrid.add(new Label("BG2:"), 0, 0, 1, 1);
	secondarybgGrid.add(bg2path, 1, 0, 1, 1);
	secondarybgGrid.add(createButton("Browse...", e->onBrowse(2)), 2, 0, 1, 1);
	secondarybgGrid.add(new Label("BG3:"), 0, 1, 1, 1);
	secondarybgGrid.add(bg3path, 1, 1, 1, 1);
	secondarybgGrid.add(createButton("Browse...", e->onBrowse(3)), 2, 1, 1, 1);
	secondarybgGrid.add(createButton("Import", e->onImportSecondary()), 0, 2, 1, 1);
	secondarybgGrid.add(createButton("Export BG2 Image", e->onExportTilemap(map.tileset2, map.layer2, "bg2.png")), 0, 3, 1, 1);
	secondarybgGrid.add(createButton("Export BG3 Image", e->onExportTilemap(map.tileset2, map.layer3, "bg3.png")), 1, 3, 1, 1);
	secondarybgGrid.setVgap(10);
	secondarybgGrid.setHgap(5);
	TitledPane secondaryPane = new TitledPane("Secondary backgrounds", secondarybgGrid);
	secondaryPane.setCollapsible(false);
	root.add(secondaryPane, 0, 1);
	
	root.setVgap(30);
	root.setPadding(new Insets(10, 10, 10, 10)); 
    }
    
    private Button createButton(String text, EventHandler<ActionEvent> handler){
	Button b = new Button(text);
	b.setOnAction(handler);
	return b;
    }
    
    private void onExportTilemap(Tileset tiles, Tilemap tmap, String defaultname){
	File f = FileDialog.saveAs(scene.getWindow(), "Save BG as", "PNG Image", "*.png", defaultname);
	if(f != null){
	    try{
		BackgroundExporter.exportTilemap(f, tmap, map.pal, tiles);
	    }catch(Exception e){
		new Alert(Alert.AlertType.ERROR, "Failed to export tilemap!").showAndWait();
	    }
	}
    }
    
    private void onImportPrimary(){
	String path = bg1path.getText();
	File f = new File(path);
	if(path.isEmpty() || f.canRead()){
	    try{
		BackgroundImporter.importBackground1(f, map, path.isEmpty());
		new Alert(Alert.AlertType.INFORMATION, "Success! " + map.tileset1.length() + " tiles used.").showAndWait();
	    }catch(Exception e){
		new Alert(Alert.AlertType.ERROR, e.getMessage()).showAndWait();
	    }
	}
    }
    
    private void onImportSecondary(){
	String path1 = bg2path.getText();
	File f1 = new File(path1);
	String path2 = bg3path.getText();
	File f2 = new File(path2);
	if((path1.isEmpty() || f1.canRead()) && (path2.isEmpty() || f2.canRead())){
	    try{
		BackgroundImporter.importSecondaryBackgrounds(f1, f2, map, path1.isEmpty(), path2.isEmpty());
		new Alert(Alert.AlertType.INFORMATION, "Success! " + map.tileset2.length() + " tiles used.").showAndWait();
	    }catch(Exception e){
		new Alert(Alert.AlertType.ERROR, e.getMessage()).showAndWait();
	    }
	}
    }


    private void onBrowse(int bg){
	File f = FileDialog.openFile(scene.getWindow(), "", "Indexed PNG Image", "*.png");
	if(f != null){
	    String s = f.getAbsolutePath();
	    
	    switch(bg){
		case 1:
		    bg1path.setText(s);
		    break;
		case 2:
		    bg2path.setText(s);
		    break;
		case 3:
		    bg3path.setText(s);
		    break;
	    }
	}
    }
}
