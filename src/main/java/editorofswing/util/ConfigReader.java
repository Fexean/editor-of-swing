

package editorofswing.util;
import java.util.HashMap;
import java.util.ArrayList;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.BufferedReader;

import editorofswing.types.*;

import java.net.URL;
import java.io.*;




public class ConfigReader {
    
    
    
    //for offsets & entity types,
    /*
	lines starting with ';' are ignored
	other lines are in format: key=value1,value2,value3, ...
    */
    public static HashMap<String, Integer> readHexSymbols(String filename){
	HashMap<String, Integer> result = new HashMap();
	try{
	    InputStream i = getResourceStream(filename);
	    BufferedReader in  = new BufferedReader(new InputStreamReader(i));
	    
	    String line;  
	    while((line=in.readLine())!=null){
		if(line.isEmpty()|| line.charAt(0) ==';'){
		    continue;
		}
		final int splitpos = line.indexOf("=");
		if(splitpos != -1){
		    final String valuestr = line.substring(splitpos+1);
		    final String keystr = line.substring(0, splitpos);
		    result.put(keystr, Integer.parseInt(valuestr, 16));
		}
	    }
	    
	}catch(Exception e){
	    System.out.println("[ConfigReader.readHexSymbols] Unable to read config:" + filename);
	    e.printStackTrace();
	}
	return result;
    }
    
    //
    public static HashMap<String, ArrayList<String>> readStructure(String filename){
	HashMap<String, ArrayList<String>> result = new HashMap();
	try{
	    InputStream i = getResourceStream(filename);
	    BufferedReader in  = new BufferedReader(new InputStreamReader(i));
	    
	    String line;  
	    while((line=in.readLine())!=null){
		if(line.isEmpty() || line.charAt(0) ==';'){
		    continue;
		}
		final int splitpos = line.indexOf("=");
		if(splitpos != -1){
		    String valuestr = line.substring(splitpos+1);
		    String keystr = line.substring(0, splitpos);
		    
		    ArrayList<String> values = new ArrayList();
		    for(String s : valuestr.split(";")){
			values.add(s);
		    }
		    
		    result.put(keystr, values);
		}
	    }
	    
	}catch(Exception e){
	    System.out.println("[ConfigReader.readStructure] Unable to read config:" + filename);
	    e.printStackTrace();
	}
	return result;
    }
    
    
    public static ArrayList<String> getKeys(String resource){
	ArrayList<String> result = new ArrayList();
	try{
	    InputStream i = getResourceStream(resource);
	    BufferedReader in  = new BufferedReader(new InputStreamReader(i));
	    
	    String line;  
	    while((line=in.readLine())!=null){
		if(line.isEmpty() || line.charAt(0) ==';'){
		    continue;
		}
		final int splitpos = line.indexOf("=");
		if(splitpos != -1){
		    String keystr = line.substring(0, splitpos);
		    result.add(keystr);
		}
	    }
	    
	}catch(Exception e){
	    System.out.println("[ConfigReader.getKeys] Unable to read config:" + resource);
	}
	return result;
    }
    
    
    
    public static ArrayList<MapBlock> readBlockset(String resourceName){
		try{		
			InputStream i = getResourceStream(resourceName);
			return readBlockset(new BufferedReader(new InputStreamReader(i)));
		}catch(Exception e){
			System.out.println("[ConfigReader.readBlockset] Failed to open resource:" + resourceName);		
		}
		return new ArrayList<MapBlock>();
    }
    
    public static ArrayList<MapBlock> readBlockset(File file) throws FileNotFoundException{
		return readBlockset(new BufferedReader(new FileReader(file)));
    }
    
    public static ArrayList<MapBlock> readBlockset(BufferedReader br){
	ArrayList<MapBlock> blocks = new ArrayList();	
	try{
	    String line;  
	    while((line=br.readLine())!=null){
		if(line.isEmpty() || line.charAt(0)==';'){continue;}
		blocks.add(new MapBlock(line));
	    }
	}catch(Exception e){
	    System.out.println("[ConfigReader.readBlockset] Unable to read blocks" + " (" + e.getMessage()+ ")");
	}
	return blocks;
    }
    
    
    public static HashMap<String, EntityTemplate> readEntityTemplates(String filename){
		HashMap<String, EntityTemplate> result = new HashMap();
		try{
		    InputStream i = getResourceStream(filename);
	    	BufferedReader in  = new BufferedReader(new InputStreamReader(i));
	    	String line;  
	    	while((line=in.readLine())!=null){
				if(line.isEmpty() || line.charAt(0)==';'){continue;}
				final String key = line.substring(0, line.indexOf('='));
				result.put(key, new EntityTemplate(line));
	    	}
		}catch(Exception e){
	    	System.out.println("[ConfigReader.readEntityTemplates] Unable to read entities:" + filename);
		}
		return result;
    }
   
	private static InputStream getResourceStream(String name) throws IOException {
		return (editorofswing.EditorOfSwing.class.getModule()).getResourceAsStream(name);
	}
    
}
