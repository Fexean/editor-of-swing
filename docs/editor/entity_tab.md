# Entity-tab
![Entity-tab Screenshot](https://gitlab.com/Fexean/editor-of-swing/-/raw/master/docs/editor/screenshots/entitytab_screenshot.png "Entity-tab Screenshot")  

In this tab you can edit the entities of a map.
The entities are drawn on the map as 16x16 rectangles where the center of the rectangle is at the entity's xy-position. The currently selected entity is drawn in yellow while the others are light blue.
If you are using an european rom and have the "Draw entity sprites" option enabled, entities will be drawn as predefined sprites with their colors taken from the level's sprite palette when possible. Note that this is not fully accurate to how things look in-game as the editor disregards priority, rotation and entities consisting of more than one sprite.

You can use the "Selected Entity" widget on the top right to select an entity. You can also select an entity by left clicking on it.
Once you select an entity, its fields will appear and you will be able to edit them. For a description of the different fields see [Entity](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/king_of_swing/entity.md).  

There's also something called entity templates. They can help you both tell the type of an entity and also change the type of an entity without having to memorize callback pointers. By clicking on the button under "Templates" , you can get a list of different entity templates and by selecting one the entity table and callback fields are set according to that template.
The editor also selects the matching template when the callback field is changed. 

You can use the "New entity..." button to create a new entity. Its fields will all be 0 so it will appear in the top left corner of the map.  

## Clicking on entities on the map
If you left click on an entity, it's selected.
If you left click on an entity and drag it, you can move the entity.  
If you right click on an entity, a clone of that entity is created.  
If you middle click on an entity, it is deleted.  
