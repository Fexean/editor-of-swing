
package editorofswing.undo;

import editorofswing.types.Map;


public class CollisionPlacementCmd implements UndoCmd{
    final private Map map;
    final private int x, y;
    final private byte oldTile, newTile;
    final private boolean merge;
    
    public CollisionPlacementCmd(Map map, int tx, int ty, byte col, boolean merge){
	this.map = map;
	this.merge = merge;
	newTile = col;
	oldTile = map.wallcol.getCollision(tx, ty);
	x = tx;
	y = ty;
    }
    
    @Override
    public void redo(){
	map.wallcol.setCollision(x,y, newTile);
    }
    
    @Override
    public void undo(){
	map.wallcol.setCollision(x,y, oldTile);
    }
    
    @Override
    public boolean shouldMerge(){
	return merge;
    }
}
