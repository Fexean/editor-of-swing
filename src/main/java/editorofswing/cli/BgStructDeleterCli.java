
package editorofswing.cli;

import editorofswing.util.LevelDeleter;
import editorofswing.util.Rom;


public class BgStructDeleterCli {
    final static String helpStrings[] = {
	"USAGE: editorofswing delete-bgstructs <romfile>",
	"",
	"Deletes non-english bgstructs and repoints them to the english ones."
    };

    public static void cliMain(String[] args){
	if (args.length < 3){
	    
	    CliUtil.printHelp(helpStrings);
	    System.exit(0);
	}
		
	Rom rom = CliUtil.tryOpenRom(args[1]);
	LevelDeleter.deleteBgStructs(rom);
	CliUtil.trySaveRom(rom);
	CliUtil.exit("Bg structs deleted");
    }

}
