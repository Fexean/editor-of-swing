# Unused entities
These entities are not used in any levels. They are also not currently in the entity templates.
These are just the ones I've noticed, there are likely a lot more.  

Spin Lever (Up/Down) Green		89C50B4  
Spin Lever (Up/Down) Blue		89C50C0  
Spin Lever (Left/Right) Green		89C5070  
Crank Moving RightHand Red		89C8364    
Crank Moving RightHand Blue	89C8370  
Crank Moving LeftHand Red		89C8338
Crank Moving LeftHand Green	89C8344
Invis Trigger (24x48)		89C22DC  
Invis Trigger (48x24)		89C22F0  
1x1 Red Peg Spawner		89C8140  
3x3 Peg Waterfall 			89C5B40  
1x1 Red Peg Waterfall		89C5AF0  
1x1 Green Peg Waterfall		89C5B04  
1x1 Blue Peg Waterfall		89C5B18  
Spike Waterfall			89C5B60  
5x5 Red Peg (Shrinked)		89C92FC  
3x3 Red Peg (Shrinked)		89C9374  
3x3 Green Peg (Shrinked)		89C939C  
3x3 Blue Peg (Shrinked)		89C93C4  
1x1 Red Peg (Shrinked)		89C93EC  
1x1 Green Peg (Shrinked)		89C9414  
1x1 Blue Peg (Shrinked)		89C943C  
1x1 Ice Block			89C9AC8  
Unused Glitchy Block 1		89C99D8  
Unused Glitchy Block 2		89C9A14  
Unused Glitchy Block 3		89C9A50  
Unused Glitchy Block 4		89C9A8C  
Controllable Peg			89C7ED8  
Glitchy Rat			89E474C  
Beetle (Circling)			89E0580  
1x1 Red Peg (Lever Moved)		89C90AC  
1x1 Green Peg (Lever Moved)		89C90D8  
1x1 Blue Peg (Lever Moved)		89C9104  
3x3 Green Peg (Lever Moved)		89C9054  
3x3 Blue Peg (Lever Moved)		89C9080  
5x5 Blue Peg (Lever Moved)		89C8FFC  
11x21 Peg Red (Left/Right)		8870088  
11x21 Peg Green (Left/Right)		887009C  
11x21 Peg Blue (Left/Right)		88700B0  
11x13 Peg Red (Left/Right)		88700C4  
11x13 Peg Green (Left/Right)		88700D8  
11x13 Peg Blue (Left/Right)		88700EC  
11x5 Peg Red (Up/Down)		886FEE4  
11x5 Peg Green (Up/Down)		886FEF8  
11x5 Peg Blue (Up/Down)		886FF0C  
11x21 Peg Red (Up/Down)		886FF20  
11x21 Peg Green (Up/Down)		886FF34  
11x21 Peg Blue (Up/Down)		886FF48  
11x13 Peg Red (Up/Down)		886FF5C  
11x13 Peg Green (Up/Down)		886FF70  
11x13 Peg Blue (Up/Down)		886FF84  
Spin Lever Pegboards Borderless	89C2EE8  
Spin Lever Gate (Bordered)		89C318C  
