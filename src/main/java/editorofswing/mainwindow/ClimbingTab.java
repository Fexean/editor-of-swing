
package editorofswing.mainwindow;

import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.canvas.*;
import javafx.scene.Scene;
import javafx.geometry.Insets;
import javafx.scene.control.Tab;
import editorofswing.types.*;

import editorofswing.util.MapEditor;
import editorofswing.util.RectUtil;
import editorofswing.rendering.*;
import javafx.scene.Cursor;




public class ClimbingTab extends Tab{
    
    BorderPane root;
    Pane canvasPane;
    Canvas canvas;
    
    CollisionRenderer renderer;
    MapEditor editor;
    Scene scene;
    
    ClimbCollision selected;
    TextField xField, yField, wField, hField;
    RadioButton centerButton;
    
    VBox toolRoot, fieldRoot;
    editorofswing.ui.util.Spinbox climbSelector;
    Label climbcountLabel;
    
    int dragSide;
    
    public ClimbingTab(){
	
	climbcountLabel = new Label();
	xField = new TextField();
	yField = new TextField();
	wField = new TextField();
	hField = new TextField();
	centerButton = new RadioButton();
	
	canvas = new Canvas();
	renderer = new CollisionRenderer(canvas);
	canvasPane = new Pane();
	root = new BorderPane();
	ScrollPane mapscroll = new ScrollPane();
	mapscroll.setContent(canvasPane);
	root.setCenter(mapscroll);
	
	toolRoot = new VBox();
	Button addButton = new Button("New Climbing Area...");
	climbSelector = new editorofswing.ui.util.Spinbox(0,0,0);
	toolRoot.getChildren().add(new Label("Selected Climbing Area:"));
	toolRoot.getChildren().add(climbSelector);
	toolRoot.getChildren().add(climbcountLabel);
	toolRoot.getChildren().add(addButton);
	
	toolRoot.setSpacing(10);
	toolRoot.setPadding(new Insets(0, 10, 10, 10)); 
	
	fieldRoot = new VBox();
	fieldRoot.setSpacing(10);
	fieldRoot.setPadding(new Insets(0, 10, 10, 10)); 
	fieldRoot.setVisible(false);
	fieldRoot.getChildren().add(new Label("X:"));
	fieldRoot.getChildren().add(xField);
	fieldRoot.getChildren().add(new Label("Y:"));
	fieldRoot.getChildren().add(yField);
	fieldRoot.getChildren().add(new Label("Width:"));
	fieldRoot.getChildren().add(wField);
	fieldRoot.getChildren().add(new Label("Height:"));
	fieldRoot.getChildren().add(hField);
	fieldRoot.getChildren().add(new Label("Centers Player:"));
	fieldRoot.getChildren().add(centerButton);
	
	Button deleteButton = new Button("Delete");
	fieldRoot.getChildren().add(deleteButton);
	toolRoot.getChildren().add(fieldRoot);
	
	root.setRight(toolRoot);
	
	setContent(root);
	setText("Climbing");
	
	climbSelector.setOnValueChange((a, b, c) -> {
	    int val = climbSelector.getValue();
	    if(val < editor.getMap().climbCol.size()){
		selected = editor.getMap().climbCol.get(val);
		updateFields();
	    }
	});
	
	xField.focusedProperty().addListener(e->{updateDataFromFields();});
	yField.focusedProperty().addListener(e->{updateDataFromFields();});
	wField.focusedProperty().addListener(e->{updateDataFromFields();});
	hField.focusedProperty().addListener(e->{updateDataFromFields();});
	centerButton.setOnMouseClicked(e->{updateDataFromFields();});
	
	deleteButton.setOnAction(e->{onDeleteButton();});
	addButton.setOnAction(e->{onAddButton();});
	
	canvas.setOnMousePressed(e->{onCanvasClicked(e.getX(), e.getY(), e.getButton());});
	canvas.setOnMouseDragged(e->{onCanvasDragged(e.getX(), e.getY(), e.getButton());});
	canvas.setOnMouseReleased(e->{
	    dragSide = -1;
	    scene.setCursor(Cursor.DEFAULT);
	});
	
	canvas.setOnMouseMoved(e->{
	    scene.setCursor(RectUtil.rectSideToCursor(getDragSide((int)e.getX(), (int)e.getY())));
	});
	
	canvas.setOnMouseExited(e -> {
	    if(dragSide == -1){
		scene.setCursor(Cursor.DEFAULT);
	    }
	});
    }
    
    
    private void updateFields(){
	redrawClimbingAreas();
	xField.setText(String.format("0x%04X", selected.x));
	yField.setText(String.format("0x%04X", selected.y));
	wField.setText(String.format("0x%08X", selected.w));
	hField.setText(String.format("0x%08X", selected.h));
	centerButton.selectedProperty().set((selected.flags == 1));
	
	fieldRoot.setVisible(true);
    }
    
    private void updateDataFromFields(){ //We need to ensure this runs before the map is saved IF there is no repoint dialog
	if(selected == null){return;}
	selected.x = editorofswing.util.StringUtil.strToInt(xField.getText());
	selected.y = editorofswing.util.StringUtil.strToInt(yField.getText());
	selected.w = (short)editorofswing.util.StringUtil.strToInt(wField.getText());
	selected.h = (short)editorofswing.util.StringUtil.strToInt(hField.getText());
	selected.flags = centerButton.selectedProperty().get() ? (short)1 : (short)0;
	redrawClimbingAreas();
	editor.setModified();
    }
    
    public void redrawClimbingAreas(){
	canvas.getGraphicsContext2D().clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
	renderer.drawMapClimbRects(editor.getMap(), selected);
    }
    
    private void onDeleteButton(){
	editor.getMap().climbCol.remove(selected);
	climbSelector.setValue(0);
	fieldRoot.setVisible(false);
	selected = null;
	updateClimbCount();
	redrawClimbingAreas();
	editor.setModified();
    }
    
    private void onAddButton(){
	selected = new ClimbCollision(0, 0, (short)16, (short)16, (short)0);
	editor.getMap().climbCol.add(selected);
	updateFields();
	updateClimbCount();
	climbSelector.setValue(editor.getMap().climbCol.size() - 1);
	editor.setModified();
    }
    
    
    private void onCanvasClicked(double x, double y, javafx.scene.input.MouseButton button){
	if(x < 0 || y < 0 || x >= canvas.getWidth() || y >= canvas.getHeight()){
	    return;
	}
	updateDataFromFields();
	ClimbCollision ent = editor.getClimbAreaAt((int)x, (int)y);
	if(ent != null){    
	    switch(button){
		case PRIMARY:
		    selected = ent;
		    dragSide = getDragSide((int)x, (int)y);
		    break;
		    
		case SECONDARY:
		    selected = new ClimbCollision(ent.x, ent.y, ent.w, ent.h, ent.flags);
		    editor.getMap().climbCol.add(selected);
		    updateClimbCount();
		    dragSide = 5; //copied area is always moved, not resized
		    break;
		    
		case MIDDLE:
		    selected = ent;
		    onDeleteButton();
		    return;
		    
		default:
		    return;
	    }
	    updateFields();
	    climbSelector.setValue(editor.getMap().climbCol.indexOf(ent));
	}
    }
    

    private void onCanvasDragged(double x, double y, javafx.scene.input.MouseButton button){	
	if(selected == null){return;}
	
	final int ix = (int)x;
	final int iy = (int)y;
	
	switch(dragSide){
	    case 1:
		selected.h = (short)(selected.y + selected.h - alignToGrid(iy));
		selected.y = alignToGrid(iy);
		selected.w = (short)(selected.x + selected.w - alignToGrid(ix));
		selected.x = alignToGrid(ix);
		break;
	    case 2:
		selected.h = (short)(selected.y + selected.h - alignToGrid(iy));
		selected.y = alignToGrid(iy);
		break;
	    case 3:
		selected.h = (short)(selected.y + selected.h - alignToGrid(iy));
		selected.y = alignToGrid(iy);
		selected.w = (short)(alignToGrid(ix)-selected.x);
		break;
	    case 6:
		selected.w = (short)(alignToGrid(ix)-selected.x);
		break;
	    case 4:
		selected.w = (short)(selected.x + selected.w - alignToGrid(ix));
		selected.x = alignToGrid(ix);
		break;
	    case 5:
		selected.x = alignToGrid(ix - selected.w/2);
		selected.y = alignToGrid(iy - selected.h/2);
		break;
	    
	    case 7:
		selected.w = (short)(selected.x + selected.w - alignToGrid(ix));
		selected.x = alignToGrid(ix);
		selected.h = (short)(alignToGrid(iy)-selected.y);
		break;
	    case 8:
		selected.h = (short)(alignToGrid(iy)-selected.y);
		break;
	    case 9:
		selected.h = (short)(alignToGrid(iy)-selected.y);
		selected.w = (short)(alignToGrid(ix)-selected.x);
		break;
	}
	updateFields();
	editor.setModified();
    }
    
    private int alignToGrid(int x){
	return (x & 0xFFFFFFFC) + 2;
    }
    
    
    public Canvas getCanvas(){
	return canvas;
    }
    
    public void resizeCanvas(double w, double h){
	canvas.setWidth(w);
	canvas.setHeight(h);
    }
    
    public void setup(MapEditor e, Canvas mapcanvas, Scene scene){
	this.scene = scene;
	canvas.setWidth(mapcanvas.getWidth());
	canvas.setHeight(mapcanvas.getHeight());
	editor = e;
	canvasPane.getChildren().clear();
	canvasPane.getChildren().add(canvas);
	canvasPane.getChildren().add(mapcanvas);
	canvas.toFront();
	selected = null;
	fieldRoot.setVisible(false);
	climbSelector.setValue(0);
	
	updateClimbCount();
	redrawClimbingAreas();

    }
    
    public void updateClimbCount(){
	int newmax = editor.getMap().climbCol.size()-1;
	climbSelector.setLimits(0, newmax);
	if(climbSelector.getValue() > newmax){
	    climbSelector.setValue(0);
	}
	climbcountLabel.setText("Climbing Areas: " + (newmax + 1));
	
    }
    
    private int getDragSide(int x, int y){
	ClimbCollision area = editor.getClimbAreaAt(x, y);
	if(area == null){
	    return -1;
	}
	java.awt.Rectangle rect = new java.awt.Rectangle(area.x, area.y, area.w, area.h);
	return RectUtil.getRectSide(rect, x, y, 0.15);
    }
}
