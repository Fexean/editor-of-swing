
package editorofswing.undo;
import java.util.ArrayDeque;

public class UndoStack {
    final private ArrayDeque<UndoCmd> doneCmds = new ArrayDeque();
    final private ArrayDeque<UndoCmd> undoneCmds = new ArrayDeque();
    
    private int maxSize;
    private int cmdCnt; //accounting merge
    
    public UndoStack(int maxSize){
	this.maxSize = maxSize;
    }
    
    public void addCommand(UndoCmd cmd){
	cmd.redo();
	doneCmds.addLast(cmd);
	undoneCmds.clear();
	
	if(!cmd.shouldMerge()){
	    cmdCnt++;
	    
	    if(cmdCnt > maxSize){
		do{
		    doneCmds.removeFirst();
		}while(doneCmds.peekFirst().shouldMerge());
	    }
	}
    }
    
    public void undo(){
	boolean merge = true;
	while(merge && !doneCmds.isEmpty()){
	    UndoCmd cmd = doneCmds.removeLast();
	    cmd.undo();
	    undoneCmds.addFirst(cmd);
	    merge = cmd.shouldMerge();
	}
	cmdCnt--;
    }
    
    public void redo(){
	boolean merge = true;
	while(merge && !undoneCmds.isEmpty()){
	    UndoCmd cmd = undoneCmds.removeFirst();
	    cmd.redo();
	    doneCmds.addLast(cmd);
	    if(undoneCmds.isEmpty()){
		return;
	    }
	    merge = undoneCmds.peekFirst().shouldMerge();
	}
	cmdCnt++;
    }
    
    public void clear(){
	doneCmds.clear();
	undoneCmds.clear();
	cmdCnt = 0;
    }
    
    public boolean canUndo(){
	return !doneCmds.isEmpty();
    }
    
    public boolean canRedo(){
	return !undoneCmds.isEmpty();
    }
}
