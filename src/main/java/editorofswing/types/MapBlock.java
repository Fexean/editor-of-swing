

package editorofswing.types;
import java.util.ArrayList;
import java.io.File;

public class MapBlock {
    public int width, height;
    public Tilemap tmap;
    public CollisionMap cmap;
    
    public MapBlock(String blockstr) throws Exception{ //Format: [width] [height] [raw tiledata] [collision data]
	String parts[] = blockstr.split(" ");
	if(parts.length < 4){
	     throw (new Exception("[MapBlock::MapBlock] blockstr too short"));
	}
	width = Integer.parseInt(parts[0]);
	height = Integer.parseInt(parts[1]);
	tmap = new Tilemap(width, height);
	cmap = new CollisionMap(width, height);
	for(int y = 0;y<height;y++){
	    for(int x = 0;x<width;x++){
		int tiledata = Integer.parseInt(parts[2 + x + y*width]);
		int coldata = Integer.parseInt(parts[2 + (height*width) + x + y*width]);
		tmap.setRaw(x, y, (short)tiledata);
		cmap.setCollision(x, y, (byte)coldata);
	    }
	}
    }
    
    public MapBlock(int w, int h){
	createMaps(w, h);
    }
    
    private void createMaps(int w, int h){
	width = w;
	height = h;
	tmap = new Tilemap(w, h);
	cmap = new CollisionMap(w, h);
    }
    
    @Override
    public String toString(){
	String str = String.format("%d %d ", width, height);
	for(int i = 0;i<height;i++){for(int j = 0;j<width;j++){str += Short.toUnsignedInt(tmap.getRaw(j, i))+" ";}}
	for(int i = 0;i<height;i++){for(int j = 0;j<width;j++){str += Byte.toUnsignedInt(cmap.getCollision(j, i))+" ";}}
	return str;
    }
    
    
    public static void saveToFile(ArrayList<MapBlock> blocks, File file) throws java.io.IOException{
	java.io.FileWriter wr = new java.io.FileWriter(file);
	for(MapBlock b : blocks){
	    wr.write(b.toString() + "\n");
	}
	wr.close();
    }
}
