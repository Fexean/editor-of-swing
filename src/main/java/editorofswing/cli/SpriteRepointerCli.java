
package editorofswing.cli;

import editorofswing.util.SpriteRepointer;
import editorofswing.util.Rom;
import editorofswing.util.RomConstants;
import editorofswing.util.RomMisc;

public class SpriteRepointerCli {
    //RomConstants must be initialized before this class is used
    int[] oldCounts = {RomConstants.affineTileCnt, RomConstants.regularTileCnt, RomConstants.playerTileCnt};
    int[] newCounts = {0x10000, 0x10000, 0x10000};
    
    final static String helpStrings[] = {
        "USAGE: editorofswing spr-repoint <romfile> [newCounts] [oldCounts]",
        "",
        "Options:",
        "[newCounts] and [oldCounts] should be colon separated lists of hexadecimals with no spaces (example:1AC,200,200).",
        "Tile count values should be in order: affine, regular, player.",
        "if not given, newCounts will default to maximum (0x10000) and oldCounts will default to vanilla ROM values."
    };
    
    public static void cliMain(String[] args) {
        if (args.length < 2) {
            CliUtil.printHelp(helpStrings);
            System.exit(0);
        }
        Rom rom = CliUtil.tryOpenRom(args[1]); 
        SpriteRepointerCli cli = new SpriteRepointerCli(args, rom);
        CliUtil.exit("Sprite repoint complete");
    }
    
    int[] parseCounts(String counts){
        String[] s = counts.split(",");
        int[] result = new int[3];
        try{
            result[0] = Integer.parseInt(s[0], 16);
            result[1] = Integer.parseInt(s[1], 16);
            result[2] = Integer.parseInt(s[2], 16);
        }catch(Exception e){
            CliUtil.exit("Invalid counts:" + counts);
        }
        return result;
    }
    
    public void parseArgs(String[] args){
        if(args.length >= 3)
            newCounts = parseCounts(args[2]);
        if(args.length == 4)
            oldCounts = parseCounts(args[3]);
    }
    
    public SpriteRepointerCli(String[] args, Rom rom) {
        parseArgs(args);
        SpriteRepointer repointer = new SpriteRepointer(rom);
        
        int freespace = RomMisc.align4(rom.sizeUsed()); //place tiles at the end of rom to avoid slow freespace search
        if(freespace > 32*1024*1024 - 0x20*(newCounts[0] + newCounts[1] + newCounts[2])){
            CliUtil.exit("Failed to find free space for tiles.");
        }

        repointer.repointSpriteTiles(freespace, oldCounts, newCounts);
        CliUtil.trySaveRom(rom);
        
        System.out.println("Updated sprite tile counts:");
        final String[] names = {"Affine", "Regular", "Player"};
        for(int i = 0;i<3;i++){
            System.out.println(names[i] + "\t" + oldCounts[i] + " => " + newCounts[i]);
        }
    }
}
