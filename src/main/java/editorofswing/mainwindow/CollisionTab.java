
package editorofswing.mainwindow;


import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.canvas.*;
import javafx.geometry.Insets;
import javafx.collections.FXCollections;
import javafx.beans.value.ObservableValue;
import javafx.beans.value.ChangeListener;
import javafx.scene.control.Tab;
import java.util.HashMap;

import editorofswing.types.*;
import editorofswing.rendering.*;
import editorofswing.util.MapEditor;
import editorofswing.util.ConfigReader;

public class CollisionTab extends Tab{
    BorderPane root;
    Pane canvasPane;
    Canvas canvas;
    HashMap<String, Integer> colNames; 
    ListView colList;
    Slider opacitySlider;
    
    CollisionRenderer renderer;
    MapEditor editor;
    CheckBox leftCheckBox, rightCheckBox, upCheckBox, downCheckBox;
    
    boolean horizontalLock = false;
    boolean verticalLock = false;
    int lockX, lockY;
    int screenCursorX, screenCursorY;
    
    
    public CollisionTab(){
	
	root = new BorderPane();
	canvas = new Canvas();
	canvasPane = new Pane();
	this.renderer = new CollisionRenderer(canvas);
	
	ScrollPane canvasScroll = new ScrollPane();
	canvasScroll.setContent(canvasPane);
	root.setCenter(canvasScroll);
	
	ScrollPane toolScroll = new ScrollPane();
	
	colList  = new ListView();
	colNames = ConfigReader.readHexSymbols("CollisionNames.txt");
	colList.setItems(FXCollections.observableArrayList(ConfigReader.getKeys("CollisionNames.txt")));
	colList.getSelectionModel().select(0);
	
	VBox toolRoot = new VBox();
	toolRoot.setSpacing(10);
	toolRoot.setPadding(new Insets(0, 10, 10, 10)); 
	
	toolRoot.getChildren().add(new Label("Collision type:"));
	toolRoot.getChildren().add(colList);
	
	VBox dirBox = new VBox();
	leftCheckBox = new CheckBox("Left");
	rightCheckBox = new CheckBox("Right");
	upCheckBox = new CheckBox("Up");
	downCheckBox = new CheckBox("Down");
	dirBox.getChildren().addAll(new Label("Collision direction:"), leftCheckBox, rightCheckBox, upCheckBox, downCheckBox);
	toolRoot.getChildren().add(dirBox);
	
	toolRoot.getChildren().add(new Label("Opacity:"));
	initOpacitySlider();
	toolRoot.getChildren().add(opacitySlider);
	toolRoot.setPrefWidth(180);
	
	toolScroll.setContent(toolRoot);
	root.setRight(toolScroll);
	
	setText("Collisions");
	setContent(root);
	
	
	canvas.setOnMousePressed(e->{onCanvasClicked(e.getX(), e.getY(), e.getButton(), false);});
	canvas.setOnMouseDragged(e->{onCanvasDragged(e.getX(), e.getY(), e.getButton());});
	canvas.setOnMouseMoved(e->{onMapMouseMove(e.getX(), e.getY());});
    }
    
    
    private void initOpacitySlider(){
	final int defaultValue = 80; 
	opacitySlider = new Slider();
	opacitySlider.setMin(0);
	opacitySlider.setMax(100);
	opacitySlider.setValue(defaultValue);
	renderer.setCollisionOpacity(defaultValue/100.0);
	opacitySlider.setShowTickMarks(true);
	opacitySlider.setMajorTickUnit(50);
	opacitySlider.setMinorTickCount(5);
	opacitySlider.valueProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> ov, Number old_val, Number new_val) {
		renderer.setCollisionOpacity(new_val.doubleValue()/100);
		redraw();
            }
        });
    }
    
    private void setDirectionValue(int collision){
	upCheckBox.selectedProperty().set((collision & 1) == 1);
	downCheckBox.selectedProperty().set((collision & 2) == 2);
	leftCheckBox.selectedProperty().set((collision & 4) == 4);
	rightCheckBox.selectedProperty().set((collision & 8) == 8);
    }
    
    private int getDirectionValue(){
	int dir = 0;
	dir |= (upCheckBox.selectedProperty().get()) ? 1 : 0;
	dir |= (downCheckBox.selectedProperty().get()) ? 2 : 0;
	dir |= (leftCheckBox.selectedProperty().get()) ? 4 : 0;
	dir |= (rightCheckBox.selectedProperty().get()) ? 8 : 0;
	return dir;
    }
    
    public Canvas getCanvas(){
	return canvas;
    }
    
    public void resizeCanvas(int w, int h){
	canvas.setWidth(w);
	canvas.setHeight(h);
    }
    
    private int getSelectedCollision(){
	String name = (String)colList.getSelectionModel().getSelectedItem();
	return (colNames.get(name) << 4) | getDirectionValue();
    }
    
    public void redraw(){
	Map map = editor.getMap();
	if(map != null){
	    canvas.getGraphicsContext2D().clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
	    renderer.drawCollisions(map);
	}
    }
    
    
    private void onCanvasDragged(double x, double y, javafx.scene.input.MouseButton button){
	if(button == javafx.scene.input.MouseButton.PRIMARY || button == javafx.scene.input.MouseButton.SECONDARY){
	    onCanvasClicked(x, y, button, true);
	}
    }
    
    private void onCanvasClicked(double x, double y, javafx.scene.input.MouseButton button, boolean dragging){
	if(x < 0 || y < 0 || x >= canvas.getWidth() || y >= canvas.getHeight()){
	    return;
	}
	int pos[] = getTilePosAt((int)x, (int)y);
	int tx = pos[0];
	int ty = pos[1];
	
	switch(button){
	    case PRIMARY: //Left click: set collision
		editor.setCollisionAt(tx, ty, (byte)getSelectedCollision(), dragging);
		canvas.getGraphicsContext2D().clearRect(tx*8 +1, ty*8 +1, 7, 7);
		renderer.drawCollision(editor.getMap(), tx, ty);
		break;
	    
	    case SECONDARY:  //Right click: Copy collision
		int col = editor.getCollisionAt(tx, ty);
		for(Object t : colList.getItems()){
		    if(colNames.get((String)t) == col >> 4){
			colList.getSelectionModel().select(t);
			break;
		    }
		}
		setDirectionValue(col);
	    break;
	    
	    case MIDDLE: // Middle click: Flood fill
		editor.floodFillCollision(tx, ty, (byte)getSelectedCollision());
		canvas.getGraphicsContext2D().clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
		renderer.drawCollisions(editor.getMap());
		break;
	}
	
	onMapMouseMove(x, y);
    }
    
    public void setup(MapEditor editor, Canvas mapcanvas){
	this.editor = editor;
	
	canvas.setWidth(mapcanvas.getWidth());
	canvas.setHeight(mapcanvas.getHeight());
	
	canvasPane.getChildren().clear();
	canvasPane.getChildren().add(mapcanvas);
	canvasPane.getChildren().add(canvas);
	canvas.toFront();
	redraw();
    }
    
    private int[] getTilePosAt(int x, int y){
	int tx = x/8;
	int ty = y/8;
	
	if(horizontalLock){
	    tx = lockX/8;
	}
	
	if(verticalLock){
	    ty = lockY/8;
	}
	
	int result[] = {tx, ty};
	return result;
    }
    
    private void onMapMouseMove(double x, double y){
	final GraphicsContext ctx = canvas.getGraphicsContext2D();
	int[] pos = getTilePosAt((int)x, (int)y);
	ctx.clearRect(Math.max(1, screenCursorX-7), Math.max(1, screenCursorY-7), 23, 23);
	renderer.drawCollisions(editor.getMap(), screenCursorX-8, screenCursorY-8, 16, 16);
	
	screenCursorX = 8*pos[0];
	screenCursorY = 8*pos[1];
	ctx.strokeRect(8*pos[0], 8*pos[1], 8, 8);
    }
    
    public void setHorizontalLock(boolean value){
	if(!horizontalLock && value){
	    lockX = screenCursorX;
	}
	horizontalLock = value;
    }
    
    public void setVerticalLock(boolean value){
	if(!verticalLock && value){
	    lockY = screenCursorY;
	}
	verticalLock = value;
    }
    
    
}
