# Climbing-tab
![Climbing-tab Screenshot](https://gitlab.com/Fexean/editor-of-swing/-/raw/master/docs/editor/screenshots/climbtab_screenshot.png "Climbing-tab Screenshot")  
The climbing tab is used to edit which parts of the map are climbable.  

The climbing areas are drawn on the map as semi-transparent rectangles. The selected climbing area is drawn in light blue while the others are drawn in purple.  
You can use the "Selected Climbing Area" widget on the top right to select an climbing area. You can also select an entity by left clicking on it.  

You can use the "New Climbing Area..." button to create a new climbing area. Its fields will all be 0 so it will appear in the top left corner of the map.  

## Clicking on climbing area on the map
If you left click on an climbing area, it's selected.
If you left click and drag on the center of a climbing area, you can move it.  
if you left click and drag on the side or corner of a climbing area, you can resize it.
If you right click on an climbing area, a clone of that climbing area is created.  
If you middle click on an climbing area, it is deleted.  

As detailed in the climbing section of [Level data](https://gitlab.com/Fexean/editor-of-swing/-/blob/master/docs/king_of_swing/leveldata.md), the game also has a separate chunkmap for locating climbing areas.
The editor automatically generates this map from the climbing areas so the user doesn't have to create one.  