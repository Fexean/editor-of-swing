
package editorofswing.rendering;

import javafx.scene.canvas.*;
import javafx.scene.paint.Color;


import editorofswing.types.*;


public class CollisionRenderer {
    GraphicsContext renderer;
    double collisionOpacity;
    
    public CollisionRenderer(Canvas c){
        setCanvas(c);
	collisionOpacity = 0.5;
    }
     
    public CollisionRenderer(){
	collisionOpacity = 0.5;
    }
    
    public void setCollisionOpacity(double a){
	collisionOpacity = a;
    }
    
    public void setCanvas(Canvas canvas){
	renderer = canvas.getGraphicsContext2D();
    }
    
    public void clearCanvas(){
	final Canvas c = renderer.getCanvas();
	renderer.clearRect(0, 0, c.getWidth(), c.getHeight());
    }
    
    
    public void drawMapClimbRects(Map map){
	drawMapClimbRects(map, null);
    }
    
    public void drawMapClimbRects(Map map, ClimbCollision selected){
	renderer.setStroke(Color.rgb(255, 0, 255));
	renderer.setFill(Color.rgb(255, 0, 255));
	for(ClimbCollision col : map.climbCol){
	    if(col != selected){
		renderer.strokeRect(col.x,col.y,col.w,col.h);
		renderer.setGlobalAlpha(collisionOpacity);
		renderer.fillRect(col.x,col.y,col.w,col.h);
		renderer.setGlobalAlpha(1);
	    }
	}
	
	if(selected == null){return;}
	
	renderer.setStroke(Color.rgb(0, 255, 255));
	renderer.setFill(Color.rgb(0, 255, 255));
	
	renderer.strokeRect(selected.x,selected.y,selected.w,selected.h);
	renderer.setGlobalAlpha(collisionOpacity);
	renderer.fillRect(selected.x,selected.y,selected.w,selected.h);
	renderer.setGlobalAlpha(1);
    }
    

    
    public void drawCollisions(Map map){
	drawCollisions(map, 0, 0, map.wChunks*256, map.hChunks*256);
    }
    
    public void drawCollisions(Map map, int x, int y, int w, int h){
	x = Math.max(x, 0);
	y = Math.max(y, 0);

	for(int i = y;i <= y + h && i < map.hChunks*256;i+=8){
	    for(int j = x;j <= x + w && j < map.wChunks*256;j+=8){
		drawCollision(map, j/8, i/8);
	    }
	}
    }
    
    public void drawCollision(Map map, int tx, int ty){
	final int px = tx*8;
	final int py = ty*8;
	renderer.setGlobalAlpha(collisionOpacity);
	byte col = map.wallcol.getCollision(tx,ty);
	renderer.setFill(getCollisionColor(col));
	renderer.fillRect(px, py, 8, 8);
	
	renderer.setFill(getCollisionDirColor(col));
	if((col & 1) == 1){ //up
	    renderer.fillRect(px + 1, py + 1, 6, 1);
	}
	if((col & 2) == 2){ //down
	    renderer.fillRect(px + 1, py + 6, 6, 1);
	}
	if((col & 4) == 4){ //left
	    renderer.fillRect(px + 1, py+1, 1, 6);
	}
	if((col & 8) == 8){ //right
	    renderer.fillRect(px + 6, py+1, 1, 6);
	}
	renderer.setGlobalAlpha(1);
    }
    
    private Color getCollisionColor(byte col){
	//Special cases for collision 0 (No collision) and other directionless collisions
	if(col == 0){
	    return Color.rgb(255, 255, 255);
	}else if((col & 0xF) == 0){
	    return Color.rgb(200, 200, 200);
	}
	
	switch((col >> 4) & 0xF){
	    case 0: return Color.rgb(150, 230, 250); //LEVELBORDER
	    case 1:  return Color.rgb(100, 200, 250);
	    case 2:  return Color.rgb(255, 0, 255); //SPIKE
	    case 3:  return Color.rgb(210, 60, 255);
	    case 4:  return Color.rgb(50, 250, 200);
	    case 5:  return Color.rgb(255, 150, 190);
	    case 6: return Color.rgb(255, 80, 20); //SOLID2
	    case 7: return Color.rgb(200, 100, 0);
	    case 8: return Color.rgb(255, 0, 0);  //SOLID
	    case 9: return Color.rgb(205, 255, 5);
	    case 10: return Color.rgb(255, 255, 90);
	    case 11: return Color.rgb(0, 200, 0);
	    case 12: return Color.rgb(0, 0, 200);
	    case 13: return Color.rgb(0, 200, 200);
	    case 14: return Color.rgb(70, 170, 50);
	    case 15: return Color.rgb(20, 250, 30);
	}
	System.out.println("[getCollisionColor] INVALID COLOR:"+  col);
	return Color.rgb(255, 0, 255);
    }
    
    private Color getCollisionDirColor(byte col){
	return getCollisionColor(col).invert();
    }
}
