
package editorofswing.cli;

import editorofswing.util.SpriteExporter;
import editorofswing.util.Rom;
import editorofswing.util.RomMisc;
import editorofswing.util.RomConstants;

public class SpriteDumperCli {
    Rom rom;
    SpriteExporter exporter;
    int firstId = 0;
    int lastId = 0;
    String dir = ".";
    RomConstants.SpriteType spriteTable = RomConstants.SpriteType.AFFINE;
    
    final static String helpStrings[] = {
        "USAGE: editorofswing spr-dump <romfile> [options] <range start> <range end>",
        "",
        "Options:",
        "--affine\t\tdump sprites from affine table (default)",
        "--player\t\tdump sprites from player table",
        "--regular\t\tdump sprites from regular table",
        "--dir=<dir>\t\tstore dumped sprite in <dir> directory",
        "",
        "<range start> and <range end> should be hexadecimal numbers for dumped sprite id range."
    };
    
    private void parseArgs(String[] args){
        for(int i = 2;i<args.length && args[i].startsWith("-");i++){
            if (args[i].equals("--affine")) {
                spriteTable = RomConstants.SpriteType.AFFINE;
            } else if (args[i].equals("--player")) {
                spriteTable = RomConstants.SpriteType.PLAYER;
            } else if (args[i].equals("--regular")) {
                spriteTable = RomConstants.SpriteType.REGULAR;
            } else if (args[i].startsWith("--dir=")) {
                dir = args[i].substring(6);
            }
        }
        
        try{
            firstId = Integer.parseInt(args[args.length-2], 16);
            lastId = Integer.parseInt(args[args.length-1], 16);
        }catch(Exception e){
            CliUtil.exit("Failed to parse range:" + args[args.length-2] + " to "+ args[args.length-1]);
        }
    }
    
    public static void cliMain(String[] args) {
        if (args.length < 4) {
            CliUtil.printHelp(helpStrings);
            System.exit(0);
        }

        SpriteDumperCli cli = new SpriteDumperCli(args);
        CliUtil.exit("Sprite dump complete");
    }

    public SpriteDumperCli(String[] args) {
        rom = CliUtil.tryOpenRom(args[1]);
        exporter = new SpriteExporter(rom);
        parseArgs(args);
        
        while(firstId <= lastId){
            exporter.exportSpritePng(spriteTable, firstId, String.format("%s/%d.png", dir, firstId));
            firstId++;
        }
    }
}
