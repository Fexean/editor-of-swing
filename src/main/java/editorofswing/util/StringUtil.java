
package editorofswing.util;


public class StringUtil {
    
    
    private static String parseDigits(String text){
        String result="";
        if(text.startsWith("-")){
            result+="-";
        }
        for(int i = 0;i<text.length();i++){
            char c = text.charAt(i);
            if(Character.isDigit(c)){
                result += c;
            }
        }

        return result;
    }
    
    public static int strToInt(String text){
        if(text == null||!isInt(text) ||parseDigits(text).isEmpty() || parseDigits(text).equals("-")){
            return 0;
        }
        if(text.startsWith("0x")){
            if(text.length() > 2){
                return (int)Long.parseLong(text.substring(2),16);
            } 
        }else{
            return Integer.parseInt(parseDigits(text));
        }
        return 0;
    }
    
    public static int hexStrToInt(String text){
        if(!isValidHexString(text)){
            return 0;
        }
        if(text.startsWith("0x")){
            if(text.length() > 2){
                return (int)Long.parseLong(text.substring(2),16);
            } 
        }else{
            return (int)Long.parseLong(parseDigits(text), 16);
        }
        return 0;
    }
    
    public static boolean isValidHexString(String text){
	if(text == null||!isInt(text) ||parseDigits(text).isEmpty() || parseDigits(text).equals("-")){
            return false;
        }
	return true;
    }
    
    private static boolean isInt(String text){
	return text.matches("^[0-9a-fA-FxX\\-:]+$");
    }
}
