
package editorofswing.rendering;

import javafx.scene.canvas.*;


import editorofswing.types.*;

public class TileRenderer {
    GraphicsContext renderer;
    javafx.scene.image.PixelWriter pixelwriter;
    
    public TileRenderer(Canvas c){
        setCanvas(c);
    }
    
    public TileRenderer(){}
    
    public void setCanvas(Canvas canvas){
	renderer = canvas.getGraphicsContext2D();
	pixelwriter = renderer.getPixelWriter();
    }
    
    
    public void drawTile(Tile tile, Palette pal, int palOffset, int x, int y, boolean hFlip, boolean vFlip){
	for(int i = y;i<y+8;i++){
            for(int j = x;j<x+8;j++){
                int xOfs = hFlip ? (2*x+7-j) : j;
		int yOfs = vFlip ? (2*y+7-i) : i;
		short index = (short)(16*palOffset + tile.getColorIndex((byte)(j-x), (byte)(i-y)));
                if(index % 16 != 0){
		    pixelwriter.setColor(xOfs, yOfs, pal.colors[index]);
                }
            }
	}
    }
    
    public void drawTileNoTransparency(Tile tile, Palette pal, int palOffset, int x, int y, boolean hFlip, boolean vFlip){
	for(int i = y;i<y+8;i++){
            for(int j = x;j<x+8;j++){
                int xOfs = hFlip ? (2*x+7-j) : j;
		int yOfs = vFlip ? (2*y+7-i) : i;
		short index = (short)(16*palOffset + tile.getColorIndex((byte)(j-x), (byte)(i-y)));
		pixelwriter.setColor(xOfs, yOfs, pal.colors[index]);
            }
        }
    }
    
    
    public static void drawTileToBuf(int buf[], int pal[], Tile tile, int palOffset, int x, int y, boolean hFlip, boolean vFlip, int bufwidth){
	for(int i = y;i<y+8;i++){
            for(int j = x;j<x+8;j++){
                int xOfs = hFlip ? (2*x+7-j) : j;
		int yOfs = vFlip ? (2*y+7-i) : i;
		short index = (short)(16*palOffset + tile.getColorIndex((byte)(j-x), (byte)(i-y)));
                if(index % 16 != 0){
		    buf[xOfs + yOfs*bufwidth] = pal[index];
                }
            }
	}
    }
    
    public static void drawTileToBufNoTransparency(int buf[], int pal[], Tile tile, int palOffset, int x, int y, boolean hFlip, boolean vFlip, int bufwidth){
	for(int i = y;i<y+8;i++){
            for(int j = x;j<x+8;j++){
                int xOfs = hFlip ? (2*x+7-j) : j;
		int yOfs = vFlip ? (2*y+7-i) : i;
		short index = (short)(16*palOffset + tile.getColorIndex((byte)(j-x), (byte)(i-y)));
		buf[xOfs + yOfs*bufwidth] = pal[index];
            }
	}
    }
    
    
}
