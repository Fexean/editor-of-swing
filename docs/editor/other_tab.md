# Other-tab
![Other-tab Screenshot](https://gitlab.com/Fexean/editor-of-swing/-/raw/master/docs/editor/screenshots/othertab_screenshot.png "Other-tab Screenshot")  

## Song
Pointer to the background music used for the stage.

## Scroll function
This is a function pointer which gets called every frame to scroll the backgrounds.

## Coconut ID
This is the IWRAM offset where the game stores whether the player has found the crystal coconut for this level.
Set to FFFFFFFF on levels with no crystal coconut.  
Only levels 0x00 - 0x6F have this value, it's ignored when saving map at index larger than 0x6F.  


## Medal address
This is the IWRAM address where the game stores whether the player has found the medal for this level.  
Set to FFFFFFFF on levels without a medal.  
Only levels 0x00 - 0x6F have this value, it's ignored when saving map at index larger than 0x6F.  

## Importing / Exporting Palettes
The 256-color palettes for backgrounds and sprites can be exported and imported in the JASC-format.  
When importing the background palette, the first 32 colors are not imported as they are shared by all levels.  

## Importing / Exporting Primary Tileset
The tileset used by BG1 can be exported as a PNG image and imported from a indexed PNG.  
The tileset can at most contain 512 tiles. Each tile's pixel's color is selected by the lower 4 bits of the index of the color used in the png (index % 16).

## Resizing the map
The map's size can only be changed in steps of 256 pixels, for finer control you can use the camera-tab.  
Width can't be larger than 1024, unless the map's height is 256.

## Background Exporter/Importer

![Background Exporter/Importer Screenshot](https://gitlab.com/Fexean/editor-of-swing/-/raw/master/docs/editor/screenshots/bg_exporter_importer.png "Background Exporter/Importer Screenshot")  

The background exporter/importer window allows you to export backgrounds as PNGs and import PNGs to replace the backgrounds.  
This is the only way to edit BG2 & BG3 with this program, but it can also be used to edit BG1.  

To import a background, click the "browse..." button next to the background you want to import and select your image. The image needs to be a 256-color indexed PNG with the same dimensions as the map. You should also make sure that each 8x8 area only uses colors from the same 16-color palette.
After selecting the file click import. BG1 can have at most 512 different tiles while BG2 & BG3 can have a total of 760 tiles. If you leave the filepath empty, an empty tilemap will be inserted. BG2 and BG3 are both inserted at the same time because they share the same tileset.  

When a PNG is imported the editor generates a tileset and a tilemap from the image for the map to use, the map's palette is not modified.  
The PNG's palette is interpreted as 16 16-color palettes. Each tile's pixel's color is selected by the lower 4 bits of the index of the color used in the png (index % 16).
Each tile's palette in the tilemap is selected by finding the most used 16-color palette of that tile, while ignoring transparent pixels.  
