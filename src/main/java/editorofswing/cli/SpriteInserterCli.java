package editorofswing.cli;

import java.util.ArrayList;
import java.io.File;

import editorofswing.util.Rom;
import editorofswing.util.RomMisc;
import editorofswing.util.RomConstants;
import editorofswing.util.SpriteInserter;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;

public class SpriteInserterCli {

    Rom rom;
    RomConstants.SpriteType spriteTable = RomConstants.SpriteType.AFFINE;
    int tileNum = -1;
    boolean autoTileAlloc = true;
    boolean freeTiles = false;
    ArrayList<Integer> spriteIds = new ArrayList();
    ArrayList<String> sprFileNames = new ArrayList();
    SpriteInserter inserter;

    final static String helpStrings[] = {
        "USAGE: editorofswing spr-insert <romfile> [options] [sprites]",
        "",
        "Options:",
        "--affine\t\tinsert sprite into affine table (default)",
        "--player\t\tinsert sprite into player table",
        "--regular\t\tinsert sprite into regular table",
        "-tile=<id>\t\tinsert tiles starting at index id in sprite tileset",
        "-f\t\tdelete tiles used by the sprites that are replaced",
        "",
        "Format for [sprites]:",
        "<Graphic ID>=<png filename>",
        "For example: 1A=spr.png would replace graphic 0x1A with spr.png",
        "You can specify multiple sprites in this format, separated by spaces.",
        "If -tile is not specified, the first empty tile will be chosen."
    };

    public static void cliMain(String[] args) {
        if (args.length < 3) {
            CliUtil.printHelp(helpStrings);
            System.exit(0);
        }

        SpriteInserterCli cli = new SpriteInserterCli(args);
        CliUtil.exit("Sprite insertion complete");
    }

    public SpriteInserterCli(String[] args) {
        rom = CliUtil.tryOpenRom(args[1]);
        inserter = new SpriteInserter(rom);
        int i = parseOptions(args, 2);
        parseSprites(args, i);
        insertSprites();

        CliUtil.trySaveRom(rom);
    }

    private void insertSprites() {
        int addr = RomMisc.align4(RomMisc.findFreeSpace(rom, RomConstants.freespace, 10000)); //TODO: maybe find out space needed

        for (int i = 0; i < spriteIds.size(); i++) {
            try {
                File f = new File(sprFileNames.get(i));
                if (!f.canRead()) {
                    CliUtil.exit("Unable to read file: " + sprFileNames.get(i));
                }
                BufferedImage img = ImageIO.read(f);
                if(autoTileAlloc){
                    tileNum = inserter.findFreeTileNum(spriteTable, img);
                    if(tileNum == -1)
                        CliUtil.exit("Failed to find free tiles.");
                }
                
                if(freeTiles){
                    inserter.freeSpriteTiles(spriteTable, spriteIds.get(i));
                }
                
                switch (spriteTable) {
                    case AFFINE:
                        tileNum = inserter.insertImgAsAffineSprite(spriteIds.get(i), img, addr, tileNum);
                        break;
                    case PLAYER:
                        tileNum = inserter.insertImgAsPlayerSprite(spriteIds.get(i), img, addr, tileNum);
                        break;
                    case REGULAR:
                        tileNum = inserter.insertImgAsRegularSprite(spriteIds.get(i), img, addr, tileNum);
                        break;
                }

                System.out.println(String.format("Inserted %s as sprite %02X at %06X", sprFileNames.get(i), spriteIds.get(i), addr));
            } catch (Exception e) {
                e.printStackTrace();
                CliUtil.exit("Failed to insert sprite \"" + sprFileNames.get(i) + "\"\nError: " + e.getMessage());
            }
        }
    }

    private int parseOptions(String[] args, int start) {
        int i = start;
        for (; i < args.length && args[i].startsWith("-"); i++) {
            if (args[i].equals("--affine")) {
                spriteTable = RomConstants.SpriteType.AFFINE;
            } else if (args[i].equals("--player")) {
                spriteTable = RomConstants.SpriteType.PLAYER;
            } else if (args[i].equals("--regular")) {
                spriteTable = RomConstants.SpriteType.REGULAR;
            } else if (args[i].startsWith("-tile=")) {
                try {
                    tileNum = Integer.parseInt(args[i].substring(1 + args[i].indexOf("=")), 16);
                    autoTileAlloc = false;
                } catch (Exception e) {
                    CliUtil.exit("tileNum must be an integer:" + args[i]);
                }
            } else if(args[i].equals("-f")){
                freeTiles = true;
            }else {
                CliUtil.exit("Invalid option:" + args[i]);
            }
        }
        return i;
    }

    private int parseSprites(String[] args, int start) {
        int i = start;
        for (; i < args.length; i++) {
            if (args[i].contains("=")) {
                sprFileNames.add(args[i].substring(1 + args[i].indexOf("=")));
                try {
                    spriteIds.add(Integer.parseInt(args[i].substring(0, args[i].indexOf("=")), 16));
                } catch (Exception e) {
                    CliUtil.exit("Invalid spriteID:" + args[i].substring(0, args[i].indexOf("=")) + " in arg:" + args[i]);
                }
            } else {
                CliUtil.exit("Invalid sprite argument:" + args[i]);
            }
        }

        if (spriteIds.isEmpty()) {
            CliUtil.exit("No sprites specified");
        }
        return i;
    }

}
