
package editorofswing;

import editorofswing.cli.*;
import editorofswing.mainwindow.MainWindow;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.image.Image;


public class EditorOfSwing extends Application {

    @Override
    public void start(Stage primaryStage) {
        try{	    
            primaryStage.getIcons().add(new Image(ClassLoader.getSystemResourceAsStream("icon.png")));
        }catch(Exception e){
            System.out.println("FAILED TO LOAD ICON: resources/icon.png");
        }
        
	new MainWindow().setupUI(primaryStage);
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
		if(args.length >= 1){
	    	if(args[0].equals("insert")){
                    MapInserterCli.cliMain(args);
	    	}else if(args[0].equals("delete-maps")){
                    LevelDeleterCli.cliMain(args);
	    	}else if(args[0].equals("delete-bgstructs")){
                    BgStructDeleterCli.cliMain(args);
	    	}else if(args[0].equals("spr-insert")){
                    SpriteInserterCli.cliMain(args);
                }else if(args[0].equals("spr-dump")){
                    SpriteDumperCli.cliMain(args);
                }else if(args[0].equals("spr-repoint")){
                    SpriteRepointerCli.cliMain(args);}
                else{
                    CliUtil.exit("Unknown command: " + args[0]);
	    	}
		}else{
                    launch(args);
		}
    }
    
}
