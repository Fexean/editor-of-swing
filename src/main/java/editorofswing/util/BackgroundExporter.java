
package editorofswing.util;

import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.File;

import editorofswing.types.*;

public class BackgroundExporter {
    
    public static void exportTilemap(File file, Tilemap tmap, Palette pal, Tileset tiles) throws Exception{
	final BufferedImage img = new BufferedImage(8*tmap.width, 8*tmap.height, BufferedImage.TYPE_BYTE_INDEXED, pal.getColorModel());
	for(int y = 0;y<tmap.height;y++){
	    for(int x = 0;x<tmap.width;x++){
		final Tile t = tiles.getTile(tmap.getTileAt(x, y));
		final int palnum = tmap.getPal(x, y);
		final boolean hflip = tmap.getHFlip(x, y);
		final boolean vflip = tmap.getVFlip(x, y);
		t.drawTileToBufferedImage(pal, img, 8*x, 8*y, palnum, hflip, vflip);
	    }
	}
	ImageIO.write(img, "png", file);
    }
    
    public static void exportTilemap(String filepath, Tilemap tmap, Palette pal, Tileset tiles) throws Exception{
	exportTilemap(new File(filepath), tmap, pal, tiles);
    }
}
