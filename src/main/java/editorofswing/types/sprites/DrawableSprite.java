
package editorofswing.types.sprites;
import editorofswing.util.Rom;
import editorofswing.util.RomConstants;
import editorofswing.rendering.TileRenderer;
import editorofswing.types.Tile;
import java.awt.image.BufferedImage;

public class DrawableSprite {
    public int framebuf[];
    public int w;
    public int h;
  
    public DrawableSprite(Rom rom, RegularSprite spr, int rgbPal[], int palOffset) throws IllegalArgumentException{
	int tileset = RomConstants.regularSprTiles;
	w = spr.getWidth();
	h = spr.getHeight();
	if(w == -1 || h == -1){
	    throw new IllegalArgumentException("Invalid sprite size: " + w + " " + h);
	}
	framebuf = new int[w*h];

	int minY = Integer.MAX_VALUE;
	int minX = Integer.MAX_VALUE;
	for(SubSprite sub : spr.subsprites){
	    minY = Math.min(minY, sub.yOffset);
	    minX = Math.min(minX, sub.xOffset);
	}
	
	for(SubSprite sub : spr.subsprites){
	    int tilenum = 0;
	    for(int suby = 0;suby<sub.getHeight();suby+=8){
		for(int subx = 0;subx<sub.getWidth();subx+=8){
		    Tile t = Tile.loadTile4bpp(rom, tileset + 0x20*sub.tmap[tilenum]);
		    TileRenderer.drawTileToBuf(framebuf, rgbPal, t, palOffset, sub.xOffset-minX+subx, sub.yOffset-minY+suby, false, false, w);
		    tilenum++;
		}
	    }
	}
    }
    
    
    public DrawableSprite(Rom rom, AffineSprite spr, int rgbPal[], int palOffset, int tileset) throws IllegalArgumentException{
	w = spr.getWidth();
	h = spr.getHeight();
	if(w == -1 || h == -1){
	    throw new IllegalArgumentException("Invalid sprite size: " + w + " " + h);
	}
	framebuf = new int[w*h];
	int tilenum = 0;
	for(int y = 0;y<h;y+=8){
	    for(int x = 0;x<w;x+=8){
		
		//avoid drawing empty preAlloc tiles
		if(tilenum >= spr.preAlloc){
		    Tile t = Tile.loadTile4bpp(rom, tileset + 0x20*spr.tmap[tilenum-spr.preAlloc]);
		    TileRenderer.drawTileToBuf(framebuf, rgbPal, t, palOffset, x, y, false, false, w);
		   
		    //avoid drawing empty postAlloc tiles
		    if(tilenum + 1 == spr.tilecount + spr.preAlloc){
			return;
		    }
		}
		
		tilenum++;
	    }
	}
    }
    
    public static DrawableSprite fromPlayerSpr(Rom rom, AffineSprite spr, int rgbPal[], int palOffset){
	int tileset = RomConstants.playerSprTiles;
	return new DrawableSprite(rom, spr, rgbPal, palOffset, tileset);
    }
    
    public static DrawableSprite fromAffineSpr(Rom rom, AffineSprite spr, int rgbPal[], int palOffset){
	int tileset = RomConstants.affineSprTiles;
	return new DrawableSprite(rom, spr, rgbPal, palOffset, tileset);
    }
    
    public static DrawableSprite fromRegularSpr(Rom rom, RegularSprite spr, int rgbPal[], int palOffset){
	return new DrawableSprite(rom, spr, rgbPal, palOffset);
    }
    
    public BufferedImage asImage(){
        BufferedImage img = new BufferedImage(w, h, BufferedImage.TYPE_4BYTE_ABGR);
        for(int y = 0;y<h;y++){
	    for(int x = 0;x<w;x++){
                img.setRGB(x, y, framebuf[x + y*w]);
            }
        }
        return img;
    }
}
