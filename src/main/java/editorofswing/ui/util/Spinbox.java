/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package editorofswing.ui.util;
import java.awt.Point;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.canvas.*;
import javafx.event.*;
import javafx.scene.paint.Color;
import javafx.scene.Scene;
import javafx.geometry.Insets;
import javafx.stage.Popup;

import java.util.ArrayList;
import javafx.collections.ObservableList;
import javafx.collections.FXCollections;
import javafx.beans.value.*;
import javafx.beans.*;
import javafx.beans.value.ObservableValue;
import javafx.stage.Stage;


import java.util.EventListener;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Pos;
import javafx.scene.control.Tab;


import javafx.util.converter.IntegerStringConverter;
/**
 *  My installation of javafx doesn't have Spinner so I'm creating my own scuffed version
 */
public class Spinbox extends HBox{
    
    Button up, down;
    TextField field;
    int value;
    int min, max;
    
    public Spinbox(int min, int max, int initial){
	up = new Button(">");
	down = new Button("<");
	field = new TextField(""+initial);
	//field.setDisable(true);
	
	value = initial;
	this.max = max;
	this.min = min;
	
	
	this.getChildren().add(field);
	this.getChildren().add(down);
	this.getChildren().add(up);
	
	down.setOnAction(e->{
	    if(value > this.min){
		value--;
		field.setText(""+value);
	    }
	});
	
	up.setOnAction(e->{
	    if(value < this.max){
		value++;
		field.setText(""+value);
	    }
	});
	
    }
    
    public void setOnValueChange(ChangeListener a){
	field.textProperty().addListener((observable, value1, value2)->{
	    try{
		int x = Integer.parseInt(field.getText());
		x = Math.min(x, max);
		x = Math.max(x, min);
		value = x;
		field.setText(""+x);
	    }catch(Exception e){
		System.out.println("invaliud");
		field.setText(value1);
		return;
	    }
	    a.changed(observable, value1, value2);
	});
    }
    
    public void setValue(int val){
	//System.out.println("[spinbox] try to set "+ val+ " "+Math.min(Math.max(val, min), max));
	value = Math.min(Math.max(val, min), max);
	field.setText(""+value);
    }
    
    public int getValue(){
	return value;
    }
    
    public void setLimits(int min_, int max_){
	min = min_;
	max = max_;
	//System.out.println("[spinbox] limits:"+min + " "+max);
    }
}
