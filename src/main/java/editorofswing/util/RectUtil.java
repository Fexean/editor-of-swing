/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package editorofswing.util;

import java.awt.Rectangle;
import javafx.scene.Cursor;
import java.util.ArrayList;
import java.util.Iterator;

public class RectUtil {
    
    
    /*
    rectangle sides:
    1 2 3
    4 5 6
    7 8 9
    (-1 if outside)
    */
    public static int getRectSide(Rectangle rect, int x, int y, double treshold){

	//rect
	final int rx = rect.x;
	final int ry = rect.y;
	final int rw = rect.width;
	final int rh = rect.height;
	
	if(!(x > rx && x < rx+rw && y > ry && y < ry+rh)){
	    return -1;	//(x,y) not in rect
	}
	
	//position in range [0,1]
	final double sx = 1.0*(x - rx)/rw;
	final double sy = 1.0*(y - ry)/rh;
	
	if(sx <= treshold){ //left side
	    if(sy <= treshold){
		return 1;
	    }
	    if(sy >= 1-treshold){
		return 7;
	    }
	    return 4;
	}
	
	if(sx >= 1-treshold){ //right side
	    if(sy <= treshold){
		return 3;
	    }
	    if(sy >= 1-treshold){
		return 9;
	    }
	    return 6;
	}
	
	if(sy <= treshold){ //top
	    return 2;
	}
	if(sy >= 1-treshold){ //bottom
	    return 8;
	}
	
	return 5;
    }
    
    
    public static Cursor rectSideToCursor(int side){
	switch(side){
	    case 2:
	    case 8:
		return Cursor.V_RESIZE;
	    case 4:
	    case 6:
		return Cursor.H_RESIZE;
	    case 1:
		return Cursor.NW_RESIZE;
	    case 3:
		return Cursor.NE_RESIZE;
	    case 7:
		return Cursor.SW_RESIZE;
	    case 9:
		return Cursor.SE_RESIZE;
	    default:
		return Cursor.DEFAULT;
	}
    }
    
    
    private static int getNearestSmallerSpriteSize(int size){
        int sizes[] = {64, 32, 16, 8};
        for(int s : sizes){
            if(s < size)
                return s;
        }
        return 8;
    }
    
    /**Splits rectangles in a list into two until all rects are GBA OAM sized.
     * 
     * @param rects
     * @return 
     */
    public static ArrayList<Rectangle> splitRectsIntoSprites(ArrayList<Rectangle> rects){
        boolean complete = false;
        while(!complete){
            complete = true;
            for(Rectangle rect : rects){
                final int rx = rect.x;
                final int ry = rect.y;
                final int rw = rect.width;
                final int rh = rect.height;
                
                if((rw != 8 && rw != 16 && rw != 32 && rw != 64)
                        || (rw == 64 && rh < 32))
                {
                    int w = getNearestSmallerSpriteSize(rw);
                    rects.add(new Rectangle(rx, ry, w, rh));
                    rects.add(new Rectangle(rx+w, ry, rw-w, rh));
                    rects.remove(rect);
                    complete = false;
                    break;
                }
                
                if((rh != 8 && rh != 16 && rh != 32 && rh != 64)
                        || (rh == 64 && rw < 32))
                {
                    int h = getNearestSmallerSpriteSize(rh);
                    rects.add(new Rectangle(rx, ry, rw, h));
                    rects.add(new Rectangle(rx, ry+h, rw, rh-h));
                    rects.remove(rect);
                    complete = false;
                    break;
                }
            }
        }
        return rects;
    }
    
    public static ArrayList<Rectangle> splitRectsIntoSprites(int w, int h){
        ArrayList<Rectangle> r = new ArrayList();
        r.add(new Rectangle(0, 0, w, h));
        return splitRectsIntoSprites(r);
    }
}
