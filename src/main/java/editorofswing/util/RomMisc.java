

package editorofswing.util;
import java.util.ArrayList;


public class RomMisc {
    private Rom rom;
    
    public static boolean isRomPtr(int ptr){
	return ((ptr&0x9000000) != 0);
    }
    
    public static int align4(int addr){
	return (addr+3)&(~3);
    }
    
    public static int align2(int addr){
	return (addr+1)&(~1);
    }
    
    public RomMisc(Rom r){
        this.rom = r;
    }
    

    

    
    public static int findFreeSpace(Rom rom, int start, int length){
        if (length < 0x100)length = 0x100;
	int tmp = findFreeSpace(rom, start, length, (byte)0xFF);
        if(tmp == -1){
            return findFreeSpace(rom, start, length, (byte)0x00);
        }
        return tmp;
    }
    
    public static int findFreeSpace(Rom rom, int start, int length, byte emptyData){
        int freeAddr = -1;
        int addr = start & 0x1FFFFFF;
        while(addr < rom.size()){
            while(rom.read8(addr) != emptyData && addr < rom.size()){
                addr++;
            }
            boolean result = isEmpty(rom, addr, length, emptyData);
            if(result){
                freeAddr = addr;
                break;
            }
            addr+=0x100;
        }
        
        return freeAddr;
    }
    
    
    public static boolean isEmpty(Rom rom, int address, int length, byte emptyData){
        byte[] data = rom.read(address, length);
        for(int i = 0;i<data.length;i++){
            if(data[i] != emptyData){
                return false;
            }
        }
        return true;
    }
    
    public static void copy(Rom rom, int srcAddr, int dstAddr, int len){
        byte[] data = rom.read(srcAddr, len);
        rom.write(data, dstAddr);
    }
    
    /*
    public void clearSpace(int addr, int len){
        byte[] free = new byte[len];
        for(int i = 0;i<free.length;i++){
            free[i] = (byte)0xFF;
        }
        rom.write(free, addr);
    }
    
    
    int[] findPointers(long pointingTo){
         ArrayList<Integer> pointers = new ArrayList<>();
         for(int i = 0;i<rom.size()-4;i+=4){
             if(rom.read32(i) == pointingTo){
                 pointers.add(rom.read32(i));
             }
         }
         int[] ptrs = new int[pointers.size()];
         for(int i = 0;i<pointers.size();i++){
             ptrs[i] = pointers.get(i);
         }
         return ptrs;
    }
    
    
    public boolean repoint(long dataSrc, int len){
        int[] pointers = findPointers(dataSrc);
        return repoint(pointers, len);
    }
    
    public boolean repoint(int[] srcPtr, int len){
        if(srcPtr.length == 0 || len <= 0){
            return false;
        }
        long src = rom.read32(srcPtr[0]);
        long freeAddr = findFreeSpace(rom, 0, (len > 0x100) ? len : 0x100);
        if(freeAddr == -1){
            return false;
        }
        copy((int)src, (int)freeAddr, len);
        clearSpace((int)src, len);
        for(int i = 0;i<srcPtr.length;i++){
            rom.write32(srcPtr[i], (int)freeAddr);
        }
        return true;
    }*/
}
