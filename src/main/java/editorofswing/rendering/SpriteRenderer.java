
package editorofswing.rendering;


import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import editorofswing.types.sprites.*;
import editorofswing.util.Rom;
import editorofswing.types.Palette;

public class SpriteRenderer {
    GraphicsContext ctx;
    javafx.scene.image.PixelWriter pw;
    Rom rom;	//needed to access sprite tiles
    
    public SpriteRenderer(Rom rom){
	this.rom = rom;
    }
    
    public void setGraphicsContext(GraphicsContext ctx){
	this.ctx = ctx;
	pw = ctx.getPixelWriter();
    }
    
    public void setCanvas(Canvas c){
	ctx = c.getGraphicsContext2D();
	pw = ctx.getPixelWriter();
    }
    
    public void clearCanvas(){
	final Canvas c = ctx.getCanvas();
	ctx.clearRect(0, 0, c.getWidth(), c.getHeight());
    }
    
    public void draw(DrawableSprite spr, int x, int y){
	//using pw.setPixels would overwrite other sprites' pixels with transparent pixels
	for(int j = 0;j<spr.h;j++){
	    for(int i = 0;i<spr.w;i++){
		int color = spr.framebuf[j*spr.w+i];
		if(color != 0)
		    pw.setArgb(x+i, y+j, color);
	    }
	}
    }
    
    public void drawAffine(AffineSprite spr, int x, int y, Palette pal, int palNum) throws Exception{
	draw(DrawableSprite.fromAffineSpr(rom, spr, pal.convertToARGB(), palNum), x, y);
    }
    
    public void drawPlayer(AffineSprite spr, int x, int y, Palette pal, int palNum) throws Exception{
	draw(DrawableSprite.fromPlayerSpr(rom, spr, pal.convertToARGB(), palNum), x, y);
    }
    
    public void drawRegular(RegularSprite spr, int x, int y, Palette pal, int palNum) throws Exception{
	draw(new DrawableSprite(rom, spr, pal.convertToARGB(), palNum), x, y);
    }
}
