
package editorofswing.ui.dialog;

import javafx.scene.control.*;
import java.util.Optional;


public class ConfirmationDialog {
    
    //Returns true if OK was clicked, false otherwise
    public static boolean getConfirmation(String title, String text){
	ButtonType okButtonType = new ButtonType("OK", ButtonBar.ButtonData.OK_DONE);
	ButtonType cancelButtonType = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
	Dialog<ButtonType> dialog = new Dialog<>();
	
	dialog.getDialogPane().getButtonTypes().add(okButtonType);
	dialog.getDialogPane().getButtonTypes().add(cancelButtonType);
	dialog.setTitle(title);
	dialog.setContentText(text);
	Optional<ButtonType> result = dialog.showAndWait();
	
	return result.isPresent() && result.get() == okButtonType;
    }
}
