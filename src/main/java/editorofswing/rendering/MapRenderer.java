

package editorofswing.rendering;

import javafx.scene.canvas.*;
import javafx.scene.paint.Color;


import editorofswing.types.*;
import java.util.ArrayList;


public class MapRenderer {
    TileRenderer tRend = new TileRenderer();
    GraphicsContext renderer;
    javafx.scene.image.PixelWriter pixelwriter;

    boolean drawMapBgLayers = true;
    final int TILESET_WIDTH = 16;   //Width of tileset in tiles
    
    public MapRenderer(Canvas c){
        setCanvas(c);
	tRend.setCanvas(c);
    }
    
    public MapRenderer(){}
    
    public void setCanvas(Canvas canvas){
	renderer = canvas.getGraphicsContext2D();
	pixelwriter = renderer.getPixelWriter();
    }
    
    public void clearCanvas(){
	final Canvas c = renderer.getCanvas();
	renderer.clearRect(0, 0, c.getWidth(), c.getHeight());
    }
    
    
    
    
    public void outlineTilesetTile(short tileNum){
	outlineRect(8*(tileNum%TILESET_WIDTH), 8*(tileNum/TILESET_WIDTH), 8,8);
	
    }

    
    public void outlineRect(int x, int y, int w, int h){
	renderer.setStroke(Color.rgb(255, 0, 255));
	renderer.strokeRect((x),(y),(w),(h));
    }
    

    
    
    public void drawTileset(Tileset tiles, Palette pal, byte paloffset, boolean hFlip, boolean vFlip){
	if(pal.colors.length <= 16*paloffset){
	    System.out.println("[drawTileset] Invalid palette!");
	    paloffset = 0;
	}
	for(int i = 0;i<tiles.length();i++){
            tRend.drawTileNoTransparency(tiles.getTile(i), pal, paloffset, 8*(i%TILESET_WIDTH), (i/TILESET_WIDTH)*8, hFlip, vFlip);
        }
    }
    
   

    
    public void drawMapBlockThumbnail(int x, int y, MapBlock block, Tileset tiles, Palette pal){
	final int SIZE = 32; //width & height
	for(int j = 0;j<SIZE;j++){
	    for(int i = 0;i<SIZE;i++){
		double tx = 1.0*i*block.width / SIZE;
		double ty = 1.0*j*block.height / SIZE;
		int ftx = Math.min((int)tx, block.width - 1);
		int fty =  Math.min((int)ty, block.height - 1);
		Tile t = tiles.getTile(block.tmap.getTileAt(ftx, fty));
		byte subx = (block.tmap.getHFlip(ftx, fty)) ? (byte)(7 - 8*(tx-ftx)) : (byte)(8*(tx-ftx));
		byte suby = (block.tmap.getVFlip(ftx, fty)) ? (byte)(7 - 8*(ty-fty)) : (byte)(8*(ty-fty));
		pixelwriter.setColor(x+i, y+j, pal.colors[16*block.tmap.getPal(ftx, fty)+t.getColorIndex(subx, suby)]);
	    }
	}
    }
    
    public void drawBlockset(ArrayList<MapBlock> blocks, Map map){
	int i = 0;
	final int w = 32*4;
	final int h = 32+32*blocks.size()/4;
	for(int y = 0;y<h;y+=32){
	    for(int x = 0;x<w;x+=32){
		if(i >= blocks.size()){
		    return;
		}
		drawMapBlockThumbnail(x, y, blocks.get(i), map.tileset1, map.pal);
		i += 1;
	    }
	}
    }
    
    
    
    private void drawMapTileToBuf(Map map, int buf[], int pal[], int mapX, int mapY, int bufwidth, int bufx, int bufy){
	int tile; short palOfs; Tile t;
	final int tx = mapX/8;
	final int ty = mapY/8;
	if(drawMapBgLayers){
	    tile = map.layer3.getTileAt(tx, ty);
	    palOfs = map.layer3.getPal(tx, ty);
	    t = map.tileset2.getTile(tile);
	    TileRenderer.drawTileToBufNoTransparency(buf, pal, t, palOfs, bufx, bufy, map.layer3.getHFlip(tx, ty), map.layer3.getVFlip(tx, ty), bufwidth);
	
	    tile = map.layer2.getTileAt(tx, ty);
	    palOfs = map.layer2.getPal(tx, ty);
	    t = map.tileset2.getTile(tile);
	    TileRenderer.drawTileToBuf(buf, pal, t, palOfs, bufx, bufy, map.layer2.getHFlip(tx, ty), map.layer2.getVFlip(tx, ty), bufwidth);
	}
	tile = map.layer1.getTileAt(tx, ty);
	palOfs = map.layer1.getPal(tx, ty);
	t = map.tileset1.getTile(tile);
	TileRenderer.drawTileToBuf(buf, pal, t, palOfs, bufx, bufy, map.layer1.getHFlip(tx, ty), map.layer1.getVFlip(tx, ty), bufwidth);
    }
    
    public void drawMap(Map map){
	drawMap(map, 0, 0, map.w, map.h);
    }
    
    public void drawMap(Map map, int x1, int y1, int x2, int y2){
	x1 = x1&(~7);
	x2 = Math.min(x2 & (~7), map.wChunks*256);
	y1 = y1&(~7);
	y2 = Math.min(y2&(~7), map.hChunks*256);
	final int w = x2-x1;
	final int h = y2-y1;        

	int pal[] = map.pal.convertToARGB();
	
	//Create framebuffer and draw map tiles to it
	int buf[] = new int[w * h];
	
	if(!drawMapBgLayers){
	    //grey background
	    java.util.Arrays.fill(buf, 0xFF808080);
	}
	for(int i = 0;i<w;i+=8){
            for(int j = 0;j<h;j+=8){
                drawMapTileToBuf(map, buf, pal, x1 + i, y1 + j, w, i, j);
            }
        }
	
	//Render buffer
	pixelwriter.setPixels(x1, y1, w, h, javafx.scene.image.PixelFormat.getIntArgbInstance(), buf, 0, w);
    }
    
    
    public void setMapBackgroundDrawing(boolean value){
	drawMapBgLayers = value;
    }
}

                